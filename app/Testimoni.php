<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimoni extends Model
{
    public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function kost()
	{
		return $this->belongsTo('App\Kost', 'kost_id');
	}
}
