<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesan extends Model
{
	protected function getHuruf($id){
		$abjad = ["A", "B", "C"];
		$index = (integer) ($id / 1000);
		return $abjad[$index];
	}

	protected function formatID($id) {
		$huruf = $this->getHuruf($id);
		return $huruf . sprintf('%04d', ($id % 1000));
	}

	public function setIdPesanAttribute($value) {
		$this->attributes['id_pesan'] = $this->formatID($value);
	}

    public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function kost()
	{
		return $this->belongsTo('App\Kost', 'kost_id');
	}
}
