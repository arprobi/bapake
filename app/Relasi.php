<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relasi extends Model
{
    public $timestamps  = false;

    public function kost()
	{
		return $this->belongsTo('App\Kost', 'id_kost');
	}

    public function parameter()
	{
		return $this->belongsTo('App\Parameter', 'id_parameter');
	}
}
