<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


class User extends Model implements Authenticatable, CanResetPasswordContract
{
    use \Illuminate\Auth\Authenticatable;
    use CanResetPassword;
    
	public function kost()
    {
    	return $this->hasMany('App\Kost', 'user_id');
    }

    public function pesan()
    {
    	return $this->hasMany('App\Pesan', 'user_id');
    }

    protected $hidden = [
        'password', 'remember_token',
    ];
}
