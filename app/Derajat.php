<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Derajat extends Model
{
    public $timestamps  = false;

    public function kost()
	{
		return $this->belongsTo('App\Kost', 'kost_id');
	}

	public function himpunan()
	{
		return $this->belongsTo('App\Himpunan', 'himpunan_id');
	}
}
