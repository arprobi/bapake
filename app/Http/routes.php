<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('/login');
});

// Daftar Route
Route::get('/register', 'AuthController@register');
Route::post('/register', 'AuthController@postRegister');

//Login Route
Route::get('/login', 'AuthController@login');
Route::post('/handleLogin', 'AuthController@handleLogin');
Route::get('/logout', 'AuthController@logout');

// Reset Password
Route::get('password/reset/{token?}', 'PasswordController@showResetForm');
Route::post('password/email', 'PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'PasswordController@reset');
Route::get('formatid', 'PesanController@pesanID');


Route::group(['middleware' => 'auth'], function() {
	Route::get('/home', 'HomeController@index');
	Route::get('/profil', 'HomeController@profil');
	Route::post('/foto_profil', 'AuthController@foto_profil');
	Route::get('/ubah_profil', 'HomeController@ubah_profil');
	Route::post('/update_profil', 'HomeController@update_profil');
	Route::post('/gantipassword', 'AuthController@gantipassword');
	Route::get('/detail/{detail}', 'HomeController@detail_kost');
	Route::get('/listkost/{gender}', 'HomeController@listkost');
	Route::post('/testimoni', 'HomeController@testimoni');
	Route::delete('/testimoni', 'HomeController@hapusTestimoni');
	Route::get('/addkost', 'PemilikController@add_mykost');
	Route::get('/unduh', 'HomeController@unduh');
	/* ---ADMINISTRATOR--- */
	Route::group(['middleware' => 'role:admin'], function() 
	{
		Route::get('/admin', 'AdminController@beranda');
		
		/* ---PARAMETER/VARIABEL ROUTE--- */
		Route::get('/parameter','ParameterController@index');
		Route::post('/parameter','ParameterController@store');
		Route::put('/parameter','ParameterController@update');
		Route::delete('/parameter','ParameterController@destroy');

		/* ---HIMPUNAN ROUTE--- */
		Route::get('/himpunan','HimpunanController@index');
		Route::post('/himpunan','HimpunanController@store');
		Route::put('/himpunan','HimpunanController@update');
		Route::delete('/himpunan','HimpunanController@destroy');

		/* ---DERAJAT KEANGGOTAAN ROUTE--- */
		Route::get('/derajat','DerajatController@index');
		Route::get('/derajat/generate','DerajatController@generate');

		/* ---PENGGUNA ROUTE--- */
		Route::get('/pengguna/{level}', 'UserController@user');
		Route::resource('/user','UserController');
		Route::delete('/user','UserController@destroy');

		/* ---KOSTAN ROUTE--- */
		Route::resource('/kost','KostController');
		Route::delete('/kost','KostController@destroy');
		Route::post('/carikost','KostController@carikost');

		/* ---PEMESANAN ROUTE--- */
		Route::get('/pesan','PesanController@index');
	});

	/* ---PEMILIK--- */
	Route::group(['middleware' => 'role:pemilik'], function() 
	{
		Route::get('/kostsaya', 'PemilikController@kostsaya');
		Route::post('/storekost', 'PemilikController@storekost');
		Route::get('/lengkapi/{kost}', 'PemilikController@lengkapi');
		Route::post('/lengkapi', 'PemilikController@postlengkapi');
		Route::get('/ubahkriteria/{id}', 'PemilikController@ubahkriteria');
		Route::get('/edit_kost/{id}', 'PemilikController@edit_kost');
		Route::post('/update_kost', 'PemilikController@update_kost');
		Route::delete('/hapuskost', 'PemilikController@hapuskost');
	});

	/* ---MAHASISWA--- */
	Route::group(['middleware' => 'role:mahasiswa|admin'], function()
	{
		Route::post('/cari', 'HomeController@cari');
		Route::get('/pencarian', 'HomeController@pencarian');
		Route::post('/pesan','PesanController@store');
		Route::get('/pesan/{id}','PesanController@show');
		Route::delete('/pesan','PesanController@destroy');
	});

});
