<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $roles = explode('|', $role);

        if (!in_array(Auth::user()->level, $roles)) {
            return redirect('/');
        }
        // if (Auth::user()->level != $role) {
        //     return redirect('/');
        // }
        return $next($request);
    }
}
