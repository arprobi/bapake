<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use Image;
use File;
use App\Http\Requests;
use Auth;
use App\Kost;
use App\User;
use App\Parameter;
use App\Himpunan;
use App\Relasi;
use App\Derajat;

class PemilikController extends Controller
{
    public function kostsaya()
    {
        $id = Auth::user()->id;
        $kost = Kost::where('user_id', $id)->get();
        $parameters = Parameter::all();
        $parameter = count($parameters);
        return view('userview.kost_saya', compact('kost', 'parameter'));
    }

    public function add_mykost()
    {
        if (Auth::user()->level == 'pemilik') {
            return view('userview.add_mykost');
        } else {
            return redirect('/home')->with('gagal', 'Hanya tersedia untuk pemilik kost.');
        }
        
    }

    public function storekost(Request $request)
    {
        $this->validate($request, [
            'nama_kost' => 'required',
            'jenis_kost' => 'required|min:1',
            'kamar_mandi' => 'required|min:1',
            'jumlah_kamar' => 'required',
            'alamat_kost' => 'required',
            'deskripsi' => 'required',
            'foto_1' => 'required|mimes:jpeg,jpg,png',
            'foto_2' => 'required|mimes:jpeg,jpg,png',
            'foto_3' => 'required|mimes:jpeg,jpg,png',
            'foto_4' => 'required|mimes:jpeg,jpg,png',
        ]);
        
        $kost = new Kost();
        $kost->user_id = Auth::user()->id;
        $nama = Auth::user()->nama_depan;
        $kost->nama_kost = Input::get('nama_kost');
        $kost->jenis_kost = Input::get('jenis_kost');
        $kost->kamar_mandi = Input::get('kamar_mandi');
        $kost->jumlah_kamar = preg_replace('/\D/','',Input::get('jumlah_kamar'));
        $kost->sisa_kamar = Input::get('jumlah_kamar');
        $kost->alamat_kost = Input::get('alamat_kost');
        $kost->deskripsi = Input::get('deskripsi');
        $kost->status = ('Proses');

        $foto_1 = Input::file('foto_1');
        $foto_2 = Input::file('foto_2');
        $foto_3 = Input::file('foto_3');
        $foto_4 = Input::file('foto_4');
        
        $filename_1 = time().'1'.$nama.$kost->user_id.'.'.$foto_1->getClientOriginalExtension();
        $filename_2 = time().'2'.$nama.$kost->user_id.'.'.$foto_2->getClientOriginalExtension();
        $filename_3 = time().'3'.$nama.$kost->user_id.'.'.$foto_3->getClientOriginalExtension();
        $filename_4 = time().'4'.$nama.$kost->user_id.'.'.$foto_4->getClientOriginalExtension();

        $path_1 = public_path('kost_image/' . $filename_1);
        $path_2 = public_path('kost_image/' . $filename_2);
        $path_3 = public_path('kost_image/' . $filename_3);
        $path_4 = public_path('kost_image/' . $filename_4);
        
        Image::make($foto_1)->resize(320,240)->save($path_1);
        Image::make($foto_2)->resize(320,240)->save($path_2);
        Image::make($foto_3)->resize(320,240)->save($path_3);
        Image::make($foto_4)->resize(320,240)->save($path_4);
        
        $kost->foto_1 = $filename_1;
        $kost->foto_2 = $filename_2;
        $kost->foto_3 = $filename_3;
        $kost->foto_4 = $filename_4;

        $kost->save();

        if ($kost) {
            return redirect('/kostsaya')->with('sukses', 'Berhasil menambah kost.');
        } else {
            return redirect('/kostsaya')->with('gagal', 'Terjadi kesalahan saat menambah kost.');
        }
    }

    public function lengkapi($id)
    {
    	$kost = Kost::find($id);
    	$parameter = Parameter::all();
        return view('userview.form_lengkapi', compact('parameter', 'kost'));
    }

    public function postlengkapi(Request $request)
    {
        $parameter = Input::get('id_parameter');
        $nilai = preg_replace('/\D/','',Input::get('nilai'));
        $id_kost = Input::get('id_kost');

        // Hapus Kost yang ada di tabel relasis dan derajat keanggotaan
        $rel_cek = Relasi::where('id_kost', $id_kost);
        if (!empty($rel_cek)) {
            $rel_cek->delete();
        }
        
        $der_cek = Derajat::where('kost_id', $id_kost);
        if (!empty($rel_cek)) {
            $der_cek->delete();
        }
        
        $count = count($parameter);

        for ($i=0; $i < $count ; $i++) 
        { 
            $relasi = new Relasi();
            $relasi->id_kost = $id_kost;
            $relasi->id_parameter = $parameter[$i];
            $relasi->nilai_parameter = $nilai[$i];

            $relasi->save();

            $id = $relasi->id_parameter;

            $par = Parameter::find($id);
            $himpunan = Himpunan::where('id_parameter', $id)->get();
            $nilai_derajat = 0;
            foreach ($himpunan as $him) 
            {
                $derajat = new Derajat();
                $derajat->kost_id = $id_kost;
                $derajat->himpunan_id = $him->id;

                /* --- BAHU KIRI --- */
                if ($him->nilai_1 <= 0 && $him->nilai_2 == 0) {
                    if ($nilai[$i] >= $him->nilai_2 && $nilai[$i] <= $him->nilai_3) {
                        $mu = 1;

                        $derajat->nilai = $mu;
                        $derajat->save();
                    }

                    else if ($nilai[$i] > $him->nilai_3 && $nilai[$i] < $him->nilai_4) {
                        $mu = ($him->nilai_4 - $nilai[$i]) / ($him->nilai_4 - $him->nilai_3);
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }

                    else if ($nilai[$i] < $him->nilai_1) {
                        $mu = 1;
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }
                    else {
                        $mu = 0;
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }
                }

                /* --- SEGITIGA --- */
                else if ($him->nilai_2 == $him->nilai_3) {
                    if ($nilai[$i] == $him->nilai_2) {
                        $mu = 1;
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }

                    else if ($nilai[$i] > $him->nilai_1 && $nilai[$i] < $him->nilai_2) {
                        $mu = ($nilai[$i] - $him->nilai_1) / ($him->nilai_2 - $him->nilai_1);
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }

                    else if ($nilai[$i] > $him->nilai_3 && $nilai[$i] < $him->nilai_4) {
                        $mu = ($him->nilai_4 - $nilai[$i]) / ($him->nilai_4 - $him->nilai_3);
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }
                    else {
                        $mu = 0;
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }
                }

                /* --- BAHU KANAN --- */
                else if ($him->nilai_3 == $par->nilai_angka && $him->nilai_4 >= $par->nilai_angka) {
                    if ($nilai[$i] >= $him->nilai_2 && $nilai[$i] <= $him->nilai_3) {
                        $mu = 1;
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }

                    else if ($nilai[$i] > $him->nilai_1 && $nilai[$i] < $him->nilai_2) {
                        $mu = ($nilai[$i] - $him->nilai_1) / ($him->nilai_2 - $him->nilai_1);
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }

                    else if ($nilai[$i] > $him->nilai_4) {
                        $mu = 1;
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }

                    else {
                        $mu = 0;
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }
                }

                /* --- TRAPESIUM --- */
                else {
                    if ($nilai[$i] > $him->nilai_1 && $nilai[$i] < $him->nilai_2) {
                        $mu = ($nilai[$i] - $him->nilai_1) / ($him->nilai_2 - $him->nilai_1);
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }

                    else if ($nilai[$i] >= $him->nilai_2 && $nilai[$i] <= $him->nilai_3) {
                        $mu = 1;
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }

                    else if ($nilai[$i] > $him->nilai_3 && $nilai[$i] < $him->nilai_4) {
                        $mu = ($him->nilai_4 - $nilai[$i]) / ($him->nilai_4 - $him->nilai_3);
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }
                    else {
                        $mu = 0;
                        
                        $derajat->nilai = $mu;
                        $derajat->save();
                    }
                }
            }
        }

        if ($relasi) {
            return redirect('/kostsaya')->with('sukses', 'Berhasil menambah kriteria kost.');
        } else {
            return redirect('/kostsaya')->with('gagal', 'Terjadi kesalahan saat menambah kriteria kost.');
        }
    }

    public function edit_kost($id) {
        $kost = Kost::find($id);
        return view('userview.form_edit_kost', compact('kost'));
    }

    public function update_kost(Request $request) {
        $id = Input::get('id_kost');
        
        $kost = Kost::find($id);
        
        $kost->nama_kost = Input::get('nama_kost');
        $kost->jenis_kost = Input::get('jenis_kost');
        $kost->kamar_mandi = Input::get('kamar_mandi');
        $kost->jumlah_kamar = Input::get('jumlah_kamar');
        $kost->sisa_kamar = Input::get('jumlah_kamar');
        $kost->alamat_kost = Input::get('alamat_kost');
        $kost->deskripsi = Input::get('deskripsi');

        if (Input::hasFile('foto_1')) {

            File::delete('kost_image/' . $kost->foto_1);
            
            $foto_1 = Input::file('foto_1');
            $filename_1 = time().'1'.$kost->user_id.'.'.$foto_1->getClientOriginalExtension();
            $path_1 = public_path('kost_image/' . $filename_1);
            Image::make($foto_1)->resize(320,240)->save($path_1);
            $kost->foto_1 = $filename_1;
        }

        if (Input::hasFile('foto_2')) {

            File::delete('kost_image/' . $kost->foto_2);
            
            $foto_2 = Input::file('foto_2');
            $filename_2 = time().'2'.$kost->user_id.'.'.$foto_2->getClientOriginalExtension();
            $path_2 = public_path('kost_image/' . $filename_2);
            Image::make($foto_2)->resize(320,240)->save($path_2);
            $kost->foto_2 = $filename_2;
        }

        if (Input::hasFile('foto_3')) {

            File::delete('kost_image/' . $kost->foto_3);
            
            $foto_3 = Input::file('foto_3');
            $filename_3 = time().'3'.$kost->user_id.'.'.$foto_3->getClientOriginalExtension();
            $path_3 = public_path('kost_image/' . $filename_3);
            Image::make($foto_3)->resize(320,240)->save($path_3);
            $kost->foto_3 = $filename_3;
        }

        if (Input::hasFile('foto_4')) {

            File::delete('kost_image/' . $kost->foto_4);
            
            $foto_4 = Input::file('foto_4');
            $filename_4 = time().'4'.$kost->user_id.'.'.$foto_4->getClientOriginalExtension();
            $path_4 = public_path('kost_image/' . $filename_4);
            Image::make($foto_4)->resize(320,240)->save($path_4);
            $kost->foto_4 = $filename_4;
        }

        $kost->update();

        if ($kost) {
            return redirect('/kostsaya')->with('sukses', 'Berhasil mengubah kost.');
        } else {
            return redirect('/kostsaya')->with('gagal', 'Terjadi kesalahan saat mengubah kost.');
        }
    }

    public function ubahkriteria($id) {
        $relasis = Relasi::where('id_kost', $id)->get();
        return view('userview.form_ubah_kriteria', compact('relasis'));
    }

    public function hapuskost(Request $request) {
        $kost = Kost::find($request->id);

        unlink(public_path('kost_image/' . $kost->foto_1));
        unlink(public_path('kost_image/' . $kost->foto_2));
        unlink(public_path('kost_image/' . $kost->foto_3));
        unlink(public_path('kost_image/' . $kost->foto_4));

        $kost->delete();

        if ($kost) {
            return redirect('/kostsaya')->with('sukses', 'Berhasil menghapus kost anda.');
        } else {
            return redirect('/kostsaya')->with('gagal', 'Terjadi kesalahan saat menghapus kost anda.');
        }
    }
}
