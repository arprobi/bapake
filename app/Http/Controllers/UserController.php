<?php 

namespace App\Http\Controllers;

use App\User;
use App\Level;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Image;
use Storage;
use Hash;

/**
* 
*/
class UserController extends Controller
{
	public function index() {
		$users = User::where('level', 'admin')->get();
		return view('user.admin', compact('users'));
	}

	public function create()
	{
		return view('user.create',compact('level'));
	}

	public function store(Request $request)
	{
		$this->validate($request, [
            'no_identitas' => 'required|max:225',
            'nama_depan' => 'required|max:225',
            'nama_belakang' => 'required|max:225',
            'email' => 'required|email|unique:users|max:225',
            'username' => 'required|unique:users|max:225',
            'jenis' => 'required',
            'hp' => 'required|max:20',
        ]);

		$user = new User();
		$user->no_identitas = Input::get('no_identitas');
		$user->nama_depan = Input::get('nama_depan');
		$user->nama_belakang = Input::get('nama_belakang');
		$user->email = Input::get('email');
		$user->username = Input::get('username');
		$user->password = Hash::make($user->no_identitas);
		$user->level = 'admin';
		$user->jenis = Input::get('jenis');
		$user->hp = Input::get('hp');
		$user->foto = 'user.png';

		$user->save();

		if ($user) {
            return redirect('user')->with('sukses', 'Selamat, anda berhasil menambah admin');
        } else {
            return redirect('user')->with('gagal', 'Maaf, terjadi kesalahan saat menambah.');
        }

	}

	public function show($id) {
		$user = User::find($id);
		return view('user.view', compact('user'));
	}

	public function edit($id) {
		$user = User::find($id);
		return view('user.update', compact('level', 'user'));
	}

	public function update(Request $request ,$id) {
		$user = User::find($id);
		$user->no_identitas = $request['no_identitas'];
		$user->nama_depan = $request['nama_depan'];
		$user->nama_belakang = $request['nama_belakang'];
		$user->email = $request['email'];
		$user->username = $request['username'];
		$user->jenis = $request['jenis'];
		$user->hp = $request['hp'];

		if ($user->update()) {
            return redirect()->back()->with('sukses', 'Selamat, anda berhasil meghapus admin');
        } else {
            return redirect()->back()->with('gagal', 'Maaf, terjadi kesalahan saat mengubah.');
        }
	}

	public function destroy(Request $request) {
		$user = User::find($request->id);
		Storage::delete('user_image/'.$user->foto);
		
		if ($user->delete()) {
            return redirect()->back()->with('sukses', 'Selamat, anda berhasil meghapus admin');
        } else {
            return redirect()->back()->with('gagal', 'Maaf, terjadi kesalahan saat menghapus.');
        }
	}

	public function user($level) {
		$users = User::where('level', $level)->get();
		return view('user.index', compact('users','level'));
	}

}

 ?>