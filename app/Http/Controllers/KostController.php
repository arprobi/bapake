<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kost;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Image;
use App\Http\Requests;
use File;

class KostController extends Controller
{
    public function index() {
		$kost = Kost::all();
		return view('kost.index', compact('kost'));
	}

	public function create()
	{
		return view('kost.create');
	}

    public function store(Request $request)
	{
		$this->validate($request, [
            'nama_kost' => 'required',
            'jenis_kost' => 'required|min:1',
            'kamar_mandi' => 'required|min:1',
            'jumlah_kamar' => 'required',
            'alamat_kost' => 'required',
            'deskripsi' => 'required',
            'foto_1' => 'required|mimes:jpeg,jpg,png|max:1520',
            'foto_2' => 'required|mimes:jpeg,jpg,png|max:1520',
            'foto_3' => 'required|mimes:jpeg,jpg,png|max:1520',
            'foto_4' => 'required|mimes:jpeg,jpg,png|max:1520',
        ]);

		$kost = new Kost();
        $kost->user_id = Auth::user()->id;
        $nama = Auth::user()->nama_depan;
        $kost->nama_kost = Input::get('nama_kost');
        $kost->jenis_kost = Input::get('jenis_kost');
        $kost->kamar_mandi = Input::get('kamar_mandi');
        $kost->jumlah_kamar = Input::get('jumlah_kamar');
        $kost->sisa_kamar = preg_replace('/\D/','',Input::get('jumlah_kamar'));
        $kost->alamat_kost = Input::get('alamat_kost');
        $kost->deskripsi = Input::get('deskripsi');
        $kost->status = ('Proses');

        $foto_1 = Input::file('foto_1');
        $foto_2 = Input::file('foto_2');
        $foto_3 = Input::file('foto_3');
        $foto_4 = Input::file('foto_4');
        
        $filename_1 = time().'1'.$nama.$kost->user_id.'.'.$foto_1->getClientOriginalExtension();
        $filename_2 = time().'2'.$nama.$kost->user_id.'.'.$foto_2->getClientOriginalExtension();
        $filename_3 = time().'3'.$nama.$kost->user_id.'.'.$foto_3->getClientOriginalExtension();
        $filename_4 = time().'4'.$nama.$kost->user_id.'.'.$foto_4->getClientOriginalExtension();

        $path_1 = public_path('kost_image/' . $filename_1);
        $path_2 = public_path('kost_image/' . $filename_2);
        $path_3 = public_path('kost_image/' . $filename_3);
        $path_4 = public_path('kost_image/' . $filename_4);
        
        Image::make($foto_1)->resize(320,240)->save($path_1);
        Image::make($foto_2)->resize(320,240)->save($path_2);
        Image::make($foto_3)->resize(320,240)->save($path_3);
        Image::make($foto_4)->resize(320,240)->save($path_4);
        
        $kost->foto_1 = $filename_1;
        $kost->foto_2 = $filename_2;
        $kost->foto_3 = $filename_3;
        $kost->foto_4 = $filename_4;

        $kost->save();

        if ($kost) {
            return redirect('/kost')->with('sukses', 'Berhasil menambah kost.');
        } else {
            return redirect('/kost')->with('gagal', 'Terjadi kesalahan saat menambah kost.');
        }

	}

	public function show($id) {
		$kost = Kost::find($id);
		return view('kost.view', compact('kost'));
	}

	public function edit($id) {
		$kost = Kost::find($id);
		return view('kost.update', compact('kost'));
	}

	public function update(Request $request ,$id) {
		
        $kost = Kost::find($id);

		$kost->nama_kost = Input::get('nama_kost');
        $kost->jenis_kost = Input::get('jenis_kost');
        $kost->kamar_mandi = Input::get('kamar_mandi');
        $kost->jumlah_kamar = Input::get('jumlah_kamar');
        $kost->sisa_kamar = Input::get('jumlah_kamar');
        $kost->alamat_kost = Input::get('alamat_kost');
        $kost->deskripsi = Input::get('deskripsi');
        $kost->status = Input::get('status');

        if (Input::hasFile('foto_1')) {
            File::delete('kost_image/' . $kost->foto_1);
            
            $foto_1 = Input::file('foto_1');
            $filename_1 = time().'1'.$kost->user_id.'.'.$foto_1->getClientOriginalExtension();
            $path_1 = public_path('kost_image/' . $filename_1);
            Image::make($foto_1)->resize(320,240)->save($path_1);
            $kost->foto_1 = $filename_1;
        }

        if (Input::hasFile('foto_2')) {
            File::delete('kost_image/' . $kost->foto_2);
            
            $foto_2 = Input::file('foto_2');
            $filename_2 = time().'2'.$kost->user_id.'.'.$foto_2->getClientOriginalExtension();
            $path_2 = public_path('kost_image/' . $filename_2);
            Image::make($foto_2)->resize(320,240)->save($path_2);
            $kost->foto_2 = $filename_2;
        }

        if (Input::hasFile('foto_3')) {
            File::delete('kost_image/' . $kost->foto_3);
            
            $foto_3 = Input::file('foto_3');
            $filename_3 = time().'3'.$kost->user_id.'.'.$foto_3->getClientOriginalExtension();
            $path_3 = public_path('kost_image/' . $filename_3);
            Image::make($foto_3)->resize(320,240)->save($path_3);
            $kost->foto_3 = $filename_3;
        }

        if (Input::hasFile('foto_4')) {
            File::delete('kost_image/' . $kost->foto_4);
            
            $foto_4 = Input::file('foto_4');
            $filename_4 = time().'4'.$kost->user_id.'.'.$foto_4->getClientOriginalExtension();
            $path_4 = public_path('kost_image/' . $filename_4);
            Image::make($foto_4)->resize(320,240)->save($path_4);
            $kost->foto_4 = $filename_4;
        }

		$kost->update();

		if ($kost) {
            return redirect('/kost')->with('sukses', 'Berhasil mengubah kost.');
        } else {
            return redirect('/kost')->with('gagal', 'Terjadi kesalahan saat mengubah kost.');
        }
	}

	public function destroy(Request $request) {
		$kost = Kost::find($request->id);

		unlink(public_path('kost_image/' . $kost->foto_1));
		unlink(public_path('kost_image/' . $kost->foto_2));
		unlink(public_path('kost_image/' . $kost->foto_3));
		unlink(public_path('kost_image/' . $kost->foto_4));

		$kost->delete();

        if ($kost) {
            return redirect('/kost')->with('sukses', 'Berhasil menghapus kost.');
        } else {
            return redirect('/kost')->with('gagal', 'Terjadi kesalahan saat menghapus kost.');
        }
	}

    public function carikost(Request $request) {
        $cari = Input::get('cari');
        $kost = Kost::where('nama_kost', 'like', '%'.$cari.'%')->get();
        return view('kost.index', compact('kost'));  
    }
}
