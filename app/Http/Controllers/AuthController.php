<?php

namespace App\Http\Controllers;

use App\Config;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\User;
use Auth;
use Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;

class AuthController extends Controller
{
    public function login()
    {
        if (Auth::check()) {
            return redirect('/home');
        } else {
            return view('auth.login');
        }
    }

    public function handleLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required|min:4',
        ]);

        $email = $request->email;
        $password = $request->password;

        $user = User::where('email', $email)->first();
        if ($user) {
            if (Hash::check($password, $user->password)) {
                if ($request->remember) {
                    Auth::login($user, true);
                } else {
                    Auth::login($user, false);
                }
                if (Auth::user()->level == 'admin') {
                    return redirect('/admin');
                } else {
                    return redirect('/home');
                }
            } else {
                return redirect('/login')->with('gagal', 'Alamat E-Mail dengan kata sandi tidak cocok.');
            }
        } else {
            return redirect('/login')->with('gagal', 'Alamat E-Mail tidak ditemukan.');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function register()
    {
        if (Auth::check()) {
            return redirect('/');
        } else {
            return view('auth.register');
        }
    }

    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'no_identitas' => 'required|max:225',
            'nama_depan' => 'required|max:225',
            'nama_belakang' => 'required|max:225',
            'email' => 'required|email|unique:users|max:225',
            'username' => 'required|unique:users|max:225',
            'jenis' => 'required',
            'hp' => 'required|max:20',
            'level' => 'required|max:225',
            'password' => 'required|min:6|max:225',
        ]);
        
        $user = new User();

        $user->no_identitas = Input::get('no_identitas');
        $user->nama_depan = Input::get('nama_depan');
        $user->nama_belakang = Input::get('nama_belakang');
        $user->email = Input::get('email');
        $user->username = Input::get('username');
        $user->jenis = Input::get('jenis');
        $user->hp = Input::get('hp');
        $user->level = Input::get('level');
        $user->foto = ('user.png');

        $password = Input::get('password');
        $user->password = Hash::make($password);
        
        $user->save();

        if ($user) {
            Auth::login($user);
            return redirect('/home');
        } else {
            return redirect('/register')->with('gagal', 'Terjadi kesalahan, lengkapi formulir dengan benar.');
        }
    }

    public function foto_profil(Request $request) {
        $id = Input::get('id');

        $user = User::find($id);

        if ($user->foto != 'user.png') {
            unlink(public_path('user_image/' . $user->foto));
        }

        $foto = Input::file('foto');
        $profil = User::find($id);
        $filename = time().'.'.$profil->username.'.'.$foto->getClientOriginalExtension();
        $path = public_path('user_image/' . $filename);
        Image::make($foto)->resize(450,450)->save($path);
        $profil->foto = $filename;
        
        $profil->update();

        if ($profil) {
            return back()->with('sukses', 'Berhasil mengganti foto profil.');
        } else {
            return back()->with('gagal', 'Gagal mengganti foto, terdapat kesalahan.');
        }
    }

    public function gantipassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:6',
            'new_password' => 'required|min:6|confirmed',
        ]);

        $user = Auth::user();
        if (Hash::check($request->password, $user->password)) {
            $user->password = Hash::make($request->new_password);
            
            if ($user->save()) {
                return redirect('/profil')->with('sukses', 'Berhasil mengubah kata sandi.');
            } else {
                return redirect('/profil')->with('gagal', 'Gagal, terjadi kesalahan.');
            }
        } else {
            return redirect('/profil')->with('gagal', 'Kata sandi lama tidak benar.');
        }
    }
}