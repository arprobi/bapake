<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Kost;
use App\Parameter;
use App\Himpunan;
use App\Pesan;

class AdminController extends Controller
{
    public function beranda() 
    {
    	$pemilik = User::where('level', 'pemilik')->count();
    	$mahasiswa = User::where('level', 'mahasiswa')->count();
    	$kost = Kost::count();
    	$variabel = Parameter::count();
    	$himpunan = Himpunan::count();
    	$pesan = Pesan::count();
    	return view('admin.beranda', compact('pemilik','mahasiswa', 'kost', 'variabel', 'himpunan','pesan'));
    }
    
    public function bantuan() 
    {
    	return view('admin.bantuan');
    }
}
