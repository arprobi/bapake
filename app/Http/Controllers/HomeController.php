<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Kost;
use App\User;
use App\Parameter;
use App\Himpunan;
use App\Derajat;
use App\Pesan;
use App\Testimoni;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use DB;
use Response;
use Image;
use Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kost = Kost::where('status', 'Disetujui')->orderBy('created_at', 'desc')->take(3)->get();
        return view('userview.index', compact('kost'));
    }

    public function profil()
    {
        $id = Auth::user()->id;
        $user = User::find($id);
        return view('userview.profil', compact('user'));
    }

    public function listkost($gender)
    {
        //Update Jumlah Kosan Dulu Berdasar SMS Yg Masuk
        $inbox = $users = DB::table('inbox')->select('TextDecoded','ID')->where('Processed', 'false')->get();
        
        foreach ($inbox as $i => $text) {
            $key = explode(" ", $text->TextDecoded);
            if (($key[0] == 'DEAL') && (!empty($key[1]))) {
                $pesan = Pesan::find($key[1]);
                $pesan->status = 'Sepakat';
                //Ubah status pesan
                $pesan->update();
                //Ubah jumlah kamar kos (-1);
                $kost = Kost::find($pesan->kost_id);
                $kost->sisa_kamar = $kost->sisa_kamar-1;
                $kost->update();
                //Update sms sudah terbaca
                DB::table('inbox')->where('ID', $text->ID)->update(['Processed' => 'true']);
            } elseif (($key[0] == 'BATAL') && (!empty($key[1]))){
                $pesan = Pesan::find($key[1]);
                $pesan->status = 'Batal';
                //Ubah status pesan
                $pesan->update();
                DB::table('inbox')->where('ID', $text->ID)->update(['Processed' => 'true']);
            } else {
                DB::table('inbox')->where('ID', $text->ID)->update(['Processed' => 'true']);
            }
        }

        if ($gender == 'all') {
            $kost = Kost::where('status', 'Disetujui')->get();
        }
        else {
            $kost = Kost::where('jenis_kost', $gender)->where('status', 'Disetujui')->get();
        }
        return view('userview.daftar_kost', compact('kost'));
    }

    public function detail_kost($id)
    {
        $testimoni = Testimoni::where('kost_id', $id)->get();
        $kost = Kost::find($id);
        return view('userview.detail_kost', compact('kost','testimoni'));
    }

    public function pencarian()
    {
        $parameter = Parameter::all();
        return view('userview.form_pencarian', compact('parameter'));
    }

    public function cari(Request $request)
    {
        $cari = Input::get('cari');
        $hasil = array();
        $new = array();
        $kosts = Derajat::groupBy('kost_id')->distinct()->get();

        for ($i=0; $i < count($cari) ; $i++) { 
            if ($cari[$i] > 0) {
                $new[] = $cari[$i];
            }
        }

        foreach ($kosts as $get) {
            $min = '0.00';
            $kost = Kost::where('id', $get->kost_id)->first();
            for ($i=0; $i < count($new) ; $i++) {
                $derajat = Derajat::where('kost_id', $get->kost_id)->where('himpunan_id', [$new[$i]])->orderBy('kost_id','asc')->first();

                if (($i == 0 && $derajat->nilai > $min) || $derajat->nilai < $min) {
                    $min = $derajat->nilai;
                }

                $data = array(
                    'kost' => $kost,
                    'nilai' => $min,
                );
            }

            $hasil[] = $data;
        }

        for ($i=0; $i < count($hasil); $i++) { 
            for ($j=$i; $j < count($hasil); $j++) { 
                if ($i < count($hasil)-1) {
                    if ($hasil[$i]['nilai'] < $hasil[$j]['nilai']) {
                        $temp = $hasil[$i];
                        $hasil[$i] = $hasil[$j];
                        $hasil[$j] = $temp;
                    }
                }
            }
        }
        return view('userview.hasil_cari', compact('hasil'));
    }

    public function testimoni (Request $request) {
        // cek user sudah pesan belum
        $id = Input::get('kost_id');
        $pesan = Pesan::where('user_id', Auth::user()->id)
                        ->where('kost_id', $id)
                        ->where('status', 'Sepakat')->first();

        if (!empty($pesan)) {
            $testimoni = new Testimoni();
            $testimoni->user_id = Auth::user()->id;
            $testimoni->kost_id = $id;
            $testimoni->komentar = Input::get('komentar');

            $testimoni->save();
            return back()->with('sukses', 'Terimakasih telah memberikan komentar.');
        } else {
            return back()->with('gagal', 'Maaf, komentar hanya untuk pengguna yang sudah memesan kost ini.');
        }
        
    }

    public function hapusTestimoni(Request $request) {
        $testimoni = Testimoni::find($request->id);

        if ($testimoni->user_id == Auth::user()->id | Auth::user()->level == 'admin') {
            if ($testimoni->delete()) {
                return back()->with('sukses', 'Komentar dihapus.');
            } else {
                return back()->with('gagal', 'Gagal Hapus komentar.');
            }
        } else {
            return back()->with('gagal', 'Hanya dapat menghapus komentar anda.');
        }
    }

    public function ubah_profil() {
        $id = Auth::user()->id;
        $user = User::find($id);
        return view('userview.edit_form', compact('user'));
    }

    public function update_profil(Request $request) {        
        $id = Input::get('id');
        
        $user = User::find($id);
        $user->no_identitas = Input::get('no_identitas');
        $user->nama_depan = Input::get('nama_depan');
        $user->nama_belakang = Input::get('nama_belakang');
        $user->email = Input::get('email');
        $user->username = Input::get('username');
        $user->jenis = Input::get('jenis');
        $user->hp = Input::get('hp');
        
        $user->update();

        if ($user) {
            return redirect('/profil')->with('sukses', 'Selamat, anda berhasil mengubah profil.');
        } else {
            return redirect('/profil')->with('gagal', 'Terjadi kesalahan, lengkapi formulir dengan benar.');
        }
    }

    public function unduh() {
        $file= public_path(). "/download/user_manual.pdf";
        $headers = array(
              'Content-Type: application/pdf',
            );
        return Response::download($file, 'user_manual.pdf', $headers);
    }

}
