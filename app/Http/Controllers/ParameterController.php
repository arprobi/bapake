<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parameter;
use Illuminate\Support\Facades\Input;
use App\Kost;

use App\Http\Requests;

class ParameterController extends Controller
{
    public function index() {
		$parameter = Parameter::all();
		return view('parameter.index', compact('parameter'));
	}

	public function create()
	{
		return view('parameter.create');
	}

    public function store(Request $request)
	{
		$this->validate($request, [
            'nama_parameter' => 'required|max:100',
            'nilai_angka' => 'required|integer',
            'satuan' => 'required|max:100',
        ]);

		$parameter = new Parameter();
		$parameter->nama_parameter = Input::get('nama_parameter');
		$parameter->nilai_angka = Input::get('nilai_angka');
		$parameter->satuan = Input::get('satuan');

		$parameter->save();

		$kost = Kost::all();
		$parameter->kost()->attach($kost);

		if ($parameter) {
            return redirect('/parameter')->with('sukses', 'Berhasil menambah variabel.');
        } else {
            return redirect('/parameter')->with('gagal', 'Terjadi kesalahan saat menambah variabel.');
        }

	}

	public function show($id) {
		$halaman = 'parameter';
		$parameter = Parameter::find($id);
		return view('parameter.view', compact('halaman','parameter'));
	}

	public function edit($id) {
		$parameter = Parameter::find($id);
		return view('parameter.update', compact('parameter'));
	}

	public function update(Request $request) {
		$parameter = Parameter::find($request->id);
		$parameter->nama_parameter = $request->nama_parameter;
		$parameter->nilai_angka = $request->nilai_angka;
		$parameter->satuan = $request->satuan;

		$parameter->update();

		if ($parameter) {
            return redirect('/parameter')->with('sukses', 'Berhasil mengubah variabel.');
        } else {
            return redirect('/parameter')->with('gagal', 'Terjadi kesalahan saat mengubah variabel.');
        }
	}

	public function destroy(Request $request) {
		$parameter = Parameter::find($request->id);
		$parameter->delete();
		if ($parameter) {
            return redirect('/parameter')->with('sukses', 'Berhasil menghapus parameter.');
        } else {
            return redirect('/parameter')->with('gagal', 'Terjadi kesalahan saat menghapus parameter.');
        }
	}
}
