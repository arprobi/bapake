<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Derajat;
use App\Kost;
use App\Parameter;
use App\Himpunan;
use App\Relasi;
use App\Http\Requests;

class DerajatController extends Controller
{
    public function index() {
    	$derajats = Derajat::paginate(10);
		return view('derajat.index', compact('derajats'));
	}

	public function generate() {
		Derajat::truncate();
		$kosts = Kost::where('status', 'Disetujui')->get();
		foreach ($kosts as $kost) {
			foreach ($kost->relasi as $relasi) {
				foreach ($relasi->parameter->himpunan as $himpunan) {
					$derajat = new Derajat();
	                $derajat->kost_id = $kost->id;
	                $derajat->himpunan_id = $himpunan->id;
					/* --- Mulai Penghitungan Derajat Keanggotaan --- */
					/* --- BAHU KIRI --- */
					if ($himpunan->nilai_1 <= 0 && $himpunan->nilai_2 == 0) {
						if ($relasi->nilai_parameter >= $himpunan->nilai_2 && $relasi->nilai_parameter <= $himpunan->nilai_3) {
	                        $mu = 1;

	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }

	                    else if ($relasi->nilai_parameter > $himpunan->nilai_3 && $relasi->nilai_parameter < $himpunan->nilai_4) {
	                        $mu = ($himpunan->nilai_4 - $relasi->nilai_parameter) / ($himpunan->nilai_4 - $himpunan->nilai_3);
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }
	                    else if ($relasi->nilai_parameter < $himpunan->nilai_1) {
	                        $mu = 1;
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }
	                    else {
	                        $mu = 0;
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }
					}
					/* --- SEGITIGA --- */
	                else if ($himpunan->nilai_2 == $himpunan->nilai_3) {
	                    if ($relasi->nilai_parameter == $himpunan->nilai_2) {
	                        $mu = 1;
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }

	                    else if ($relasi->nilai_parameter > $himpunan->nilai_1 && $relasi->nilai_parameter < $himpunan->nilai_2) {
	                        $mu = ($relasi->nilai_parameter - $himpunan->nilai_1) / ($himpunan->nilai_2 - $himpunan->nilai_1);
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }

	                    else if ($relasi->nilai_parameter > $himpunan->nilai_3 && $relasi->nilai_parameter < $himpunan->nilai_4) {
	                        $mu = ($himpunan->nilai_4 - $relasi->nilai_parameter) / ($himpunan->nilai_4 - $himpunan->nilai_3);
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }
	                    else {
	                        $mu = 0;
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }
	                }
	                /* --- BAHU KANAN --- */
	                else if ($himpunan->nilai_3 == $relasi->parameter->nilai_angka && $himpunan->nilai_4 >= $relasi->parameter->nilai_angka) {
	                    if ($relasi->nilai_parameter >= $himpunan->nilai_2 && $relasi->nilai_parameter <= $himpunan->nilai_3) {
	                        $mu = 1;
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }

	                    else if ($relasi->nilai_parameter > $himpunan->nilai_1 && $relasi->nilai_parameter < $himpunan->nilai_2) {
	                        $mu = ($relasi->nilai_parameter - $himpunan->nilai_1) / ($himpunan->nilai_2 - $himpunan->nilai_1);
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }

	                    else if ($relasi->nilai_parameter > $himpunan->nilai_4) {
	                        $mu = 1;
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }

	                    else {
	                        $mu = 0;
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }
	                }

	                /* --- TRAPESIUM --- */
	                else {
	                    if ($relasi->nilai_parameter > $himpunan->nilai_1 && $relasi->nilai_parameter < $himpunan->nilai_2) {
	                        $mu = ($relasi->nilai_parameter - $himpunan->nilai_1) / ($himpunan->nilai_2 - $himpunan->nilai_1);
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }

	                    else if ($relasi->nilai_parameter >= $himpunan->nilai_2 && $relasi->nilai_parameter <= $himpunan->nilai_3) {
	                        $mu = 1;
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }

	                    else if ($relasi->nilai_parameter > $himpunan->nilai_3 && $relasi->nilai_parameter < $himpunan->nilai_4) {
	                        $mu = ($himpunan->nilai_4 - $relasi->nilai_parameter) / ($himpunan->nilai_4 - $himpunan->nilai_3);
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }
	                    else {
	                        $mu = 0;
	                        
	                        $derajat->nilai = $mu;
	                        $derajat->save();
	                    }
	                }
				}
			}
		}
		return redirect('derajat')->with('sukses', 'Derajat keanggotaan berhasil di perbaharui');
	}
}
