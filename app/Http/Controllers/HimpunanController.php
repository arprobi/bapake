<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Himpunan;
use App\Parameter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;

class HimpunanController extends Controller
{
    public function index() {
    	$par = Parameter::all();
		$himpunan = Himpunan::paginate(10);
		return view('himpunan.index', compact('himpunan','par'));
	}

	public function create()
	{
		$par = Parameter::all();
		return view('himpunan.create', compact('par'));
	}

    public function store(Request $request)
	{
		$this->validate($request, [
            'id_parameter' => 'required',
            'nama_himpunan' => 'required|max:100',
            'nilai_1' => 'required|integer',
            'nilai_2' => 'required|integer',
            'nilai_3' => 'required|integer',
            'nilai_4' => 'required|integer',
        ]);

		$himpunan = new Himpunan();
		$himpunan->id_parameter = Input::get('id_parameter');
		$himpunan->nama_himpunan = Input::get('nama_himpunan');
		$himpunan->nilai_1 = Input::get('nilai_1');
		$himpunan->nilai_2 = Input::get('nilai_2');
		$himpunan->nilai_3 = Input::get('nilai_3');
		$himpunan->nilai_4 = Input::get('nilai_4');

		$cek = Himpunan::where('id_parameter', $himpunan->id_parameter)->where('nama_himpunan', $himpunan->nama_himpunan)->count();

		if ($cek <= 0) 
		{
			$himpunan->save();
			if ($himpunan) {
	            return redirect('/himpunan')->with('sukses', 'Berhasil menambah himpunan.');
	        } else {
	            return redirect('/himpunan')->with('gagal', 'Terjadi kesalahan saat menambah himpunan.');
	        }
		}
		else 
		{
			return redirect('/himpunan')->with('gagal', 'Variabel dan himpunan sudah ada.');
		}
	}

	public function show($id) {
		$halaman = 'himpunan';
		$himpunan = Himpunan::find($id);
		return view('himpunan.view', compact('halaman','himpunan'));
	}

	public function edit($id) {
		$himpunan = Himpunan::find($id);
		$par = Parameter::all();
		return view('himpunan.update', compact('himpunan', 'par'));
	}

	public function update(Request $request) 
	{
		$this->validate($request, [
            'id_parameter' => 'required',
            'nama_himpunan' => 'required|max:100',
            'nilai_1' => 'required|integer',
            'nilai_2' => 'required|integer',
            'nilai_3' => 'required|integer',
            'nilai_4' => 'required|integer',
        ]);
        
		$himpunan = Himpunan::find($request->id);
		$himpunan->id_parameter = $request->id_parameter;
		$himpunan->nama_himpunan = $request->nama_himpunan;
		$himpunan->nilai_1 = $request->nilai_1;
		$himpunan->nilai_2 = $request->nilai_2;
		$himpunan->nilai_3 = $request->nilai_3;
		$himpunan->nilai_4 = $request->nilai_4;

		$himpunan->update();

		if ($himpunan) {
            return redirect('/himpunan')->with('sukses', 'Berhasil mengubah himpunan.');
        } else {
            return redirect('/himpunan')->with('gagal', 'Terjadi kesalahan saat mengubah himpunan.');
        }
	}

	public function destroy(Request $request) {
		$himpunan = Himpunan::find($request->id);
		$himpunan->delete();
		if ($himpunan) {
            return redirect('/himpunan')->with('sukses', 'Berhasil menghapus himpunan.');
        } else {
            return redirect('/himpunan')->with('gagal', 'Terjadi kesalahan saat menghapus himpunan.');
        }
	}
}
