<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Pesan;
use App\Kost;
use Auth;
use DB;

class PesanController extends Controller
{
    public function index() {
    	//Update Jumlah Kosan Dulu Berdasar SMS Yg Masuk
    	$inbox = $users = DB::table('inbox')->select('TextDecoded','ID')->where('Processed', 'false')->get();
        
        foreach ($inbox as $i => $text) {
            $key = explode(" ", $text->TextDecoded);
            if (($key[0] == 'DEAL') && (!empty($key[1]))) {
                $pesan = Pesan::find($key[1]);
                $pesan->status = 'Sepakat';
                //Ubah status pesan
                $pesan->update();
                //Ubah jumlah kamar kos (-1);
                $kost = Kost::find($pesan->kost_id);
                $kost->sisa_kamar = $kost->sisa_kamar-1;
                $kost->update();
                //Update sms sudah terbaca
                DB::table('inbox')->where('ID', $text->ID)->update(['Processed' => 'true']);
            } elseif (($key[0] == 'BATAL') && (!empty($key[1]))){
            	$pesan = Pesan::find($key[1]);
                $pesan->status = 'Batal';
                //Ubah status pesan
                $pesan->update();
                DB::table('inbox')->where('ID', $text->ID)->update(['Processed' => 'true']);
            } else {
            	DB::table('inbox')->where('ID', $text->ID)->update(['Processed' => 'true']);
            }
        }
        
    	$pesans = Pesan::all();
    	return view('pesan.index', compact('pesans'));
    }

    public function store(Request $request)
	{
		$pesan = new Pesan();
		$pesan->user_id = Auth::user()->id;
		$pesan->kost_id = Input::get('kost_id');
		$pesan->created_at = \Carbon\Carbon::now();
		$pesan->updated_at = \Carbon\Carbon::now();

		$pesan->save();

		$id = Input::get('kost_id');
		
		$id_pesan = $pesan->id;
		$mahasiswa = Auth::user()->nama_depan;
		$pemilik = $pesan->kost->user->hp;

		if(substr(trim($pemilik), 0, 1)=='0') {
			$tujuan = '+62'.substr(trim($pemilik), 1);
		}

		DB::table('outbox')->insert([
			'DestinationNumber' => $tujuan,
		    'TextDecoded' => 'Mahasiswa A.N '.$mahasiswa.' memesan kostan anda. Silahkankan balas pesan ini dengan format: DEAL[spasi]'.$id_pesan.' jika transaksi berhasil.',
		    'CreatorID' => $mahasiswa
		]);

		if ($pesan) {
            return redirect()->back()->with('sukses', 'Anda berhasil memesan kost berikut. Lakukan transaksi secara langsung dengan datang ke alamat kost yang tertera secepatnya. Jika dalam waktu 2x24 tidak dilakukan transaksi maka akan secara otomatis pemesanan dibatalkan');
        } else {
            return redirect()->back()->with('gagal', 'Maaf, terjadi kesalahan dalam proses pemesanan kost.');
        }

	}

    public function show($id) {
        $pesan = Pesan::find($id);
        return view('pesan.view', compact('pesan'));
    }

	public function destroy(Request $request) {
		$pesan = Pesan::find($request->id);
		$pesan->delete();
		if ($pesan) {
            return redirect('/pesan')->with('sukses', 'Berhasil menghapus pesan.');
        } else {
            return redirect('/pesan')->with('gagal', 'Terjadi kesalahan saat menghapus pesan.');
        }
	}

    public function pesanID() {
        // Cari idpesan terakhir
        $id_terakhir = Pesan::all()->last();
        $idnya = $id_terakhir->id; //untuk nyimpan id terakhirnya

        $pesan = new Pesan();
        $pesan->id_pesan = $idnya+1;
        $pesan->kost_id = 1;
        $pesan->user_id = 3;
        $pesan->save();
    }
}
