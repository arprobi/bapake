<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kost extends Model
{
    public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function relasi()
    {
    	return $this->hasMany('App\Relasi', 'id_kost');
    }

    public function testimoni()
    {
        return $this->hasMany('App\Testimoni', 'kost_id');
    }

    public function derajat()
    {
        return $this->hasMany('App\Derajat', 'kost_id');
    }

    public function pesan()
    {
        return $this->hasMany('App\Pesan', 'kost_id');
    }

}
