<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Himpunan extends Model
{
    public $timestamps  = false;

    //Relasi dengan table parameter
    public function parameter()
	{
		return $this->belongsTo('App\Parameter', 'id_parameter');
	}

	public function derajat()
    {
    	return $this->hasMany('App\Derajat', 'himpunan_id');
    }
	
}
