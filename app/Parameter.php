<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
	public $timestamps  = false;

	//Relasi dengan table himpunan
	public function himpunan()
    {
    	return $this->hasMany('App\Himpunan','id_parameter');
    }
    
	public function relasi()
    {
    	return $this->hasMany('App\Relasi', 'id_parameter');
    }

    public function kost()
    {
        return $this->belongsToMany('App\Kost','relasis','id_parameter','id_kost');
    }
}
