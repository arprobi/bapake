<?php

namespace App;

class Config
{
    const APP_NAME = 'Bapake';
    const LEVEL_ADMIN = 'admin';
    const LEVEL_MAHASISWA = 'mahasiswa';
    const LEVEL_PEMILIK = 'pemilik';
}
