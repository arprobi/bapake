
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 09, 2017 at 08:36 PM
-- Server version: 10.1.20-MariaDB
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u744930526_bapak`
--

-- --------------------------------------------------------

--
-- Table structure for table `daemons`
--

CREATE TABLE IF NOT EXISTS `daemons` (
  `Start` text NOT NULL,
  `Info` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `derajats`
--

CREATE TABLE IF NOT EXISTS `derajats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kost_id` int(11) NOT NULL,
  `himpunan_id` int(11) NOT NULL,
  `nilai` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `derajats_ibfk_1` (`kost_id`),
  KEY `derajats_ibfk_2` (`himpunan_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=299 ;

--
-- Dumping data for table `derajats`
--

INSERT INTO `derajats` (`id`, `kost_id`, `himpunan_id`, `nilai`) VALUES
(1, 2, 1, '0.00'),
(2, 2, 2, '0.71'),
(3, 2, 3, '0.09'),
(4, 2, 4, '0.00'),
(5, 2, 5, '0.00'),
(6, 2, 6, '0.58'),
(7, 2, 7, '0.00'),
(8, 2, 8, '0.60'),
(9, 2, 9, '0.33'),
(10, 2, 10, '1.00'),
(11, 2, 11, '0.00'),
(12, 2, 12, '0.00'),
(13, 2, 13, '0.50'),
(14, 2, 14, '0.20'),
(15, 2, 15, '0.00'),
(16, 2, 16, '0.05'),
(17, 2, 17, '0.92'),
(18, 2, 18, '0.00'),
(19, 2, 19, '1.00'),
(20, 2, 20, '0.00'),
(21, 2, 21, '0.00'),
(22, 2, 22, '1.00'),
(23, 2, 23, '0.00'),
(24, 2, 24, '0.00'),
(25, 2, 25, '0.29'),
(26, 2, 26, '0.50'),
(27, 2, 27, '0.00'),
(28, 3, 1, '0.00'),
(29, 3, 2, '0.63'),
(30, 3, 3, '0.00'),
(31, 3, 4, '0.00'),
(32, 3, 5, '0.80'),
(33, 3, 6, '0.05'),
(34, 3, 7, '0.25'),
(35, 3, 8, '0.50'),
(36, 3, 9, '0.00'),
(37, 3, 10, '0.00'),
(38, 3, 11, '0.50'),
(39, 3, 12, '0.25'),
(40, 3, 13, '0.31'),
(41, 3, 14, '0.50'),
(42, 3, 15, '0.00'),
(43, 3, 16, '0.00'),
(44, 3, 17, '0.50'),
(45, 3, 18, '0.20'),
(46, 3, 19, '1.00'),
(47, 3, 20, '0.00'),
(48, 3, 21, '0.00'),
(49, 3, 22, '1.00'),
(50, 3, 23, '0.00'),
(51, 3, 24, '0.00'),
(52, 3, 25, '1.00'),
(53, 3, 26, '0.00'),
(54, 3, 27, '0.00'),
(55, 6, 1, '0.63'),
(56, 6, 2, '0.00'),
(57, 6, 3, '0.00'),
(58, 6, 4, '0.00'),
(59, 6, 5, '0.00'),
(60, 6, 6, '0.00'),
(61, 6, 7, '0.00'),
(62, 6, 8, '0.00'),
(63, 6, 9, '1.00'),
(64, 6, 10, '1.00'),
(65, 6, 11, '0.00'),
(66, 6, 12, '0.00'),
(67, 6, 13, '0.93'),
(68, 6, 14, '0.00'),
(69, 6, 15, '0.00'),
(70, 6, 16, '1.00'),
(71, 6, 17, '0.00'),
(72, 6, 18, '0.00'),
(73, 6, 19, '1.00'),
(74, 6, 20, '0.00'),
(75, 6, 21, '0.00'),
(76, 6, 22, '0.00'),
(77, 6, 23, '0.00'),
(78, 6, 24, '0.20'),
(79, 6, 25, '1.00'),
(80, 6, 26, '0.00'),
(81, 6, 27, '0.00'),
(82, 8, 1, '0.88'),
(83, 8, 2, '0.00'),
(84, 8, 3, '0.00'),
(85, 8, 4, '0.00'),
(86, 8, 5, '0.60'),
(87, 8, 6, '0.00'),
(88, 8, 7, '0.00'),
(89, 8, 8, '1.00'),
(90, 8, 9, '0.00'),
(91, 8, 10, '0.25'),
(92, 8, 11, '0.50'),
(93, 8, 12, '0.00'),
(94, 8, 13, '0.60'),
(95, 8, 14, '0.04'),
(96, 8, 15, '0.00'),
(97, 8, 16, '1.00'),
(98, 8, 17, '0.00'),
(99, 8, 18, '0.00'),
(100, 8, 19, '1.00'),
(101, 8, 20, '0.00'),
(102, 8, 21, '0.00'),
(103, 8, 22, '0.63'),
(104, 8, 23, '0.00'),
(105, 8, 24, '0.00'),
(106, 8, 25, '0.86'),
(107, 8, 26, '0.00'),
(108, 8, 27, '0.00'),
(109, 9, 1, '0.00'),
(110, 9, 2, '0.71'),
(111, 9, 3, '0.09'),
(112, 9, 4, '0.50'),
(113, 9, 5, '0.00'),
(114, 9, 6, '0.00'),
(115, 9, 7, '0.25'),
(116, 9, 8, '0.50'),
(117, 9, 9, '0.00'),
(118, 9, 10, '0.38'),
(119, 9, 11, '0.25'),
(120, 9, 12, '0.00'),
(121, 9, 13, '0.00'),
(122, 9, 14, '0.00'),
(123, 9, 15, '0.93'),
(124, 9, 16, '1.00'),
(125, 9, 17, '0.00'),
(126, 9, 18, '0.00'),
(127, 9, 19, '0.60'),
(128, 9, 20, '0.00'),
(129, 9, 21, '0.00'),
(130, 9, 22, '0.00'),
(131, 9, 23, '0.00'),
(132, 9, 24, '0.20'),
(133, 9, 25, '1.00'),
(134, 9, 26, '0.00'),
(135, 9, 27, '0.00'),
(136, 10, 1, '0.63'),
(137, 10, 2, '0.00'),
(138, 10, 3, '0.00'),
(139, 10, 4, '0.00'),
(140, 10, 5, '0.00'),
(141, 10, 6, '0.60'),
(142, 10, 7, '0.25'),
(143, 10, 8, '0.50'),
(144, 10, 9, '0.00'),
(145, 10, 10, '1.00'),
(146, 10, 11, '0.00'),
(147, 10, 12, '0.00'),
(148, 10, 13, '0.83'),
(149, 10, 14, '0.00'),
(150, 10, 15, '0.00'),
(151, 10, 16, '0.00'),
(152, 10, 17, '0.00'),
(153, 10, 18, '0.80'),
(154, 10, 19, '0.00'),
(155, 10, 20, '0.00'),
(156, 10, 21, '0.53'),
(157, 10, 22, '0.00'),
(158, 10, 23, '0.00'),
(159, 10, 24, '0.68'),
(160, 10, 25, '0.36'),
(161, 10, 26, '0.38'),
(162, 10, 27, '0.00'),
(163, 11, 1, '0.00'),
(164, 11, 2, '0.00'),
(165, 11, 3, '0.55'),
(166, 11, 4, '0.85'),
(167, 11, 5, '0.00'),
(168, 11, 6, '0.00'),
(169, 11, 7, '0.00'),
(170, 11, 8, '0.60'),
(171, 11, 9, '0.33'),
(172, 11, 10, '0.13'),
(173, 11, 11, '0.75'),
(174, 11, 12, '0.00'),
(175, 11, 13, '0.94'),
(176, 11, 14, '0.00'),
(177, 11, 15, '0.00'),
(178, 11, 16, '0.50'),
(179, 11, 17, '0.17'),
(180, 11, 18, '0.00'),
(181, 11, 19, '1.00'),
(182, 11, 20, '0.00'),
(183, 11, 21, '0.00'),
(184, 11, 22, '0.00'),
(185, 11, 23, '0.67'),
(186, 11, 24, '0.06'),
(187, 11, 25, '1.00'),
(188, 11, 26, '0.00'),
(189, 11, 27, '0.00'),
(190, 12, 1, '0.00'),
(191, 12, 2, '0.63'),
(192, 12, 3, '0.00'),
(193, 12, 4, '0.00'),
(194, 12, 5, '0.50'),
(195, 12, 6, '0.13'),
(196, 12, 7, '0.00'),
(197, 12, 8, '0.60'),
(198, 12, 9, '0.33'),
(199, 12, 10, '0.00'),
(200, 12, 11, '0.00'),
(201, 12, 12, '1.00'),
(202, 12, 13, '0.85'),
(203, 12, 14, '0.00'),
(204, 12, 15, '0.00'),
(205, 12, 16, '0.75'),
(206, 12, 17, '0.00'),
(207, 12, 18, '0.00'),
(208, 12, 19, '0.00'),
(209, 12, 20, '0.75'),
(210, 12, 21, '0.09'),
(211, 12, 22, '0.00'),
(212, 12, 23, '0.00'),
(213, 12, 24, '0.63'),
(214, 12, 25, '0.71'),
(215, 12, 26, '0.00'),
(216, 12, 27, '0.00'),
(244, 15, 1, '0.00'),
(245, 15, 2, '0.88'),
(246, 15, 3, '0.00'),
(247, 15, 4, '1.00'),
(248, 15, 5, '0.00'),
(249, 15, 6, '0.00'),
(250, 15, 7, '0.00'),
(251, 15, 8, '0.00'),
(252, 15, 9, '1.00'),
(253, 15, 10, '0.88'),
(254, 15, 11, '0.00'),
(255, 15, 12, '0.00'),
(256, 15, 13, '1.00'),
(257, 15, 14, '0.00'),
(258, 15, 15, '0.00'),
(259, 15, 16, '0.00'),
(260, 15, 17, '0.80'),
(261, 15, 18, '0.08'),
(262, 15, 19, '0.00'),
(263, 15, 20, '0.75'),
(264, 15, 21, '0.09'),
(265, 15, 22, '0.00'),
(266, 15, 23, '1.00'),
(267, 15, 24, '0.00'),
(268, 15, 25, '1.00'),
(269, 15, 26, '0.00'),
(270, 15, 27, '0.00'),
(271, 13, 1, '0.94'),
(272, 13, 2, '0.00'),
(273, 13, 3, '0.00'),
(274, 13, 4, '0.00'),
(275, 13, 5, '0.00'),
(276, 13, 6, '0.75'),
(277, 13, 7, '0.25'),
(278, 13, 8, '0.50'),
(279, 13, 9, '0.00'),
(280, 13, 10, '1.00'),
(281, 13, 11, '0.00'),
(282, 13, 12, '0.00'),
(283, 13, 13, '1.00'),
(284, 13, 14, '0.00'),
(285, 13, 15, '0.00'),
(286, 13, 16, '0.25'),
(287, 13, 17, '0.58'),
(288, 13, 18, '0.00'),
(289, 13, 19, '0.80'),
(290, 13, 20, '0.00'),
(291, 13, 21, '0.00'),
(292, 13, 22, '1.00'),
(293, 13, 23, '0.00'),
(294, 13, 24, '0.00'),
(295, 13, 25, '0.00'),
(296, 13, 26, '0.82'),
(297, 13, 27, '0.08'),
(298, 13, 28, '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `gammu`
--

CREATE TABLE IF NOT EXISTS `gammu` (
  `Version` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gammu`
--

INSERT INTO `gammu` (`Version`) VALUES
(13);

-- --------------------------------------------------------

--
-- Table structure for table `himpunans`
--

CREATE TABLE IF NOT EXISTS `himpunans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parameter` int(11) NOT NULL,
  `nama_himpunan` varchar(30) NOT NULL,
  `nilai_1` int(11) NOT NULL,
  `nilai_2` int(11) NOT NULL,
  `nilai_3` int(11) NOT NULL,
  `nilai_4` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `himpunans_ibfk_1` (`id_parameter`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `himpunans`
--

INSERT INTO `himpunans` (`id`, `id_parameter`, `nama_himpunan`, `nilai_1`, `nilai_2`, `nilai_3`, `nilai_4`) VALUES
(1, 1, 'Murah', 0, 0, 2200000, 3000000),
(2, 1, 'Sedang', 2500000, 3300000, 3300000, 4000000),
(3, 1, 'Mahal', 3400000, 4500000, 4500000, 4500000),
(4, 2, 'Dekat', 0, 0, 200, 400),
(5, 2, 'Sedang', 300, 500, 500, 600),
(6, 2, 'Jauh', 500, 900, 900, 900),
(7, 3, 'Sempit', 0, 0, 6, 10),
(8, 3, 'Sedang', 8, 10, 10, 15),
(9, 3, 'Luas', 10, 16, 16, 16),
(10, 4, 'Sedikit', 0, 0, 4, 12),
(11, 4, 'Sedang', 8, 12, 12, 16),
(12, 4, 'Banyak', 12, 20, 20, 20),
(13, 5, 'Dekat', 0, 0, 200, 1000),
(14, 5, 'Sedang', 500, 1000, 1000, 1600),
(15, 5, 'Jauh', 1000, 2500, 2500, 2500),
(16, 6, 'Dekat', 0, 0, 50, 250),
(17, 6, 'Sedang', 130, 250, 250, 350),
(18, 6, 'Jauh', 250, 500, 500, 500),
(19, 7, 'Dekat', 0, 0, 200, 450),
(20, 7, 'Sedang', 400, 450, 450, 650),
(21, 7, 'Jauh', 450, 1000, 1000, 1000),
(22, 8, 'Dekat', 0, 0, 300, 700),
(23, 8, 'Sedang', 500, 700, 700, 850),
(24, 8, 'Jauh', 700, 1500, 1500, 1500),
(25, 9, 'Dekat', 0, 0, 1500, 2200),
(26, 9, 'Sedang', 1800, 2200, 2200, 2750),
(27, 9, 'Jauh', 2200, 3500, 3500, 3500),
(28, 14, 'Sempit', 0, 0, 5, 10);

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

CREATE TABLE IF NOT EXISTS `inbox` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ReceivingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Text` text NOT NULL,
  `SenderNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text NOT NULL,
  `SMSCNumber` varchar(20) NOT NULL DEFAULT '',
  `Class` int(11) NOT NULL DEFAULT '-1',
  `TextDecoded` text NOT NULL,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RecipientID` text NOT NULL,
  `Processed` enum('false','true') NOT NULL DEFAULT 'false',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `inbox`
--

INSERT INTO `inbox` (`UpdatedInDB`, `ReceivingDateTime`, `Text`, `SenderNumber`, `Coding`, `UDH`, `SMSCNumber`, `Class`, `TextDecoded`, `ID`, `RecipientID`, `Processed`) VALUES
('2017-01-14 06:33:21', '2017-01-08 00:35:59', '004F00690069', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'Oii', 1, '', 'true'),
('2017-01-14 06:33:21', '2017-01-08 01:21:37', '0059004100200033', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'YA 3', 2, '', 'true'),
('2017-01-14 06:33:21', '2017-01-08 10:41:26', '004400450041004C00200032', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'DEAL 2', 3, '', 'true'),
('2017-01-14 06:33:21', '2017-01-08 10:41:50', '006600750063006B', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'fuck', 4, '', 'true'),
('2017-01-14 06:33:21', '2017-01-13 06:24:54', '004400450041004C00200034', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'DEAL 4', 5, '', 'true'),
('2017-01-14 13:55:13', '2017-01-14 13:54:39', '004400450041004C00200035', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'DEAL 5', 6, '', 'true'),
('2017-01-14 13:57:25', '2017-01-14 13:57:10', '0042004100540041004C00200036', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'BATAL 6', 7, '', 'true'),
('2017-01-16 03:24:33', '2017-01-16 03:24:39', '004400450041004C00200037', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'DEAL 7', 8, '', 'true');

--
-- Triggers `inbox`
--
DROP TRIGGER IF EXISTS `balas_sms`;
DELIMITER //
CREATE TRIGGER `balas_sms` AFTER INSERT ON `inbox`
 FOR EACH ROW INSERT INTO outbox SET DestinationNumber = new.SenderNumber, TextDecoded = IF(SUBSTRING_INDEX(new.TextDecoded, ' ', 1)='DEAL','Selamat transaksi kost berhasil',IF(SUBSTRING_INDEX(new.TextDecoded, ' ', 1)='BATAL','Terimakasih, semoga beruntung lain waktu','Maaf Format SMS Salah')), CreatorID = 'admin'
//
DELIMITER ;
DROP TRIGGER IF EXISTS `inbox_timestamp`;
DELIMITER //
CREATE TRIGGER `inbox_timestamp` BEFORE INSERT ON `inbox`
 FOR EACH ROW BEGIN
    IF NEW.ReceivingDateTime = '0000-00-00 00:00:00' THEN
        SET NEW.ReceivingDateTime = CURRENT_TIMESTAMP();
    END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kosts`
--

CREATE TABLE IF NOT EXISTS `kosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nama_kost` varchar(100) NOT NULL,
  `jenis_kost` enum('Pria','Wanita','Campur') NOT NULL,
  `kamar_mandi` enum('Luar','Dalam') NOT NULL,
  `jumlah_kamar` int(11) NOT NULL,
  `sisa_kamar` int(11) NOT NULL,
  `foto_1` text NOT NULL,
  `foto_2` text,
  `foto_3` text,
  `foto_4` text,
  `alamat_kost` text NOT NULL,
  `deskripsi` text NOT NULL,
  `status` enum('Disetujui','Proses') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kosts_ibfk_1` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `kosts`
--

INSERT INTO `kosts` (`id`, `user_id`, `nama_kost`, `jenis_kost`, `kamar_mandi`, `jumlah_kamar`, `sisa_kamar`, `foto_1`, `foto_2`, `foto_3`, `foto_4`, `alamat_kost`, `deskripsi`, `status`, `created_at`, `updated_at`) VALUES
(2, 4, 'Kosan Bu Priyo', 'Wanita', 'Luar', 2, 1, '148453633114.jpg', '148452704224.jpg', '148452704234.jpg', '148452704344.jpg', 'RT 06 RW 03 Desa Sidakangen Kecamatan Kalimanah Kabupaten Purbalingga Jawa Tengah""', 'Kos Putri Nyaman Tenang, \r\nFasilitas : Kamar Mandi Dalem, Lemari, Kasur, Meja Belajar, dan lain-lain\r\n(Harga belum termasuk listrik)"', 'Disetujui', '2016-12-15 22:45:27', '2017-01-15 20:24:33'),
(3, 4, 'Kos Pak Joko Bal', 'Wanita', 'Luar', 14, 14, '148439421714.jpg', '148439421924.jpg', '148439421934.jpg', '148439421944.jpg', 'Desa Blater RT 01 RW 05 Kecamatan Kalimanah Kabupaten Purbalingga Jawa Tengah', 'Kos Putri Nyaman, Aman, Dekat dengan alfamart dan kampus fakultas teknik universitas jenderal soedirman', 'Disetujui', '2016-12-15 23:07:44', '2017-01-14 04:43:58'),
(6, 4, 'Kosan dr. Herni Setyowati', 'Wanita', 'Luar', 4, 3, '148299474814.jpg', '148299474924.jpg', '148299474934.jpg', '148299475044.jpg', 'Ds Jompo Kec. Kalimanah Kab, Purbalingga"', 'Kos Putri, Fasilitas : ruang tamu, kasur, Lemari, Kamar mandi luar, kamar mandi dalam, kamar luas, batas jam malam pkl 09.00. laki-laki dilarang masuk kamar, silahkan berada di ruang tamu"', 'Disetujui', '2016-12-17 19:08:33', '2017-01-13 23:05:53'),
(8, 4, 'Kosan Bu Tik Tik', 'Pria', 'Luar', 6, 5, '14820534991Pratama4.jpg', '14820534992Pratama4.jpg', '14820534993Pratama4.jpg', '14820534994Pratama4.jpg', 'Ds. Blater RT 02/RW 05 No 31 Kc. Kalimanah, Kb. Purbalingga Prov.Jawa Tengah ', 'Fasilitas : Kamar mandi luar (3 kamar), Free Air, Kasur, Lemari, Parkir Luas, Dapur, Ruang Tamu, TV', 'Disetujui', '2016-12-18 02:31:41', '2017-01-13 23:05:53'),
(9, 4, 'Warung Oren (Geprek)', 'Pria', 'Luar', 9, 9, '14820538471Pratama4.jpg', '14820538472Pratama4.jpg', '14820538473Pratama4.jpg', '14820538474Pratama4.jpg', 'Samping Lapangan Desa Blater Kecamatan Kalimanah Kabupaten Purbalingga Jawa Tengah.', 'Fasilitas : Kasur, Lemari, Kamar Mandi (luar dan dalam), Meja Belajar, Free Air, Dekat dengan Kampus, Parkir Luas, ', 'Disetujui', '2016-12-18 02:37:30', '2016-12-18 02:37:30'),
(10, 4, 'Kosan Pak Hamid', 'Pria', 'Luar', 4, 3, '14829957881Pratama4.jpg', '14829957882Pratama4.jpg', '14829957883Pratama4.jpg', '14829957884Pratama4.jpg', 'RT 17 / RW 07 Desa Siakangen Kecamatan Kalimanah Kabupaten Purbalingga Jawa Tengah, 53371', 'Kost putra murah, kamar mandi luar,\r\nFasilitas : Ruang tamu, ruang sholat, TV, Dapur, Kasur, Meja Belajar, Lemari. Harga belum termasuk listrik dan air.', 'Disetujui', '2016-12-29 00:16:31', '2017-01-14 06:55:13'),
(11, 4, 'Wisma Reishofa', 'Wanita', 'Dalam', 11, 11, '14829969321Pratama4.jpg', '14829969322Pratama4.jpg', '14829969323Pratama4.jpg', '14829969324Pratama4.jpg', 'Jalan Mayjen Sungkono No. 9a, Sidakangen, Kalimanah, Purbalingga, Jawa Tengah 53371 Indonesia"', 'Kost putri luas nyaman, bersih, harga tergantung ukuran. fasilitas : meja belajar, kasur, lemari, bantal, sprei, ruang tamu.\r\nBukan kost bebas, waktu kunjung lawan jenis max jam 9."', 'Disetujui', '2016-12-29 00:35:34', '2016-12-29 01:23:43'),
(12, 4, 'Wisma Alden', 'Wanita', 'Luar', 20, 20, '14843234971Pratama4.jpg', '14843234972Pratama4.jpg', '14843234973Pratama4.jpg', '14843234974Pratama4.jpg', 'Wisma Alden Desa Blater No 17 RT 02 RW 03 Kecamatan Kalimanah, Kabupaten Purbalingga Jawa Tengah, 53371', 'Kost Putri Luas, Nyaman, Aman, Banyak Pilihan.\r\nFasilitas : Kasur, Lemari, Kamar Mandi Luar/Dalam, Harga termasuk Listrik, Dekat Dengan Kampus', 'Disetujui', '2017-01-13 09:05:04', '2017-01-14 07:27:52'),
(13, 6, 'Kontrakan Bpk.Marsudi', 'Pria', 'Luar', 3, 3, '148452655916.jpg', '14844937822Mokhammad6.jpg', '14844937823Mokhammad6.jpg', '14844937824Mokhammad6.jpg', 'RT 06 RW 03 Desa Sidakangen Kecamatan Kalimanah Kabupaten Purbalingga, Jawa Tengah 53371', 'Kontrakan Lengkap, TV, Ruang Tamu, Dapur, 3 Kamar, Parkir Luas, Kamar Mandi, Listrik Pascabayar\r\nFasilitas : Kasur, Meja, Lemari, Tv, Peralatan Dapur dan lain-lain\r\nMurah, Lokasi aman, tentram, damai', 'Disetujui', '2017-01-15 08:23:06', '2017-01-15 20:29:34'),
(14, 6, 'Griya Alifah', 'Pria', 'Dalam', 6, 6, '148452650016.jpg', '148452650226.jpg', '148452650436.jpg', '148452650546.jpg', 'RT 06 RW 03 Desa Sidakangen Kecamatan Kalimanah Kabupaten Purbalingga, Jawa Tengah 53371', 'Kosan Lengkap, 6 Kamar, Parkir Luas, Kamar Mandi Dalam, \r\nFasilitas : Kasur, Meja, Lemari, dan lain-lain Murah, Lokasi aman, tentram, damai', 'Proses', '2017-01-15 08:26:29', '2017-01-15 17:28:26'),
(16, 6, 'Wisma Demo', 'Pria', 'Luar', 20, 20, '14853273661Mokhammad6.jpg', '14853273662Mokhammad6.JPG', '14853273663Mokhammad6.jpg', '14853273664Mokhammad6.jpg', 'Jalan Mawar Melati Semuanya Indah Desan Kebunku, Kecamatan Penuh Dengan Bunga Kabupaten Suwe Ora jamu', 'Lihat kebunku penuh dengan bunga mawar melati semuanya indah', 'Proses', '2017-01-25 11:56:06', '2017-01-25 11:56:06');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_10_25_014312_create_table_user', 2);

-- --------------------------------------------------------

--
-- Table structure for table `outbox`
--

CREATE TABLE IF NOT EXISTS `outbox` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SendingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SendBefore` time NOT NULL DEFAULT '23:59:59',
  `SendAfter` time NOT NULL DEFAULT '00:00:00',
  `Text` text,
  `DestinationNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text,
  `Class` int(11) DEFAULT '-1',
  `TextDecoded` text NOT NULL,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MultiPart` enum('false','true') DEFAULT 'false',
  `RelativeValidity` int(11) DEFAULT '-1',
  `SenderID` varchar(255) DEFAULT NULL,
  `SendingTimeOut` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `DeliveryReport` enum('default','yes','no') DEFAULT 'default',
  `CreatorID` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `outbox_date` (`SendingDateTime`,`SendingTimeOut`),
  KEY `outbox_sender` (`SenderID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Triggers `outbox`
--
DROP TRIGGER IF EXISTS `outbox_timestamp`;
DELIMITER //
CREATE TRIGGER `outbox_timestamp` BEFORE INSERT ON `outbox`
 FOR EACH ROW BEGIN
    IF NEW.InsertIntoDB = '0000-00-00 00:00:00' THEN
        SET NEW.InsertIntoDB = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.SendingDateTime = '0000-00-00 00:00:00' THEN
        SET NEW.SendingDateTime = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.SendingTimeOut = '0000-00-00 00:00:00' THEN
        SET NEW.SendingTimeOut = CURRENT_TIMESTAMP();
    END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `outbox_multipart`
--

CREATE TABLE IF NOT EXISTS `outbox_multipart` (
  `Text` text,
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text,
  `Class` int(11) DEFAULT '-1',
  `TextDecoded` text,
  `ID` int(10) unsigned NOT NULL DEFAULT '0',
  `SequencePosition` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`,`SequencePosition`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `parameters`
--

CREATE TABLE IF NOT EXISTS `parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_parameter` varchar(100) NOT NULL,
  `nilai_angka` int(11) NOT NULL,
  `satuan` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `parameters`
--

INSERT INTO `parameters` (`id`, `nama_parameter`, `nilai_angka`, `satuan`) VALUES
(1, 'Harga', 4500000, 'Rupiah'),
(2, 'Jarak dari Kampus', 900, 'Meter'),
(3, 'Luas Kamar', 16, 'Meter2'),
(4, 'Jumlah Kamar', 20, 'Kamar'),
(5, 'Jarak dari Masjid', 2500, 'Meter'),
(6, 'Jarak dari Warung', 500, 'Meter'),
(7, 'Akses dari Jalan Raya', 1000, 'Meter'),
(8, 'Jarak dari Minimarket', 1500, 'Meter'),
(9, 'Jarak dari Klinik Kesehatan', 3500, 'Meter'),
(14, 'Luas Parkiran', 20, 'meter2');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pbk`
--

CREATE TABLE IF NOT EXISTS `pbk` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '-1',
  `Name` text NOT NULL,
  `Number` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pbk_groups`
--

CREATE TABLE IF NOT EXISTS `pbk_groups` (
  `Name` text NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pesans`
--

CREATE TABLE IF NOT EXISTS `pesans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `kost_id` int(11) NOT NULL,
  `status` enum('Menunggu','Sepakat','Batal') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pesans_ibfk_1` (`user_id`),
  KEY `pesans_ibfk_2` (`kost_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `pesans`
--

INSERT INTO `pesans` (`id`, `user_id`, `kost_id`, `status`, `created_at`, `updated_at`) VALUES
(4, 3, 8, 'Batal', '2017-01-12 23:23:46', '2017-01-13 23:01:10'),
(7, 5, 2, 'Sepakat', '2017-01-15 20:03:46', '2017-01-15 20:24:32'),
(8, 5, 12, 'Menunggu', '2017-01-23 07:18:33', '2017-01-23 07:18:33'),
(9, 13, 8, 'Menunggu', '2017-01-23 08:15:15', '2017-01-23 08:15:15');

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

CREATE TABLE IF NOT EXISTS `phones` (
  `ID` text NOT NULL,
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TimeOut` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Send` enum('yes','no') NOT NULL DEFAULT 'no',
  `Receive` enum('yes','no') NOT NULL DEFAULT 'no',
  `IMEI` varchar(35) NOT NULL,
  `Client` text NOT NULL,
  `Battery` int(11) NOT NULL DEFAULT '-1',
  `Signal` int(11) NOT NULL DEFAULT '-1',
  `Sent` int(11) NOT NULL DEFAULT '0',
  `Received` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IMEI`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phones`
--

INSERT INTO `phones` (`ID`, `UpdatedInDB`, `InsertIntoDB`, `TimeOut`, `Send`, `Receive`, `IMEI`, `Client`, `Battery`, `Signal`, `Sent`, `Received`) VALUES
('', '2017-01-16 03:50:15', '2017-01-16 02:40:39', '2017-01-16 03:50:25', 'yes', 'yes', '860547004556469', 'Gammu 1.33.0, Windows Server 2007, GCC 4.7, MinGW 3.11', 100, 84, 2, 1);

--
-- Triggers `phones`
--
DROP TRIGGER IF EXISTS `phones_timestamp`;
DELIMITER //
CREATE TRIGGER `phones_timestamp` BEFORE INSERT ON `phones`
 FOR EACH ROW BEGIN
    IF NEW.InsertIntoDB = '0000-00-00 00:00:00' THEN
        SET NEW.InsertIntoDB = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.TimeOut = '0000-00-00 00:00:00' THEN
        SET NEW.TimeOut = CURRENT_TIMESTAMP();
    END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `relasis`
--

CREATE TABLE IF NOT EXISTS `relasis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kost` int(11) NOT NULL,
  `id_parameter` int(11) NOT NULL,
  `nilai_parameter` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_kost` (`id_kost`),
  KEY `id_parameter` (`id_parameter`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=168 ;

--
-- Dumping data for table `relasis`
--

INSERT INTO `relasis` (`id`, `id_kost`, `id_parameter`, `nilai_parameter`) VALUES
(19, 11, 1, 4000000),
(20, 11, 2, 230),
(21, 11, 3, 12),
(22, 11, 4, 11),
(23, 11, 5, 250),
(24, 11, 6, 150),
(25, 11, 7, 15),
(26, 11, 8, 750),
(27, 11, 9, 1300),
(28, 12, 1, 3000000),
(29, 12, 2, 550),
(30, 12, 3, 12),
(31, 12, 4, 20),
(32, 12, 5, 320),
(33, 12, 6, 100),
(34, 12, 7, 500),
(35, 12, 8, 1200),
(36, 12, 9, 1700),
(37, 2, 1, 3500000),
(38, 2, 2, 730),
(39, 2, 3, 12),
(40, 2, 4, 2),
(41, 2, 5, 600),
(42, 2, 6, 240),
(43, 2, 7, 150),
(44, 2, 8, 160),
(45, 2, 9, 2000),
(46, 3, 1, 3000000),
(47, 3, 2, 520),
(48, 3, 3, 9),
(49, 3, 4, 14),
(50, 3, 5, 750),
(51, 3, 6, 300),
(52, 3, 7, 15),
(53, 3, 8, 50),
(54, 3, 9, 700),
(55, 6, 1, 2500000),
(56, 6, 2, 1100),
(57, 6, 3, 16),
(58, 6, 4, 4),
(59, 6, 5, 260),
(60, 6, 6, 30),
(61, 6, 7, 50),
(62, 6, 8, 860),
(63, 6, 9, 5),
(64, 8, 1, 2300000),
(65, 8, 2, 420),
(66, 8, 3, 10),
(67, 8, 4, 10),
(68, 8, 5, 520),
(69, 8, 6, 20),
(70, 8, 7, 120),
(71, 8, 8, 450),
(72, 8, 9, 1600),
(82, 10, 1, 2500000),
(83, 10, 2, 740),
(84, 10, 3, 9),
(85, 10, 4, 4),
(86, 10, 5, 340),
(87, 10, 6, 450),
(88, 10, 7, 740),
(89, 10, 8, 1240),
(90, 10, 9, 1950),
(91, 9, 1, 3500000),
(92, 9, 2, 300),
(93, 9, 3, 9),
(94, 9, 4, 9),
(95, 9, 5, 2400),
(96, 9, 6, 5),
(97, 9, 7, 300),
(98, 9, 8, 860),
(99, 9, 9, 1450),
(118, 14, 1, 3000000),
(119, 14, 2, 800),
(120, 14, 3, 6),
(121, 14, 4, 6),
(122, 14, 5, 140),
(123, 14, 6, 190),
(124, 14, 7, 250),
(125, 14, 8, 260),
(126, 14, 9, 2300),
(137, 15, 1, 3200000),
(138, 15, 2, 200),
(139, 15, 3, 16),
(140, 15, 4, 5),
(141, 15, 5, 180),
(142, 15, 6, 270),
(143, 15, 7, 500),
(144, 15, 8, 700),
(145, 15, 9, 900),
(147, 2, 14, 0),
(148, 3, 14, 0),
(149, 6, 14, 0),
(150, 8, 14, 0),
(151, 9, 14, 0),
(152, 10, 14, 0),
(153, 11, 14, 0),
(154, 12, 14, 0),
(156, 14, 14, 0),
(157, 15, 14, 0),
(158, 13, 1, 2250000),
(159, 13, 2, 800),
(160, 13, 3, 9),
(161, 13, 4, 3),
(162, 13, 5, 150),
(163, 13, 6, 200),
(164, 13, 7, 250),
(165, 13, 8, 260),
(166, 13, 9, 2300),
(167, 13, 14, 15);

-- --------------------------------------------------------

--
-- Table structure for table `sentitems`
--

CREATE TABLE IF NOT EXISTS `sentitems` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SendingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DeliveryDateTime` timestamp NULL DEFAULT NULL,
  `Text` text NOT NULL,
  `DestinationNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text NOT NULL,
  `SMSCNumber` varchar(20) NOT NULL DEFAULT '',
  `Class` int(11) NOT NULL DEFAULT '-1',
  `TextDecoded` text NOT NULL,
  `ID` int(10) unsigned NOT NULL DEFAULT '0',
  `SenderID` varchar(255) NOT NULL,
  `SequencePosition` int(11) NOT NULL DEFAULT '1',
  `Status` enum('SendingOK','SendingOKNoReport','SendingError','DeliveryOK','DeliveryFailed','DeliveryPending','DeliveryUnknown','Error') NOT NULL DEFAULT 'SendingOK',
  `StatusError` int(11) NOT NULL DEFAULT '-1',
  `TPMR` int(11) NOT NULL DEFAULT '-1',
  `RelativeValidity` int(11) NOT NULL DEFAULT '-1',
  `CreatorID` text NOT NULL,
  PRIMARY KEY (`ID`,`SequencePosition`),
  KEY `sentitems_date` (`DeliveryDateTime`),
  KEY `sentitems_tpmr` (`TPMR`),
  KEY `sentitems_dest` (`DestinationNumber`),
  KEY `sentitems_sender` (`SenderID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sentitems`
--

INSERT INTO `sentitems` (`UpdatedInDB`, `InsertIntoDB`, `SendingDateTime`, `DeliveryDateTime`, `Text`, `DestinationNumber`, `Coding`, `UDH`, `SMSCNumber`, `Class`, `TextDecoded`, `ID`, `SenderID`, `SequencePosition`, `Status`, `StatusError`, `TPMR`, `RelativeValidity`, `CreatorID`) VALUES
('2017-01-08 00:53:52', '2017-01-08 00:53:39', '2017-01-08 00:53:52', NULL, '004D00610068006100730069007300770061002000610074006100730020006E0061006D006100200041006E0064007200690061006E00200061006B0061006E00200064006100740061006E0067002000640061006C0061006D002000770061006B007400750020006B007500720061006E006700200064006100720069002000320078003200340020006A0061006D00200075006E00740075006B0020006D0065006C0061006B0075006B0061006E00200073007500720076006500690020006B006F00730074002E00200050006500720073006900610070006B0061006E0020006B006F00730074002000640065006E00670061006E0020006200610069006B002E002000530075006B0073006500730021', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'Mahasiswa atas nama Andrian akan datang dalam waktu kurang dari 2x24 jam untuk melakukan survei kost. Persiapkan kost dengan baik. Sukses!', 1, '', 1, 'SendingOKNoReport', -1, 7, 255, 'Andrian'),
('2017-01-08 01:01:58', '2017-01-08 01:01:37', '2017-01-08 01:01:58', NULL, '004D00610068006100730069007300770061002000610074006100730020006E0061006D006100200041006E0064007200690061006E002000740065006C006100680020006D0065006C0061006B0075006B0061006E002000700065006D006500730061006E0061006E0020006B006F00730074002E00200053006500740065006C006100680020007400720061006E00730061006B0073006900200062006500720068006100730069006C002000730069006C00610068006B0061006E006B0061006E002000620061006C0061007300200070006500730061006E002000640065006E00670061006E00200066006F0072006D00610074003A002000590041002E0020004B006F006E006600690072006D006100730069002000700065006D006500730061006E0061006E002000680061006E00790061002000640061006C0061006D002000770061006B00740075002000320078003200340020006A0061006D002E0020', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'Mahasiswa atas nama Andrian telah melakukan pemesanan kost. Setelah transaksi berhasil silahkankan balas pesan dengan format: YA. Konfirmasi pemesanan hanya dalam waktu 2x24 jam. ', 2, '', 1, 'SendingOKNoReport', -1, 8, 255, 'Andrian'),
('2017-01-08 01:04:03', '2017-01-08 01:03:51', '2017-01-08 01:04:03', NULL, '004D00610068006100730069007300770061002000610074006100730020006E0061006D006100200041006E0064007200690061006E002000740065006C006100680020006D0065006C0061006B0075006B0061006E002000700065006D006500730061006E0061006E0020006B006F00730074002E00200053006500740065006C006100680020007400720061006E00730061006B0073006900200062006500720068006100730069006C002000730069006C00610068006B0061006E006B0061006E002000620061006C0061007300200070006500730061006E002000640065006E00670061006E00200066006F0072006D00610074003A00200032002000590041002E0020004B006F006E006600690072006D006100730069002000700065006D006500730061006E0061006E002000680061006E00790061002000640061006C0061006D002000770061006B00740075002000320078003200340020006A0061006D002E0020', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'Mahasiswa atas nama Andrian telah melakukan pemesanan kost. Setelah transaksi berhasil silahkankan balas pesan dengan format: 2 YA. Konfirmasi pemesanan hanya dalam waktu 2x24 jam. ', 3, '', 1, 'SendingOKNoReport', -1, 9, 255, 'Andrian'),
('2017-01-08 01:11:09', '2017-01-08 01:10:37', '2017-01-08 01:11:09', NULL, '004D0061006800610073006900730077006100200041002E004E00200041006E0064007200690061006E0020006D0065006D006500730061006E0020006B006F007300740061006E00200061006E00640061002E002000530069006C00610068006B0061006E006B0061006E002000620061006C0061007300200070006500730061006E00200069006E0069002000640065006E00670061006E00200066006F0072006D00610074003A002000590041005B00730070006100730069005D0033002000640061006C0061006D002000770061006B00740075002000320078003200340020006A0061006D00200073006500740065006C006100680020007400720061006E00730061006B00730069002E', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'Mahasiswa A.N Andrian memesan kostan anda. Silahkankan balas pesan ini dengan format: YA[spasi]3 dalam waktu 2x24 jam setelah transaksi.', 4, '', 1, 'SendingOKNoReport', -1, 10, 255, 'Andrian'),
('2017-01-08 10:41:31', '2017-01-08 10:41:18', '2017-01-08 10:41:31', NULL, '00530065006C0061006D00610074', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'Selamat', 5, '', 1, 'SendingOKNoReport', -1, 11, 255, 'admin'),
('2017-01-08 10:42:06', '2017-01-08 10:41:55', '2017-01-08 10:42:06', NULL, '0067006100670061006C', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'gagal', 6, '', 1, 'SendingOKNoReport', -1, 12, 255, 'admin'),
('2017-01-13 06:24:08', '2017-01-13 06:23:47', '2017-01-13 06:24:08', NULL, '004D0061006800610073006900730077006100200041002E004E00200041006E0064007200690061006E0020006D0065006D006500730061006E0020006B006F007300740061006E00200061006E00640061002E002000530069006C00610068006B0061006E006B0061006E002000620061006C0061007300200070006500730061006E00200069006E0069002000640065006E00670061006E00200066006F0072006D00610074003A0020004400450041004C005B00730070006100730069005D00340020006A0069006B00610020007400720061006E00730061006B0073006900200062006500720068006100730069006C002E', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'Mahasiswa A.N Andrian memesan kostan anda. Silahkankan balas pesan ini dengan format: DEAL[spasi]4 jika transaksi berhasil.', 7, '', 1, 'SendingOKNoReport', -1, 13, 255, 'Andrian'),
('2017-01-13 06:25:12', '2017-01-13 06:24:48', '2017-01-13 06:25:12', NULL, '00530065006C0061006D006100740020007400720061006E00730061006B007300690020006B006F0073007400200062006500720068006100730069006C', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'Selamat transaksi kost berhasil', 8, '', 1, 'SendingOKNoReport', -1, 14, 255, 'admin'),
('2017-01-14 13:53:41', '2017-01-14 13:53:26', '2017-01-14 13:53:41', NULL, '004D0061006800610073006900730077006100200041002E004E00200041006E0064007200690061006E0020006D0065006D006500730061006E0020006B006F007300740061006E00200061006E00640061002E002000530069006C00610068006B0061006E006B0061006E002000620061006C0061007300200070006500730061006E00200069006E0069002000640065006E00670061006E00200066006F0072006D00610074003A0020004400450041004C005B00730070006100730069005D00350020006A0069006B00610020007400720061006E00730061006B0073006900200062006500720068006100730069006C002E', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'Mahasiswa A.N Andrian memesan kostan anda. Silahkankan balas pesan ini dengan format: DEAL[spasi]5 jika transaksi berhasil.', 9, '', 1, 'SendingOKNoReport', -1, -1, 255, 'Andrian'),
('2017-01-14 13:54:45', '2017-01-14 13:54:35', '2017-01-14 13:54:45', NULL, '00530065006C0061006D006100740020007400720061006E00730061006B007300690020006B006F0073007400200062006500720068006100730069006C', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'Selamat transaksi kost berhasil', 10, '', 1, 'SendingOKNoReport', -1, 17, 255, 'admin'),
('2017-01-14 13:56:20', '2017-01-14 13:55:52', '2017-01-14 13:56:20', NULL, '004D0061006800610073006900730077006100200041002E004E00200041006E0064007200690061006E0020006D0065006D006500730061006E0020006B006F007300740061006E00200061006E00640061002E002000530069006C00610068006B0061006E006B0061006E002000620061006C0061007300200070006500730061006E00200069006E0069002000640065006E00670061006E00200066006F0072006D00610074003A0020004400450041004C005B00730070006100730069005D00360020006A0069006B00610020007400720061006E00730061006B0073006900200062006500720068006100730069006C002E', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'Mahasiswa A.N Andrian memesan kostan anda. Silahkankan balas pesan ini dengan format: DEAL[spasi]6 jika transaksi berhasil.', 11, '', 1, 'SendingOKNoReport', -1, 18, 255, 'Andrian'),
('2017-01-14 13:57:24', '2017-01-14 13:57:06', '2017-01-14 13:57:24', NULL, '0054006500720069006D0061006B0061007300690068002C002000730065006D006F0067006100200062006500720075006E00740075006E00670020006C00610069006E002000770061006B00740075', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'Terimakasih, semoga beruntung lain waktu', 12, '', 1, 'SendingOKNoReport', -1, 19, 255, 'admin'),
('2017-01-16 03:04:15', '2017-01-16 03:03:46', '2017-01-16 03:04:15', NULL, '004D0061006800610073006900730077006100200041002E004E002000570075006C0061006E0020006D0065006D006500730061006E0020006B006F007300740061006E00200061006E00640061002E002000530069006C00610068006B0061006E006B0061006E002000620061006C0061007300200070006500730061006E00200069006E0069002000640065006E00670061006E00200066006F0072006D00610074003A0020004400450041004C005B00730070006100730069005D00370020006A0069006B00610020007400720061006E00730061006B0073006900200062006500720068006100730069006C002E', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'Mahasiswa A.N Wulan memesan kostan anda. Silahkankan balas pesan ini dengan format: DEAL[spasi]7 jika transaksi berhasil.', 13, '', 1, 'SendingOKNoReport', -1, 20, 255, 'Wulan'),
('2017-01-16 03:24:51', '2017-01-16 03:24:22', '2017-01-16 03:24:51', NULL, '00530065006C0061006D006100740020007400720061006E00730061006B007300690020006B006F0073007400200062006500720068006100730069006C', '+6285790567597', 'Default_No_Compression', '', '+62816124', -1, 'Selamat transaksi kost berhasil', 14, '', 1, 'SendingOKNoReport', -1, 21, 255, 'admin');

--
-- Triggers `sentitems`
--
DROP TRIGGER IF EXISTS `sentitems_timestamp`;
DELIMITER //
CREATE TRIGGER `sentitems_timestamp` BEFORE INSERT ON `sentitems`
 FOR EACH ROW BEGIN
    IF NEW.InsertIntoDB = '0000-00-00 00:00:00' THEN
        SET NEW.InsertIntoDB = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.SendingDateTime = '0000-00-00 00:00:00' THEN
        SET NEW.SendingDateTime = CURRENT_TIMESTAMP();
    END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `testimonis`
--

CREATE TABLE IF NOT EXISTS `testimonis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kost_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `komentar` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kost_id` (`kost_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `testimonis`
--

INSERT INTO `testimonis` (`id`, `kost_id`, `user_id`, `komentar`, `created_at`, `updated_at`) VALUES
(1, 2, 5, 'Ini adalah kosan saya gaess', '2017-01-18 09:47:23', '2017-01-18 09:47:23'),
(2, 3, 3, 'ini komentar saya', '2017-01-19 07:13:15', '2017-01-19 07:13:15'),
(3, 3, 3, 'dan ini komentar kesua saya', '2017-01-19 07:38:32', '2017-01-19 07:38:32'),
(4, 3, 3, 'komentar yang banyak dan berbaris-baris muncullnya kaya gimana ya?? coba aja dulu deh siapa tau tetep bagus', '2017-01-19 07:47:04', '2017-01-19 07:47:04'),
(5, 3, 3, 'coba dulua aja', '2017-01-19 08:05:31', '2017-01-19 08:05:31'),
(6, 2, 3, 'tes komentar belum pesan', '2017-01-19 08:08:36', '2017-01-19 08:08:36'),
(7, 2, 5, 'aku sudah pesan jadi bisa dong komentar', '2017-01-19 08:35:08', '2017-01-19 08:35:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_identitas` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nama_depan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nama_belakang` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` enum('admin','mahasiswa','pemilik') COLLATE utf8_unicode_ci NOT NULL,
  `jenis` enum('Pria','Wanita') COLLATE utf8_unicode_ci NOT NULL,
  `hp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_username_unique` (`username`),
  KEY `users_ibfk_1` (`level`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `no_identitas`, `nama_depan`, `nama_belakang`, `email`, `username`, `password`, `level`, `jenis`, `hp`, `foto`, `created_at`, `updated_at`, `remember_token`) VALUES
(3, 'H1L013003', 'Andrian', 'Robby', 'andrianrobbypratama@gmail.com', 'arprobibpk', '$2y$10$u6QrpCLB.Mi61IoHbhZzmOawSTUEaOWX5MOkp7DMgmb9c0FtdwTua', 'admin', 'Wanita', '089234423877', '1485257763.arprobibpk.jpg', '2016-11-28 08:36:09', '2017-01-24 16:39:28', 'jPzkhc9TNN1jIqAyjnyReiXwKG7WpYDeGO7ylnT4yXto542zY0TECYRJgyzY'),
(4, '113223344', 'Sumarno', 'Sumarno', 'sumarno04@gmail.com', 'sumarno', '$2y$10$nqb04Y8CWiuYs34IurlKieMMwImg.m9QEjnq4CX488p3dBDGzE90a', 'pemilik', 'Pria', '085790567597', '1484489059.sumarno.png', '2016-12-07 07:56:10', '2017-01-15 20:23:34', 'LKZhPDJaNa4DbXfbLKcgWCBAoXWyPu7LcbtOo0RaCJVXIZ0csSCeog4lEZXq'),
(5, 'H1L013012', 'Wulan', 'Dewi Rahmawati', 'drwwulan@gmail.com', 'drwwulan', '$2y$10$bIN99X11V0DrCAZfbEpo/.cqgRKtUS9iIJEr2VKKkTsCSqs2LNMrG', 'mahasiswa', 'Wanita', '089628325275', '1484536206.drwwulan.jpg', '2017-01-15 07:01:23', '2017-01-19 08:50:43', 'XyeUBvYPZSvZeftrCgH9cflg6yTOU6eHZTjd5pLqpD41IW6YwWb1WuBaNnEV'),
(6, 'H1L013025', 'Mokhammad', 'Mukhsin Wibowo', 'mmwibowo@gmail.com', 'mukhsin', '$2y$10$/GH9Kr8Xwz9Nrhr5.3NXSeGDF/lIGU/GsigHhXxaslk5ZSrVfyzVC', 'pemilik', 'Wanita', '085790567597', 'user.png', '2017-01-15 07:32:38', '2017-01-25 12:00:06', 'YMco8UojHNet5TO21KJldRG6M7QskOhdSXfr09aOdOM0cbezaxMzYUUO5Dr9'),
(11, 'BAPAKEADMIN', 'Admin', 'Bapake', 'admin@bapake.com', 'bapake', '$2y$10$eA5FD3rw.R3wwn9dHlVYGe0g6rsavs.mo7P7GrwbtKfgkyd7Gj23W', 'admin', 'Pria', '085790567597', 'user.png', '2017-01-18 09:24:07', '2017-01-18 09:24:07', NULL),
(12, '123456789', 'bash', 'bash', 'dfssdfsd@gmail.com', 'aaa', '$2y$10$XxR5TdzbnVBCWfHaEeTGquaE8YfJo1j93kGPNnT1QjLfdiMZXorf6', 'mahasiswa', 'Pria', '123235346', 'user.png', '2017-01-22 14:22:02', '2017-01-22 14:24:05', 'xnEBEJwxvk6ktXb1chtGOIjlsCThuTpr8SrUNd1OPdvg7yXHJ7t2le6gGsO3'),
(13, 'H1L013099', 'YUSRI', 'GOBLOK', 'fadilahsusilo@gmail.com', 'AMIR', '$2y$10$O/wcT.Be4zPsynbiYUdw7.fFEQWjxicATh0np9UPVMWUvVVYHYY0S', 'mahasiswa', 'Pria', '09863439175058', 'user.png', '2017-01-23 08:12:20', '2017-01-23 08:23:34', 'GTUUtEyCzgXdVEECQVqW6kGXI9BgTs5YVV9kZMKfMOiy4zfXGNaIkzGFiUFs'),
(14, 'H1L013030', 'a', 'b', 'ab@mail.com', 'ab', '$2y$10$s0lEaVfCx9nux99cgzYQkOvWYgCrNaQi18uZ451QkZCNQkzxQBu.a', 'mahasiswa', 'Wanita', '078499', 'user.png', '2017-01-23 09:13:00', '2017-01-23 09:13:00', NULL),
(15, '123123', 'nama', 'belakang', 'email@mail.com', 'qwerty', '$2y$10$zm767uPUXiE3PeW.2UHr1OmdhphhG4K4wNsKlCys3PLmAn1eLM2YS', '', '', '090909090', 'user.png', '2017-01-25 17:49:27', '2017-01-25 17:50:29', 'YcvJaNG6F76yTtpdwFUf4RfBCP1YokaWG9dqd0aZC2IH3WMHmgO3cXcXSDpq');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
