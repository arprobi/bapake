@extends('layouts.admin')

@section('content')
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong>Ubah User</strong> Isi identitas baru user
            </div>
            <div class="card-block">
                {!! Form::model($user, ['route' => ['user.update', $user], 'method' =>'patch', 'files' => true, 'class' => 'form-horizontal'])!!}
                    <fieldset>
                        <div class="form-group row">
                            <label for="no_identitas" class="col-lg-2 control-label">No. Identitas</label>
                            <div class="col-lg-10">
                                <input name="no_identitas" type="text" class="form-control" value="{{$user->no_identitas}}" placeholder="NIM, KTP, SIM ">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nama_depan" class="col-lg-2 control-label">Nama Depan</label>
                            <div class="col-lg-10">
                                <input name="nama_depan" type="text" class="form-control" value="{{$user->nama_depan}}" placeholder="Nama Depan">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nama_belakang" class="col-lg-2 control-label">Nama Belakang</label>
                            <div class="col-lg-10">
                                <input name="nama_belakang" type="text" class="form-control" value="{{$user->nama_belakang}}" placeholder="Nama Belakang">
                            </div>
                        </div>

                        <div class="form-group row">
                        <label for="jenis" class="col-lg-2 control-label">Jenis Kelamin</label>
                            <div class="col-lg-10">
                                <select name="jenis" class="form-control" id="jenis">
                                    <option></option>
                                    <option value="Pria">Pria</option>
                                    <option value="Wanita">Wanita</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-lg-2 control-label">Alamat Email</label>
                            <div class="col-lg-10">
                                <input name="email" type="text" class="form-control" value="{{$user->email}}" placeholder="Alamat Email">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-lg-2 control-label">Username</label>
                            <div class="col-lg-10">
                                <input name="username" type="text" class="form-control" value="{{$user->username}}" placeholder="Username">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hp" class="col-lg-2 control-label">No Telepon</label>
                            <div class="col-lg-10">
                                <input name="hp" type="number" class="form-control" value="{{$user->hp}}" placeholder="No Telepon">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="reset" class="btn btn-danger">Batal</button>
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                        </div>
                    </fieldset>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection     