@extends('layouts.admin')

@section('content')
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong>Tambah User</strong> Isi identitas user
            </div>
            <div class="card-block">
                {!! Form::open(['route' => 'user.store', 'files' => true, 'class' => 'form-horizontal'])!!}

                    <div class="form-group row {{ $errors->has('no_identitas') ? ' has-danger' : '' }}">
                        <label for="no_identitas" class="col-lg-2 control-label">No. Identitas</label>
                        <div class="col-lg-10">
                            <input name="no_identitas" type="text" class="form-control" id="no_identitas" placeholder="NIM, KTP, SIM " value="{{ old('no_identitas') }}">
                            @if ($errors->has('no_identitas'))
                            <div class="form-control-feedback">
                                {{ $errors->first('no_identitas') }}
                            </div>
                            @endif
                        </div>

                    </div>

                    <div class="form-group row {{ $errors->has('nama_depan') ? ' has-danger' : '' }}">
                        <label for="nama_depan" class="col-lg-2 control-label">Nama Depan</label>
                        <div class="col-lg-10">
                            <input name="nama_depan" type="text" class="form-control" id="nama_depan" placeholder="Nama Depan" value="{{ old('nama_depan') }}">
                            @if ($errors->has('nama_depan'))
                            <div class="form-control-feedback">
                                {{ $errors->first('nama_depan') }}
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row {{ $errors->has('nama_belakang') ? ' has-danger' : '' }}">
                        <label for="nama_belakang" class="col-lg-2 control-label">Nama Belakang</label>
                        <div class="col-lg-10">
                            <input name="nama_belakang" type="text" class="form-control" id="nama_belakang" placeholder="Nama Belakang" value="{{ old('nama_belakang') }}">
                            @if ($errors->has('nama_belakang'))
                            <div class="form-control-feedback">
                                {{ $errors->first('nama_belakang') }}
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row {{ $errors->has('jenis') ? ' has-danger' : '' }}">
                    <label for="jenis" class="col-lg-2 control-label">Jenis Kelamin</label>
                        <div class="col-lg-10">
                            <select name="jenis" class="form-control" id="jenis">
                                <option>--Pilih jenis kelamin--</option>
                                <option value="Pria" {{ (old('jenis') == 'Pria' ? "selected":"") }}>Pria</option>
                                <option value="Wanita" {{ (old('jenis') == 'Wanita' ? "selected":"") }}>Wanita</option>
                            </select>
                            @if ($errors->has('jenis'))
                            <div class="form-control-feedback">
                                {{ $errors->first('jenis') }}
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row {{ $errors->has('email') ? ' has-danger' : '' }}">
                        <label for="email" class="col-lg-2 control-label">Alamat Email</label>
                        <div class="col-lg-10">
                            <input name="email" type="text" class="form-control" id="email" placeholder="Alamat Email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                            <div class="form-control-feedback">
                                {{ $errors->first('email') }}
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row {{ $errors->has('username') ? ' has-danger' : '' }}">
                        <label for="username" class="col-lg-2 control-label">Username</label>
                        <div class="col-lg-10">
                            <input name="username" type="text" class="form-control" id="username" placeholder="Username" value="{{ old('username') }}">
                            @if ($errors->has('username'))
                            <div class="form-control-feedback">
                                {{ $errors->first('username') }}
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row {{ $errors->has('hp') ? ' has-danger' : '' }}">
                        <label for="hp" class="col-lg-2 control-label">No Telepon</label>
                        <div class="col-lg-10">
                            <input name="hp" type="number" class="form-control" id="hp" placeholder="No Telepon" value="{{ old('hp') }}">
                            @if ($errors->has('hp'))
                            <div class="form-control-feedback">
                                {{ $errors->first('hp') }}
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row {{ $errors->has('level') ? ' has-danger' : '' }}">
                        <label for="hp" class="col-lg-2 control-label">Hak Akses</label>
                        <div class="col-lg-10">
                            <input name="level" type="text" class="form-control" id="level" value="Admin" disabled="true">
                        </div>
                    </div>
             
                    <div class="form-group row">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button type="reset" class="btn btn-default">Batal</button>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection