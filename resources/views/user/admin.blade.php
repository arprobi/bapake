@extends('layouts.admin')

@section('content')
<br>
@if (session('sukses'))
<div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Selamat!</strong> {{ session('sukses') }}
</div>
@elseif (session('gagal'))
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Oops!</strong> {{ session('gagal') }}
</div>
@endif

<div class="row">
    <div class="col-lg-12">
    <h3>Tabel Data Admin</h3>
    <div class="btn-group">
        <a href="{{route ('user.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Admin</a>
    </div>
    <hr>
        <div class="card card-accent-primary">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Data Administrator
            </div>
            <div class="card-block">
                <table class="table">
                    <thead class="thead-inverse">
                        <tr>
                            <th>No Identitas</th>
                            <th>Nama </th>
                            <th>Email </th>
                            <th>No HP </th>
                            <th>Aksi </th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($users as $user)
                        <tr id="{{ $user->id }}">
                            <td>{{$user->no_identitas}}</td>
                            <td>{{$user->nama_depan}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->hp}}</td>
                            <td>
                                <a href="{{route('user.show', $user->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-eye">Lihat</i></a>
                                <button type="button" name="delete" class="btn btn-sm btn-danger btn-xs"><i class="fa fa-trash">Hapus</i>
                                </button>
                            </td>
                        </tr>
                     @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/col-->
</div>
@endsection

@section('modal')
    <div id="delete" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/user') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="id" value="">

                    <div class="modal-header">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Hapus Pengguna</h4>
                    </div>

                    <div class="modal-body">
                        <p>
                            Apakah Anda yakin ingin menghapus pengguna ini?
                        </p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" name="batal">
                            <i class="fa fa-btn fa-ban"></i> Batal
                        </button>
                        <button type="submit" class="btn btn-success" name="simpan">
                            <i class="fa fa-btn fa-trash"></i> Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- My App -->
    <script type="text/javascript">        
        $(document)
        
        .on('click', 'button[name="delete"]', function() {
            var id = $(this).parents('tr').first().attr('id');
            $('#delete input[name="id"]').val(id);
            $('#delete').removeClass('fade');
            $('#delete').show();
        })

        .on('click', 'button[name="batal"], .close-modal', function() {
            $('.modal').hide();
            $('.modal input[name="id"]').val('');
        })
        ;
    </script>
@endsection