@extends('layouts.admin')
@section('content')
<br>
<div class="row">
	<div class="card">
        <div class="card-header">
            <i class="fa fa-user"></i> Data Pengguna</div>
        <div class="card-block">
	        <div class="col-sm-3">
	            <img src="{{URL::to('/user_image/'.$user->foto)}}" width="100%">
	        </div>
	        <div class="col-sm-9"> 
	            <dl class="row">
	                <dt class="col-sm-3">ID User</dt>
	                <dd class="col-sm-9">{{$user->id}}</dd>

	                <dt class="col-sm-3">No Identitas</dt>
	                <dd class="col-sm-9">{{$user->no_identitas}}</dd>

	                <dt class="col-sm-3">Nama Depan</dt>
	                <dd class="col-sm-9">{{$user->nama_depan}}</dd>

	                <dt class="col-sm-3">Nama Belakang</dt>
	                <dd class="col-sm-9">{{$user->nama_belakang}}</dd>

	                <dt class="col-sm-3">Jenis Kelamin</dt>
	                <dd class="col-sm-9">{{$user->jenis}}</dd>

	                <dt class="col-sm-3">Username</dt>
	                <dd class="col-sm-9">{{$user->username}}</dd>

	                <dt class="col-sm-3">Email</dt>
	                <dd class="col-sm-9">{{$user->email}}</dd>

	                <dt class="col-sm-3">No Telpon</dt>
	                <dd class="col-sm-9">{{$user->hp}}</dd>

	                <dt class="col-sm-3">Username</dt>
	                <dd class="col-sm-9">{{$user->username}}</dd>

	                <dt class="col-sm-3">Sebagai</dt>
	                <dd class="col-sm-9">{{$user->level}}</dd>
	            </dl>
	        </div>
	    </div>
    </div>
</div>
@endsection