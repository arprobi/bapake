@extends('layouts.admin')

@section('content')
<br>
@if (session('sukses'))
<div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Selamat!</strong> {{ session('sukses') }}
</div>
@elseif (session('gagal'))
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Oops!</strong> {{ session('gagal') }}
</div>
@endif

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            
            <div class="card-header">
                <strong>Tambah Himpunan</strong> Isi himpunan baru
            </div>
            
            <div class="card-block">
                <div class="col-md-4">
                    <form class="form-horizontal" role="form" method="POST" action="{{url('himpunan')}}">{!! csrf_field() !!}
                    <div class="form-group {{ $errors->has('id_parameter') ? ' has-danger' : '' }}">
            	        <label class="control-label" for="variabel">Variabel</label>
                        <select name="id_parameter" class="form-control" id="variabel">
                            <option>--Pilih Variabel--</option>
                            @foreach($par as $p)
                            <option value="{{$p->id}}">{{$p->nama_parameter}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('id_parameter'))
                            <div class="form-control-feedback">
                                {{ $errors->first('id_parameter') }}
                            </div>
                        @endif
                    </div>
                     <div class="form-group {{ $errors->has('nama_himpunan') ? ' has-danger' : '' }}">
                    	<label class="control-label" for="nama">Nama Himpunan</label>
                        <input name="nama_himpunan" type="text" class="form-control" id="nama" placeholder="Nama Himpunan" value="{{ old('nama_himpunan') }}">
                        @if ($errors->has('nama_himpunan'))
                            <div class="form-control-feedback">
                                {{ $errors->first('nama_himpunan') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('nilai_1') ? ' has-danger' : '' }}">
                    	<label class="control-label" for="nil1">Domain 1</label>
                        <input name="nilai_1" type="text" class="form-control" id="nilai_1" placeholder="Domain 1" value="{{ old('nilai_1') }}">
                        @if ($errors->has('nilai_1'))
                            <div class="form-control-feedback">
                                {{ $errors->first('nilai_1') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('nilai_2') ? ' has-danger' : '' }}">
                    	<label class="control-label" for="nil2">Domain 2</label>
                        <input name="nilai_2" type="text" class="form-control" id="nilai_2" placeholder="Domain 2" value="{{ old('nilai_2') }}">
                        @if ($errors->has('nilai_2'))
                            <div class="form-control-feedback">
                                {{ $errors->first('nilai_2') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('nilai_3') ? ' has-danger' : '' }}">
                    	<label class="control-label" for="nil3">Domain 3</label>
                        <input name="nilai_3" type="text" class="form-control" id="nilai_3" placeholder="Domain 3" value="{{ old('nilai_3') }}">
                        @if ($errors->has('nilai_3'))
                            <div class="form-control-feedback">
                                {{ $errors->first('nilai_3') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('nilai_4') ? ' has-danger' : '' }}">
                    	<label class="control-label" for="nil4">Domain 4</label>
                        <input name="nilai_4" type="text" class="form-control" id="nilai_4" placeholder="Domain 4" value="{{ old('nilai_4') }}">
                        @if ($errors->has('nilai_4'))
                            <div class="form-control-feedback">
                                {{ $errors->first('nilai_4') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                    	<button type="submit" class="btn btn-info"><i class="fa fa-save"> Simpan</i></button>
                        <button type="button" id="prev" class="btn btn-success"><i class="fa fa-eye"> Preview</i></button> 
                    </div>
                    </form>
                </div>

                <div class="col-md-8">
                    <canvas id="linechart" height="500" width="680">
                        
                    </canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong>Tabel Himpunan</strong>
            </div>
            <div class="card-block">
                <table class="table">
                	<thead class="thead-inverse">
                		<tr>
                            <th>Nomor</th>
                			<th>Nama Variable</th>
                			<th>Himpunan</th>
                			<th>Nilai 1</th>
                			<th>Nilai 2</th>
                			<th>Nilai 3</th>
                			<th>Nilai 4</th>
                			<th>Aksi</th>
                		</tr>
                	</thead>
                	<tbody>
                	@foreach ($himpunan as $num => $him)
                		<tr id="{{ $him->id }}">
                            <td>{{$num+1}}</td>
                			<td name="id_parameter" id="{{$him->id_parameter}}">
                                {{$him->parameter->nama_parameter}}
                            </td>
                			<td name="nama_himpunan">{{$him->nama_himpunan}}</td>
                			<td name="nilai_1">{{$him->nilai_1}}</td>
                			<td name="nilai_2">{{$him->nilai_2}}</td>
                			<td name="nilai_3">{{$him->nilai_3}}</td>
                			<td name="nilai_4">{{$him->nilai_4}}</td>
                			<td>
                				<button type="button" name="edit" class="btn btn-warning btn-sm">
                                    <i class="fa fa-btn fa-edit"></i>Ubah
                                </button>
                				<button type="button" name="delete" class="btn btn-danger btn-sm">
                                    <i class="fa fa-btn fa-trash"></i>Hapus
                                </button>
                			</td>
                		</tr>
                	@endforeach
                	</tbody>
                </table>
                <nav aria-label="...">
                    <ul class="pagination pagination-sm">
                        <li class="page-item">
                            <a class="page-link" href="{{$himpunan->previousPageUrl()}}">Sebelumnya</a>
                        </li>
                        
                        <li class="page-item">
                            <a class="page-link" href="{{$himpunan->nextPageUrl()}}">Berikutnya</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
    <div id="put" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/himpunan') }}"> {!! csrf_field() !!}

                    <input type="hidden" name="_method" value="put">
                    <input type="hidden" name="id" value="">

                    <div class="modal-header">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Ubah Himpunan</h4>
                    </div>

                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-3 control-label">Nama Parameter</label>
                            <div class="col-md-9">
                                <select name="id_parameter" class="form-control" id="variabel">
                                    <option></option>
                                    @foreach($par as $v)
                                    <option value="{{$v->id}}">{{$v->nama_parameter}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-md-3 control-label">Nama Himpunan</label>
                            <div class="col-md-9">
                                <input name="nama_himpunan" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 control-label">Nilai 1</label>
                            <div class="col-md-9">
                                <input name="nilai_1" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 control-label">Nilai 2</label>
                            <div class="col-md-9">
                                <input name="nilai_2" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 control-label">Nilai 3</label>
                            <div class="col-md-9">
                                <input name="nilai_3" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('nilai_4') ? ' has-danger' : '' }}">
                            <label class="col-md-3 control-label">Nilai 4</label>

                            <div class="col-md-9">
                                <input name="nilai_4" type="text" class="form-control">
                                @if ($errors->has('nilai_4'))
                                    <div class="form-control-feedback">
                                        {{ $errors->first('nilai_4') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" name="batal">
                            <i class="fa fa-btn fa-ban"></i> Batal
                        </button>
                        <button type="submit" class="btn btn-success" name="simpan">
                            <i class="fa fa-btn fa-save"></i> Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="delete" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/himpunan') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="id" value="">

                    <div class="modal-header">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Hapus Himpunan</h4>
                    </div>

                    <div class="modal-body">
                        <p>
                            Apakah Anda yakin ingin menghapus himpunan ini?
                        </p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" name="batal">
                            <i class="fa fa-btn fa-ban"></i> Batal
                        </button>
                        <button type="submit" class="btn btn-success" name="simpan">
                            <i class="fa fa-btn fa-trash"></i> Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- My App -->
    <script type="text/javascript">
        $(document)
        
        .on('click', 'button[name="edit"]', function() {
            var id = $(this).parents('tr').first().attr('id'); //get id parameter
            var id_parameter = $(this).parents('tr').first().find('td[name="id_parameter"]').attr('id');
            var nama_himpunan = $(this).parents('tr').first().find('td[name="nama_himpunan"]').text();
            var nilai_1= $(this).parents('tr').first().find('td[name="nilai_1"]').text();
            var nilai_2= $(this).parents('tr').first().find('td[name="nilai_2"]').text();
            var nilai_3= $(this).parents('tr').first().find('td[name="nilai_3"]').text();
            var nilai_4= $(this).parents('tr').first().find('td[name="nilai_4"]').text();
            $('#put input[name="id"]').val(id);
            $('#put select[name="id_parameter"]').val(id_parameter);
            $('#put input[name="nama_himpunan"]').val(nama_himpunan);
            $('#put input[name="nilai_1"]').val(nilai_1);
            $('#put input[name="nilai_2"]').val(nilai_2);
            $('#put input[name="nilai_3"]').val(nilai_3);
            $('#put input[name="nilai_4"]').val(nilai_4);
            $('#put').removeClass('fade');
            $('#put').show();
        })

        .on('click', 'button[name="delete"]', function() {
            var id = $(this).parents('tr').first().attr('id');
            $('#delete input[name="id"]').val(id);
            $('#delete').removeClass('fade');
            $('#delete').show();
        })

        .on('click', 'button[name="batal"], .close-modal', function() {
            $('.modal').hide();
            $('.modal input[name="id"]').val('');
            $('.modal input[name="id_parameter"]').val('');
            $('.modal input[name="nama_himpunan"]').val('');
            $('.modal input[name="nilai_1"]').val('');
            $('.modal input[name="nilai_2"]').val('');
            $('.modal input[name="nilai_3"]').val('');
            $('.modal input[name="nilai_4"]').val('');
        })
        ;
    </script>
@endsection