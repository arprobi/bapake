@extends('layouts.admin')

@section('content')
<br>
<div class="row">
	<div class="card card-block">
		<h4 class="card-title">Preview Jumlah Data</h4>
		<div class="col-sm-2">
			<div class="callout callout-info">
				<strong class="text-info">Pemilik Kost</strong><br>
				<strong class="h4 text-info">{{$pemilik}}</strong>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="callout callout-danger">
				<strong class="text-danger">Mahasiswa</strong><br>
				<strong class="h4 text-danger">{{$mahasiswa}}</strong>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="callout callout-warning">
				<strong class="text-warning">Kost</strong><br>
				<strong class="h4 text-warning">{{$kost}}</strong>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="callout callout-success">
				<strong class="text-success">Variabel</strong><br>
				<strong class="h4 text-success">{{$variabel}}</strong>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="callout callout-secondary">
				<strong class="text-muted">Himpunan</strong><br>
				<strong class="h4 text-muted">{{$himpunan}}</strong>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="callout callout-primary">
				<strong class="text-primary">Data Pesan</strong><br>
				<strong class="h4 text-primary">{{$pesan}}</strong>
			</div>
		</div>
	</div>
</div>
@endsection
