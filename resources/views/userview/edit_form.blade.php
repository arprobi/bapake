@extends('layouts.app')
@section('content')
<br>
<div class="row">
    <div class="col-lg-3">
        <div class="card">
            <div class="card-header">
                <strong>Ubah Password</strong>
            </div>
            <div class="card-block">
                <form action="{{ url('/gantipassword') }}" method="POST">
                {!! csrf_field() !!}
                    <div class="form-group {{ $errors->has('old_password') ? ' has-danger' : '' }}">
                        <label>Password Lama</label>
                        <input type="password" name="password" class="form-control" placeholder="Masukkan password lama" value="{{ old('password') }}">
                        @if ($errors->has('password'))
                            <div class="form-control-feedback">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('new_password') ? ' has-danger' : '' }}">
                        <label>Password Baru</label>
                        <input type="password" name="new_password" class="form-control" placeholder="Masukkan password baru" value="{{ old('new_password') }}">
                        @if ($errors->has('new_password'))
                            <div class="form-control-feedback">
                                {{ $errors->first('new_password') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('new_password_confirmation') ? ' has-danger' : '' }}">
                        <label>Konfirmasi Password</label>
                        <input type="password" name="new_password_confirmation" class="form-control" placeholder="Masukkan password baru" value="{{ old('new_password_confirmation') }}">
                        @if ($errors->has('password_confirmation'))
                            <div class="form-control-feedback">
                                {{ $errors->first('new_password_confirmation') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-9">
        @if (session('sukses'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Selamat!</strong> {{ session('sukses') }}
        </div>
        @elseif (session('gagal'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Oops!</strong> {{ session('gagal') }}
        </div>
        @endif

        <div class="card card-accent-success">
            <div class="card-header">
                <i class="fa fa-user"></i> Ubah Profil
            </div>
            <div class="card-block">
            	<form method="POST" action="{{ url('/update_profil') }}" role="form">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$user->id}}">
                <div class="form-group {{ $errors->has('no_identitas') ? ' has-danger' : '' }}">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-credit-card"></i>
                        </span>
                        <input name="no_identitas" type="text" class="form-control" placeholder="No Identitas (KTM, KTP, SIM, dll)" value="{{ $user->no_identitas }}">
                    </div>
                    @if ($errors->has('no_identitas'))
                        <div class="form-control-feedback">
                            {{ $errors->first('no_identitas') }}
                        </div>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('nama_depan') ? ' has-danger' : '' }}">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i>
                        </span>
                        <input name="nama_depan" type="text" class="form-control" placeholder="Nama Depan" value="{{ $user->nama_depan }}">
                    </div>
                    @if ($errors->has('nama_depan'))
                        <div class="form-control-feedback">
                            {{ $errors->first('nama_depan') }}
                        </div>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('nama_belakang') ? ' has-danger' : '' }}">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i>
                        </span>
                        <input name="nama_belakang" type="text" class="form-control" placeholder="Nama Belakang" value="{{ $user->nama_belakang }}">
                    </div>
                    @if ($errors->has('nama_belakang'))
                        <div class="form-control-feedback">
                            {{ $errors->first('nama_belakang') }}
                        </div>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('email') ? ' has-danger' : '' }}">
                    <div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type="email" name="email" class="form-control" placeholder="Alamat Email" value="{{ $user->email }}">
                    </div>
                    @if ($errors->has('email'))
                        <div class="form-control-feedback">
                            {{ $errors->first('email') }}
                        </div>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('username') ? ' has-danger' : '' }}">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-user"></i>
                        </span>
                        <input name="username" type="text" class="form-control" placeholder="Username" value="{{ $user->username }}">
                    </div>
                    @if ($errors->has('username'))
                        <div class="form-control-feedback">
                            {{ $errors->first('username') }}
                        </div>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('jenis') ? ' has-danger' : '' }}">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-venus-mars"></i>
                        </span>
                        <select name="jenis" type="text" class="form-control">
                            @if($user->jenis == 'Pria')
                            <option value="Pria" selected="true">Pria</option>
                            <option value="Wanita">Wanita</option>
                            @else
                            <option value="Pria">Pria</option>
                            <option value="Wanita" selected="true">Wanita</option>
                            @endif
                        </select>
                    </div>
                    @if ($errors->has('jenis'))
                        <div class="form-control-feedback">
                            {{ $errors->first('jenis') }}
                        </div>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('hp') ? ' has-danger' : '' }}">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-phone"></i>
                        </span>
                        <input name="hp" type="text" class="form-control" placeholder="No Telepon/HP" value="{{ $user->hp }}">
                    </div>
                    @if ($errors->has('hp'))
                        <div class="form-control-feedback">
                            {{ $errors->first('hp') }}
                        </div>
                    @endif
                </div>

                    <button type="submit" class="btn btn-success"><i class="fa fa-edit"></i> Ubah</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection