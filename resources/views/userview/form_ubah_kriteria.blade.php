@extends('layouts.app')

@section('content')
<br>
<div class="row">
    <div class="animated fadeIn">
    <!-- Main Text -->
    <!-- Side Column -->
        <div class="col-md-3">
            <div class="card">
                <div class="card-block">
                    <h3>Pencarian Kost :</h3>
                    <hr>
                    <strong>FUZZY TAHANI :</strong>
                    <p>Pencarian kost ini dilakukan dengan proses penghitungan fuzzy</p>
                    <p>
                        Masukkan setiap nilai kriteria dengan masing-masing himpunannya. Apabila anda tidak ingin memasukkan beberapa kriteria untuk memilih kost, anda dapat mengosongkan field tersebut.
                    </p>
                </div>
            </div>
        </div>
        <!-- End Side Column -->
        <div class="col-md-9">
            <div class="card card-accent-primary">
                <div class="card-header">
                    <strong>Ubah Kriteria Kost
                </div>
                <div class="card-block">
                    <form class="form-horizontal" method="POST" action="{{ url ('/lengkapi')}}">
                        {{ csrf_field() }}
                            @foreach ($relasis as $relasi)
                            <div class="form-group row">
                                <label class="col-md-2 form-control-label" for="select">
                                    {{$relasi->parameter->nama_parameter}}
                                </label>
                                <div class="col-md-9">
                                    <input type="hidden" name="id_kost" value="{{$relasi->id_kost}}">
                                    <input name="id_parameter[]" type="hidden" value="{{$relasi->parameter->id}}">
                                    <input name="nilai[]" id="nilai" type="text" class="form-control" value="{{$relasi->nilai_parameter}}">
                                </div>
                            </div>
                            @endforeach
                            <div class="form-group row">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

    // $(document).ready(function(){
    //     $('#jumlah_kamar').autoNumeric();
    // });

    var tanpa_rupiah = document.getElementById('nilai');
    tanpa_rupiah.addEventListener('keyup', function(e)
    {
        tanpa_rupiah.value = formatRupiah(this.value);
    });
    
    tanpa_rupiah.addEventListener('keydown', function(event)
    {
        limitCharacter(event);
    });

    //Fungsinya
    function formatRupiah(bilangan, prefix)
    {
        var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
            split   = number_string.split(','),
            sisa    = split[0].length % 3,
            rupiah  = split[0].substr(0, sisa),
            ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    
    function limitCharacter(event)
    {
        key = event.which || event.keyCode;
        if ( key != 188 // Comma
             && key != 8 // Backspace
             && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
             && (key < 48 || key > 57) // Non digit
             // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
            ) 
        {
            event.preventDefault();
            return false;
        }
    }

</script>
@endsection