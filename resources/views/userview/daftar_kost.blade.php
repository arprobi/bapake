@extends('layouts.app')

@section('content')
<br>
<div class="row">
    <div class="animated fadeIn">
    <!-- Side Column -->
        <div class="col-md-3">
            <div class="card card-block">
                <h3>Kost Bapak'e</h3>
                <hr>
                <strong>IINFORMASI KOST :</strong>
                <ul>
                    <li>Jenis Kost</li>
                    <li>Harga</li>
                    <li>Fasilitas</li>
                    <li>Kontak</li>
                </ul>
                <hr>
                <strong>BOOKING KOST</strong>
                <p>Anda dapat melakukan booking kost melalui sistem</p>
            </div>
        </div>
        <!-- End Side Column -->
        <!-- Main Text -->
        
        <div class="col-md-9">
        <div class="card card-block">
            <div class="col-lg-12" align="right" style="padding-bottom: 10px">
                <div class="btn-group">
                    <a href="{{url ('listkost/all')}}" type="button" class="btn btn-secondary">
                        <i class="fa fa-list"></i> Semua
                    </a>
                    <a href="{{url ('listkost/Pria')}}" type="button" class="btn btn-primary">
                        <i class="fa fa-male"> Pria</i>
                    </a>
                    <a href="{{url ('listkost/Wanita')}}" type="button" class="btn btn-danger">
                        <i class="fa fa-female"> Wanita</i>
                    </a>
                </div>
            </div>

            @foreach($kost as $view)
            <div class="col-xs-6 col-lg-4">
                <div class="card">
                    <div class="card-block">
                        <h5 align="center">{{$view->nama_kost}}</h5>
                        <img src="{{URL::to('/kost_image/'.$view->foto_1)}}" width="100%" class="card-img">
                        <hr>
                        
                        <p class="card-text">{{str_limit($view->alamat_kost, 40)}}</p>
                        
                        @if($view->jenis_kost=='Pria')
                        <p class="card-text">
                            <i class="fa fa-male text-success">&nbsp;{{$view->jenis_kost}}</i>&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-circle">&nbsp;K.M {{$view->kamar_mandi}}</i>
                        </p>

                        @else
                        <p class="card-text">
                            <i class="fa fa-female text-danger">&nbsp;{{$view->jenis_kost}}</i>&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-circle">&nbsp;K.M {{$view->kamar_mandi}}</i>
                        </p>
                        @endif

                        @if($view->sisa_kamar==0)
                        <p class="card-text">
                            <strong class="text-danger">Kamar Penuh</strong>
                        </p>
                        @else
                        <p class="card-text">
                            <strong class="text-primary">Sisa Kamar : {{$view->sisa_kamar}} </strong>
                        </p>
                        @endif
                        
                        <a href="{{url ('/detail', $view->id)}}" class="btn btn-success pull-right  "><i class="fa fa-angle-right float-xs-right font-lg"> Lihat Kost</i></a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        </div>
    </div>
</div>

@endsection
