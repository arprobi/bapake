@extends('layouts.app')

@section('content')
<br>
<div class="row">
    <div class="animated fadeIn">
        <!-- Main Text -->
        <!-- Side Column -->
        <div class="col-md-3">
            <div class="card">
                <div class="card-block">
                    <h3>Pencarian Kost :</h3>
                    <hr>
                    <strong>FUZZY TAHANI :</strong>
                    <p>Pencarian kost ini dilakukan dengan proses penghitungan fuzzy</p>
                    <p>
                        Masukkan setiap nilai kriteria dengan masing-masing himpunannya. Apabila anda tidak ingin memasukkan beberapa kriteria untuk memilih kost, anda dapat mengosongkan field tersebut.
                    </p>
                </div>
            </div>
        </div>
        <!-- End Side Column -->
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <strong>Pilih kriteria yang di pertimbangkan </strong>
                </div>
                <div class="card-block">
                    <form method="POST" action="{{ url('/cari')}}" class="form-horizontal ">
                    {!! csrf_field() !!}
                    @foreach ($parameter as $var)
                        <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="select">{{$var->nama_parameter}}</label>
                            <div class="col-md-10">
                                <select id="select" name="cari[]" class="form-control" size="1">
                                    <option value="0">-- Pilih Himpunan ---</option>
                                    @foreach ($var->himpunan as $him)
                                    <option value="{{$him->id}}">{{$him->nama_himpunan}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endforeach
                        <div class="form-group row">
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-dot-circle-o"></i> Cari
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
