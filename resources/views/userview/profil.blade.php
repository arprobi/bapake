@extends('layouts.app')
@section('content')
<br>
<div class="row">
    <div class="col-lg-3">
        <div class="card">
            <div class="card-header">
                <strong>Ubah Password</strong>
            </div>
            <div class="card-block">
                <form action="{{ url('/gantipassword') }}" method="POST">
                {!! csrf_field() !!}
                    <div class="form-group {{ $errors->has('old_password') ? ' has-danger' : '' }}">
                        <label>Password Lama</label>
                        <input type="password" name="password" class="form-control" placeholder="Masukkan password lama" value="{{ old('password') }}">
                        @if ($errors->has('password'))
                            <div class="form-control-feedback">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('new_password') ? ' has-danger' : '' }}">
                        <label>Password Baru</label>
                        <input type="password" name="new_password" class="form-control" placeholder="Masukkan password baru" value="{{ old('new_password') }}">
                        @if ($errors->has('new_password'))
                            <div class="form-control-feedback">
                                {{ $errors->first('new_password') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('new_password_confirmation') ? ' has-danger' : '' }}">
                        <label>Konfirmasi Password</label>
                        <input type="password" name="new_password_confirmation" class="form-control" placeholder="Masukkan konfirmasi password" value="{{ old('new_password_confirmation') }}">
                        @if ($errors->has('new_password_confirmation'))
                            <div class="form-control-feedback">
                                {{ $errors->first('new_password_confirmation') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-9">
        @if (session('sukses'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Selamat!</strong> {{ session('sukses') }}
        </div>
        @elseif (session('gagal'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Oops!</strong> {{ session('gagal') }}
        </div>
        @endif

        <div class="card">
            <div class="card-header">
                <i class="fa fa-user"></i> Data Profil Pengguna 
                <small class="pull-right">
                    <a href="{{url ('/ubah_profil')}}" class="btn btn-sm btn-success" align="right"><i class="fa fa-edit"></i>&nbsp; Sunting</a>
                </small>
            </div>
            <div class="card-block">
                <div class="col-sm-3">
                    <img src="{{URL::to('/user_image/'.$user->foto)}}" width="100%">
                    <form action="{{ url ('/foto_profil')}}" method="POST" enctype="multipart/form-data">{{ csrf_field() }}
                        <div class="form-group">
                            <label>Unggah foto</label>
                            <input type="file" name="foto" class="form-control" required>
                            <input type="hidden" name="id" value="{{Auth::user()->id}}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-sm btn-primary">
                                <i class="fa fa-upload"></i> Unggah
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-sm-9">
                    
                    <dl class="row">
                        <dt class="col-sm-3">ID User</dt>
                        <dd class="col-sm-9">{{$user->id}}</dd>

                        <dt class="col-sm-3">No Identitas</dt>
                        <dd class="col-sm-9">{{$user->no_identitas}}</dd>

                        <dt class="col-sm-3">Nama Depan</dt>
                        <dd class="col-sm-9">{{$user->nama_depan}}</dd>

                        <dt class="col-sm-3">Nama Belakang</dt>
                        <dd class="col-sm-9">{{$user->nama_belakang}}</dd>

                        <dt class="col-sm-3">Jenis Kelamin</dt>
                        <dd class="col-sm-9">{{$user->jenis}}</dd>

                        <dt class="col-sm-3">Username</dt>
                        <dd class="col-sm-9">{{$user->username}}</dd>

                        <dt class="col-sm-3">Email</dt>
                        <dd class="col-sm-9">{{$user->email}}</dd>

                        <dt class="col-sm-3">No Telpon</dt>
                        <dd class="col-sm-9">{{$user->hp}}</dd>

                        <dt class="col-sm-3">Username</dt>
                        <dd class="col-sm-9">{{$user->username}}</dd>
                    </dl>
                </div>
                <?php 
                    $cek_pesan = $user->pesan()->where('user_id', Auth::user()->id)->first();
                ?>
                @if(!empty($cek_pesan))
                <table class="table tabel-hover">
                    <thead>
                        <th>ID Pemesanan</th>
                        <th>Kost tujuan</th>
                        <th>Tanggal Pemesanan</th>
                        <th>Status Pesan</th>
                    </thead>
                    @foreach($user->pesan as $pesan)
                    <tr>
                        <td>{{$pesan->id}}</td>
                        <td>{{$pesan->kost->nama_kost}}</td>
                        <td>{{$pesan->created_at}}</td>
                        @if($pesan->status == 'Sepakat')
                        <td><i class="fa fa-check text-success"> {{$pesan->status}}</i></td>
                        @elseif($pesan->status == 'Menunggu')
                        <td><i class="fa fa-circle-o text-warning"> {{$pesan->status}}</i></td>
                        @else
                        <td><i class="fa fa-close text-danger"> {{$pesan->status}}</i></td>
                        @endif
                    </tr>
                    @endforeach
                </table>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection