@extends('layouts.app')

@section('content')
<div class="row">
    <div class="animated fadeIn">
        <div class="col-md-12">
            <div class="col-xs-6 col-lg-12">
                <div class="card" align="center" style="box-shadow: 0 0 1px 2px #e6e6e6; ">
                    <img src="{{URL::to('/assets/img/banner.jpg')}}" class="img-fluid" alt="Responsive image" width="100%">
                    <div class="card-block">
                        <p class="lead">
                            <strong>BAPAKE,</strong> menyajikan segala informasi mengenai kost lengkap yang ada di seluruh lingkungan kampus Fakultas Teknik USOED Purbalingga. Data kost yang ada dalam Bapake di unggah langsung oleh pemilik kost sehingga informasi yang dimuat akan lebih rinci. Informasi yang disampaikan adalah foto penampakan dari kost tersebut, alamat kost lengkap, nama pemilik, jenis kost, jarak kost dari kampus, jumlah kamar kosong, dan lain-lain
                        </p>
                        <p class="lead"><strong>GRATISSS!!!</strong></p>
                    </div>
                </div>
            </div>

            <div class="text-xs-center">
                <figure class="figure">
                    @if (session('sukses'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Selamat!</strong> {{ session('sukses') }}
                    </div>
                    @elseif (session('gagal'))
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Oops!</strong> {{ session('gagal') }}
                    </div>
                    @endif
                    <div class="text-xs-center">
                        <hr>
                        <h5 style="padding-bottom: 10px">Anda mempunyai kamar kost?</h5>
                        <a href="{{url ('addkost')}}" class="btn btn-lg btn-success">Daftakan disini</a>
                        <hr>
                    </div>

                </figure>
            </div>

            <div class="card card-block" style="box-shadow: 0 0 1px 2px #e6e6e6; ">
                <div align="center">
                    <p><h5>Kost Baru </h5></p>
                    <hr>
                </div>
                @foreach($kost as $view)
                <div class="col-xs-6 col-lg-4">
                    <div class="card">
                        <img src="{{URL::to('/kost_image/'.$view->foto_1)}}" width="100%" class="img-rounded">
                        <div class="card-block">
                            <h4 class="card-title">{{$view->nama_kost}}</h4>
                            <p class="card-text">{{str_limit($view->alamat_kost,40)}}</p>
                            <a href="{{url ('/detail', $view->id)}}">
                                <button  class="btn btn-info">Lihat Kost</button>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

</div>
@endsection
