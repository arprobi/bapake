@extends('layouts.app')

@section('content')
<br>
<div class="row">
    <div class="animated fadeIn">
    <!-- Side Column -->
        <div class="col-md-3">
            <div class="card">
                <div class="card-block">
                    <h3>Kost Bapake</h3>
                    <hr>
                    <strong>Catatan Kost :</strong>
                    <p>Setelah anda mendaftakan kost, harap untuk melengkapi identitas yang nantinya digunakan sebagai parameter dalam pencarian kost, silahkan klik tombol <i class="text-info"><strong>Isi Kriteria</strong></i> untuk mengisi kelengkapan detail kost</p>
                    <hr>
                    <p>Jika ada penambahan kriteria dari admin, anda akan secara otomatis akan mendapatkan SMS pemberitahuan, dan segera lengkapi kriteria yang baru tersebut sesuai dengan kost anda.</p>
                </div>
            </div>
        </div>
        <!-- End Side Column -->
        <!-- Main Text -->
        <div class="col-md-9">
        @if (session('sukses'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Selamat!</strong> {{ session('sukses') }}
        </div>
        @elseif (session('gagal'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Oops!</strong> {{ session('gagal') }}
        </div>
        @endif
        
        <div class="col-xs-6 col-lg-12">
            <h3>Daftar Kostku : </h3>
            <a href="{{url ('/addkost')}}" class="btn btn-primary"><i class="fa fa-plus">&nbsp;</i>Tambah Kost</a>
            <hr>
        </div>

            @foreach($kost as $view)
            <?php 
                $nilai = $view->relasi()->where('nilai_parameter', 0)->first();
            ?>
            
            <div class="col-xs-6 col-lg-4">
                <div class="card">
                    <div class="card-block p-1 clearfix">
                        <img src="{{URL::to('/kost_image/'.$view->foto_1)}}" width="255">
                    </div>
                    
                    <div id="{{$view->id}}" class="card-footer px-1 py-h">
                        @if(empty($nilai))
                        <h5 align="center">{{$view->nama_kost}}</h5>
                        @else                        
                        <h5 align="center">{{$view->nama_kost}} <i class="fa fa-exclamation-circle text-danger"></i></h5>
                        @endif
                        <a href="{{url ('/detail', $view->id)}}" class="btn btn-sm btn-block btn-success"><i class="fa fa-eye pull-right"></i> Lihat Detail</a>
                        
                        <a href="{{url ('/edit_kost', $view->id)}}" class="btn btn-sm btn-block btn-warning"><i class="fa fa-edit pull-right"></i> Ubah Detail</a>
                        
                        @if(count($view->relasi) < 1)
                        <a href="{{url ('/lengkapi', $view->id)}}" class="btn btn-sm btn-block btn-info"><i class="fa fa-pencil pull-right"></i> Isi Kriteria</a>
                        @else
                        <a href="{{url ('/ubahkriteria', $view->id)}}" class="btn btn-sm btn-block btn-primary active"><i class="fa fa-edit pull-right"></i> Ubah Kriteria</a>
                        @endif
                        
                        <button name="delete" class="btn btn-sm btn-block btn-danger">
                            <i class="fa fa-trash pull-right"></i> Hapus Kost
                        </button>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

@section('modal')
    <div id="delete" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/hapuskost') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="id" value="">

                    <div class="modal-header">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Hapus Himpunan</h4>
                    </div>

                    <div class="modal-body">
                        <p>
                            Apakah Anda yakin ingin menghapus kost ini?
                        </p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" name="batal">
                            <i class="fa fa-btn fa-ban"></i> Batal
                        </button>
                        <button type="submit" class="btn btn-success" name="simpan">
                            <i class="fa fa-btn fa-trash"></i> Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- My App -->
    <script type="text/javascript">        
        $(document)

        .on('click', 'button[name="delete"]', function() {
            var id = $(this).parents('div').first().attr('id');
            $('#delete input[name="id"]').val(id);
            $('#delete').removeClass('fade');
            $('#delete').show();
        })

        .on('click', 'button[name="batal"], .close-modal', function() {
            $('.modal').hide();
            $('.modal input[name="id"]').val('');
        })
        ;
    </script>
@endsection

