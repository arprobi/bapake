@extends('layouts.app')

@section('content')
<br>
<div class="row">
    <div class="animated fadeIn">
    <!-- Side Column -->
        <div class="col-md-3">
            <div class="card card-accent-danger">
                <div class="card-block">
                    <h3>Kost Bapak'e</h3>
                    <hr>
                    <strong>HASIL PENCARIAN KOST :</strong>
                    <ul>
                        <li>Jenis Kost</li>
                        <li>Harga</li>
                        <li>Fasilitas</li>
                        <li>Kontak</li>
                    </ul>
                    <hr>
                    <strong>BOOKING KOST</strong>
                    <p>Anda dapat melakukan booking kost melalui sistem</p>
                </div>
            </div>
        </div>
        <!-- End Side Column -->
        <!-- Main Text -->
        <div class="col-md-9">
        <h3>Hasil Pencarian</h3>
        <hr>
            @foreach($hasil as $has)
            @if($has['nilai'] >= '0.4')
            <div class="col-xs-6 col-lg-4">
                <div class="card card-accent-primary">
                    <div class="card-block p-1 clearfix">
                        <h5 align="center">{{$has['kost']['nama_kost']}}</h5>
                        <img src="{{URL::to('/kost_image/'.$has['kost']['foto_1'])}}" width="255">
                        <hr>
                        <p class="card-text text-info">Nilai Rekomendasi : {{$has['nilai'] * 100 }}%</p>
                        <p class="card-text">{{str_limit($has['kost']['alamat_kost'], 40)}}</p>
                        @if($has['kost']['jenis_kost']=='Pria')
                            <p>
                                <i class="fa fa-male text-success">&nbsp;{{$has['kost']['jenis_kost']}}</i>&nbsp;&nbsp;&nbsp;&nbsp;
                                <i class="fa fa-circle">&nbsp;Kmr.Mandi {{$has['kost']['kamar_mandi']}}</i>
                            </p>

                        @else
                            <p>
                                <i class="fa fa-male text-danger">&nbsp;{{$has['kost']['jenis_kost']}}</i>&nbsp;&nbsp;&nbsp;&nbsp;
                                <i class="fa fa-circle">&nbsp;Kmr.Mandi {{$has['kost']['kamar_mandi']}}</i>
                            </p>
                        @endif
                        @if($has['kost']['sisa_kamar']==0)
                        <p class="card-text">
                            <strong class="text-danger">Kamar Kosong</strong>
                        </p>
                        @else
                        <p class="card-text">
                            <strong class="text-primary">Sisa Kamar : {{$has['kost']['sisa_kamar']}} </strong>
                        </p>
                        @endif
                        <a href="{{url ('/detail', $has['kost']['id'])}}" class="btn btn-sm btn-block btn-success">Lihat Kost<i class="fa fa-angle-right float-xs-right font-lg"></i></a>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>

@endsection
