@extends('layouts.app')
@section('content')

<br>
<div class="row">
	<div class="col-lg-3">
		<div class="card card-block">
		<table class="table table-hover">
        	<thead class="thead-inverse">
            	<th class="text-xs-center"><i class="icon-people"></i></th>
            	<th class="text-xs-left"><i class="fa fa-sticky-note-o"></i> Komentar</th>
        	</thead>
        	<tbody>
        	@foreach($testimoni as $komentar)
        		<tr id="{{ $komentar->id }}">
        			<td class="text-xs-center">
        				<div class="small"><strong>{{$komentar->user->username}}</strong></div>
        				<div class="avatar">
        					<img src="{{URL::to('/user_image/'.$komentar->user->foto)}}" class="img-avatar">
        				</div>
        			</td>
        			<td>
        				<div><strong>{{$komentar->komentar}}</strong></div>
        				<span class="small text-primary">{{$komentar->created_at}}</span>
        				@if(Auth::user()->level!='pemilik')
                        <div class="pull-right">
                        @if(($komentar->user->id == Auth::user()->id) || (Auth::user()->level == 'admin'))
        					<button type="button" name="delete">
        						<i class="fa fa-trash"></i>
        					</button>
                        @endif
        				</div>

                        @endif
        			</td>
        		</tr>
        	@endforeach
        	</tbody>
        </table>
        @if(Auth::user()->level != 'pemilik')
        <form action="{{url ('/testimoni')}}" method="POST" class="form-group">
        {{ csrf_field() }}
        	<input type="hidden" name="kost_id" value="{{$kost->id}}">
        	<div class="form-group">
        		<textarea type="text" name="komentar" class="form-control" placeholder="berikan komentar anda" rows="2" ></textarea>
        	</div>
        	<div class="form-action">
        		<button type="submit" class="btn btn-sm btn-primary">Kirim</button>
        	</div>
        </form>
        @endif
        </div>
        
    </div>
	<div class="col-lg-9">
		<div class="col-md-12">
			
		</div>
		
		<div class="col-md-12">
			@if (session('sukses'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Selamat!</strong> {{ session('sukses') }}
            </div>
            @elseif (session('gagal'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Oops!</strong> {{ session('gagal') }}
            </div>
            @endif
            
			<div class="card">
				<!-- SLIDE FOTO KOST -->
				<div class="slideshow-container">
					<div class="mySlides">
						<img src="{{URL::to('/kost_image/'.$kost->foto_1)}}" class="card-img" style="width:100%; height: 420px">
					</div>

					<div class="mySlides">
						<img src="{{URL::to('/kost_image/'.$kost->foto_2)}}" class="card-img" style="width:100%; height: 420px">
					</div>

					<div class="mySlides">
						<img src="{{URL::to('/kost_image/'.$kost->foto_3)}}" class="card-img" style="width:100%; height: 420px">
					</div>

					<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
					<a class="next" onclick="plusSlides(1)">&#10095;</a>
				</div>
            	<!-- SELESAI -->

            	<div class="card-block">
	            	<div class="col-sm-6">
		                <dl class="row">
		                	<dd id="{{$kost->id}}" class="col-sm-12">
		                		<div class="btn-group" data-toggle="buttons">
		                			<button class="btn btn-lg btn-success">
		                				<i class="fa fa-building"></i> Sisa Kamar : {{$kost->sisa_kamar}}
		                			</button>
		                			<button type="button" name="pesan" class="btn btn-lg btn-primary">
		                				<i class="fa fa-send"></i> Pesan Kost
		                			</button>
		                		</div>
		                	</dd>

		                    <dt class="col-sm-4"><i class="icon-home"></i> Nama Kost</dt>
		                    <dd class="col-sm-8"><strong>{{$kost->nama_kost}}</strong></dd>
		                    
		                    <dt class="col-sm-4"><i class="icon-key"></i> ID Kost</dt>
		                    <dd class="col-sm-8">{{$kost->id}}</dd>

		                    <dt class="col-sm-4"><i class="icon-mustache"></i> Nama Pemilik</dt>
		                    <dd class="col-sm-8">{{$kost->user->nama_depan}}</dd>

		                    @if ($kost->jenis_kost == 'Wanita')
		                    	<dt class="col-sm-4"><i class="icon-user-female"></i> Jenis Kost</dt>
		                    	<dd class="col-sm-8">{{$kost->jenis_kost}}</dd>
		                    @elseif ($kost->jenis_kost == 'Pria')
		                    	<dt class="col-sm-4"><i class="icon-user"></i> Jenis Kost</dt>
		                    	<dd class="col-sm-8">{{$kost->jenis_kost}}</dd>
		                    @else
		                    	<dt class="col-sm-4"><i class="icon-people"></i> Jenis Kost</dt>
		                    	<dd class="col-sm-8">{{$kost->jenis_kost}}</dd>
		                    @endif

		                    <dt class="col-sm-4"><i class="icon-home"></i> Kamar Mandi</dt>
		                    <dd class="col-sm-8">{{$kost->kamar_mandi}}</dd>

		                    <dt class="col-sm-4"><i class="icon-phone"></i> Telepon</dt>
		                    <dd class="col-sm-8">{{$kost->user->hp}}</dd>

		                    <dt class="col-sm-4"><i class="icon-location-pin"></i> Alamat</dt>
		                    <dd class="col-sm-8">{{$kost->alamat_kost}}</dd>

		                    <dt class="col-sm-4"><i class="icon-speech"></i> Deskripsi</dt>
		                    <dd class="col-sm-8">{{$kost->deskripsi}}</dd>
		                </dl>
	                </div>
	                <div class="col-sm-6">
	                	<dl class="row">
	                	@foreach($kost->relasi as $relasi)
	                		@if($relasi->parameter->nama_parameter == 'Harga')
	                		<dt class="col-sm-5"></dt>
	                		<dd class="col-sm-7" align="right">
	                			<h2>Rp. {{number_format($relasi->nilai_parameter)}}</h2>
	                		</dd>
	                		@else
	                		<dt class="col-sm-6">{{$relasi->parameter->nama_parameter}}</dt>
	                		<dd class="col-sm-4" align="right">{{$relasi->nilai_parameter}}</dd>
	                		<dd class="col-sm-2">{{$relasi->parameter->satuan}}</dd>
	                		@endif
	                	@endforeach
	                	</dl>
	                </div>
                </div>
	        </div>
        </div>
	</div>
</div>

@endsection

@section('modal')
	<div id="pesan" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/pesan') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="kost_id" value="{{$kost->id}}">

                    <div class="modal-header">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Pesan Kost</h4>
                    </div>

                    <div class="modal-body">
                        <p>
                            Anda akan mengirim pesan kepada pemilik kost ini, kirim pesan?
                        </p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" name="batal">
                            <i class="fa fa-btn fa-ban"></i> Batal
                        </button>
                        <button type="submit" class="btn btn-success" name="pesan">
                            <i class="fa fa-btn fa-trash"></i> Kirim
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="delete" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/testimoni') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="id" value="">

                    <div class="modal-header">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Hapus Komentar</h4>
                    </div>

                    <div class="modal-body">
                        <p>
                            Hapus Komentar ini?
                        </p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" name="batal">
                            <i class="fa fa-btn fa-ban"></i> Batal
                        </button>
                        <button type="submit" class="btn btn-success" name="simpan">
                            <i class="fa fa-btn fa-trash"></i> Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	<script>
		var slideIndex = 1;
		showSlides(slideIndex);

		function plusSlides(n) {
		  showSlides(slideIndex += n);
		}

		function currentSlide(n) {
		  showSlides(slideIndex = n);
		}

		function showSlides(n) {
		  var i;
		  var slides = document.getElementsByClassName("mySlides");
		  var dots = document.getElementsByClassName("dot");
		  if (n > slides.length) {slideIndex = 1}    
		  if (n < 1) {slideIndex = slides.length}
		  for (i = 0; i < slides.length; i++) {
		      slides[i].style.display = "none";  
		  }
		  for (i = 0; i < dots.length; i++) {
		      dots[i].className = dots[i].className.replace(" active", "");
		  }
		  slides[slideIndex-1].style.display = "block";  
		  dots[slideIndex-1].className += " active";
		}
	</script>

	<script type="text/javascript">        
        $(document)

        .on('click', 'button[name="pesan"]', function() {
            $('#pesan').removeClass('fade');
            $('#pesan').show();
        })

        .on('click', 'button[name="batal"], .close-modal', function() {
            $('.modal').hide();
            $('.modal input[name="kost_id"]').val('');
            $('.modal input[name="user_id"]').val('');
        })

        .on('click', 'button[name="delete"]', function() {
            var id = $(this).parents('tr').first().attr('id');
            $('#delete input[name="id"]').val(id);
            $('#delete').removeClass('fade');
            $('#delete').show();
        })

        ;
    </script>
@endsection