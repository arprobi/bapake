@extends('layouts.admin')

@section('content')

<br>
@if (session('sukses'))
<div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Selamat!</strong> {{ session('sukses') }}
</div>
@elseif (session('gagal'))
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Oops!</strong> {{ session('gagal') }}
</div>
@endif

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            
            <div class="card-header">
                <strong>Tabel Pesan</strong>
            </div>
            
            <div class="card-block">
                <table class="table">
                	<thead class="thead-inverse">
                		<tr>
                			<th>Mahasiswa</th>
                			<th>Kost yg dipesan</th>
                			<th>Status</th>
                			<th>Waktu pengiriman</th>
                            <th>Aksi</th>
                		</tr>
                	</thead>
                	<tbody>
                		@foreach ($pesans as $no => $pesan)
                		<tr id="{{ $pesan->id }}">
                			<td name="user_id" >{{$pesan->user->nama_depan}}</td>
                			<td>{{$pesan->kost->nama_kost}}</td>
                            <td name="status" id="{{ $pesan->status }}">
                            @if($pesan->status == 'Sepakat')
                                <button class="btn btn-sm btn-block btn-success btn-xs">
                                    <i class="fa fa-check"></i> Sepakat
                                </button>
                            @elseif($pesan->status == 'Batal')
                                <button class="btn btn-sm btn-block btn-danger active btn-xs">
                                    <i class="fa fa-close"></i> Batal
                                </button>
                            @else
                                <button class="btn btn-sm btn-block btn-secondary btn-xs">
                                    <i class="fa fa-circle-o-notch"></i> Menunggu
                                </button>
                            @endif
                            </td>
                			<td name="created_at">{{$pesan->created_at}}</td>
                			<td>
                                <a href="{{url('pesan', $pesan->id)}}" class="btn btn-sm btn-primary btn-xs"> 
                                    <i class="fa fa-eye"></i> Lihat
                                </a>
                				<button type="button" name="delete" class="btn btn-sm btn-danger btn-xs">
                                    <i class="fa fa-btn fa-trash"></i>Hapus
                                </button>
                			</td>
                		</tr>
                		@endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
    <div id="delete" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/pesan') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="id" value="">

                    <div class="modal-header">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Hapus Pesan</h4>
                    </div>

                    <div class="modal-body">
                        <p>
                            Apakah Anda yakin ingin menghapus pesan ini?
                        </p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" name="batal">
                            <i class="fa fa-btn fa-ban"></i> Batal
                        </button>
                        <button type="submit" class="btn btn-success" name="simpan">
                            <i class="fa fa-btn fa-trash"></i> Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- My App -->
    <script type="text/javascript">        
        $(document)

        .on('click', 'button[name="delete"]', function() {
            var id = $(this).parents('tr').first().attr('id');
            $('#delete input[name="id"]').val(id);
            $('#delete').removeClass('fade');
            $('#delete').show();
        })

        .on('click', 'button[name="batal"], .close-modal', function() {
            $('.modal').hide();
            $('.modal input[name="id"]').val('');
        })
        ;
    </script>
@endsection