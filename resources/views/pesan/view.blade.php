@extends('layouts.admin')
@section('content')
<br>
<div class="row">
	<div class="col-md-12">
		<div class="card">
        	<div class="card-block">
        		<h4 class="title"> Rincian Pemesanan Kost</h4>
        		<hr>
                <dl class="row">
                    <dt class="col-sm-4"><i class="icon-key"></i> ID Pesan</dt>
                    <dd class="col-sm-8">{{$pesan->id}}</dd>

                    <dt class="col-sm-4"><i class="icon-user"></i> Pengirim</dt>
                    <dd class="col-sm-8">{{$pesan->user->nama_depan}}</dd>

                    <dt class="col-sm-4"><i class="fa fa-user"></i>  Penerima</dt>
                    <dd class="col-sm-8">{{$pesan->kost->user->nama_depan}}</dd>

                    <dt class="col-sm-4"><i class="fa fa-building"></i> Kost yang dipesan</dt>
                    <dd class="col-sm-8">{{$pesan->kost->nama_kost}}</dd>

                    <dt class="col-sm-4"><i class="fa fa-calendar"></i> Waktu Pemesanan</dt>
                    <dd class="col-sm-8">{{$pesan->created_at}}</dd>

                    <dt class="col-sm-4"><i class="fa fa-exclamation"></i> Status</dt>
                    <dd class="col-sm-8">{{$pesan->status}}</dd>
                </dl>
            </div>
        </div>
    </div>
</div>

@endsection