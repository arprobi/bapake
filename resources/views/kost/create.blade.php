@extends('layouts.admin')

@section('content')
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong>Tambah Kost</strong> Isi identitas user
            </div>
            <div class="card-block">
                {!! Form::open(['route' => 'kost.store', 'files' => true, 'class' => 'form-horizontal'])!!}
                    <div class="col-sm-12">
                        <div class="form-group row {{ $errors->has('nama_kost') ? ' has-danger' : '' }}">
                            <label class="col-md-3 form-control-label" for="nama_kost">Nama Kost</label>
                            <div class="col-lg-9">
                                <input name="nama_kost" type="text" class="form-control" id="nama_kost" placeholder="Nama Kost" value="{{ old('nama_kost') }}">
                                @if ($errors->has('nama_kost'))
                                    <div class="form-control-feedback">
                                        {{ $errors->first('nama_kost') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row {{ $errors->has('jenis_kost') ? ' has-danger' : '' }}">
                        <label class="col-md-3 form-control-label" for="jenis_kost">Jenis Kost</label>
                            <div class="col-lg-9">
                                <select name="jenis_kost" class="form-control" id="jenis_kost" value="{{ old('jenis_kost') }}">
                                    <option>--Pilih jenis kost--</option>
                                    <option value="Pria" {{ (old('jenis_kost') == 'Pria' ? "selected":"") }} >Pria</option>
                                    <option value="Wanita"{{ (old('jenis_kost') == 'Wanita' ? "selected":"") }}>Wanita</option>
                                </select>
                                @if ($errors->has('jenis_kost'))
                                    <div class="form-control-feedback">
                                        {{ $errors->first('jenis_kost') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row {{ $errors->has('kamar_mandi') ? ' has-danger' : '' }}">
                        <label class="col-md-3 form-control-label" for="kamar_mandi">Jenis Kamar Mandi</label>
                            <div class="col-lg-9">
                                <select name="kamar_mandi" class="form-control" id="kamar_mandi">
                                    <option>--Pilih jenis kamar mandi--</option>
                                    <option value="Luar" {{ (old('kamar_mandi') == 'Luar' ? "selected":"") }}>Luar</option>
                                    <option value="Dalam" {{ (old('kamar_mandi') == 'Dalam' ? "selected":"") }}>Dalam</option>
                                </select>
                                @if ($errors->has('kamar_mandi'))
                                    <div class="form-control-feedback">
                                        {{ $errors->first('kamar_mandi') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row {{ $errors->has('jumlah_kamar') ? ' has-danger' : '' }}">
                            <label class="col-md-3 form-control-label" for="jumlah_kamar">Jumlah Kamar</label>
                            <div class="col-lg-9">
                                <input name="jumlah_kamar" type="text" class="form-control" id="jumlah_kamar" placeholder="Jumlah Kamar" value="{{ old('jumlah_kamar') }}">
                                @if ($errors->has('jumlah_kamar'))
                                    <div class="form-control-feedback">
                                        {{ $errors->first('jumlah_kamar') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row{{ $errors->has('foto_1') ? ' has-danger' : '' }}">
                            <label class="col-md-3 form-control-label" for="foto_1">Foto Depan</label>
                            <div class="col-lg-9">
                                <input name="foto_1" type="file" class="form-control" id="foto_1" placeholder="Foto Depan" value="{{ old('foto_1') }}">
                                @if ($errors->has('foto_1'))
                                    <div class="form-control-feedback">
                                        {{ $errors->first('foto_1') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row{{ $errors->has('foto_2') ? ' has-danger' : '' }}">
                            <label class="col-md-3 form-control-label" for="foto_2">Foto Kamar Kost</label>
                            <div class="col-lg-9">
                                <input name="foto_2" type="file" class="form-control" id="foto_2" placeholder="Foto Kamar Kost" value="{{ old('foto_2') }}">
                                @if ($errors->has('foto_2'))
                                    <div class="form-control-feedback">
                                        {{ $errors->first('foto_2') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row{{ $errors->has('foto_3') ? ' has-danger' : '' }}">
                            <label class="col-md-3 form-control-label" for="foto_3">Foto Kamar Mandi</label>
                            <div class="col-lg-9">
                                <input name="foto_3" type="file" class="form-control" id="foto_3" placeholder="Foto Kamar Mandi" value="{{ old('foto_3') }}">
                                @if ($errors->has('foto_3'))
                                    <div class="form-control-feedback">
                                        {{ $errors->first('foto_3') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row{{ $errors->has('foto_4') ? ' has-danger' : '' }}">
                            <label class="col-md-3 form-control-label" for="foto_4">Foto Pendukung</label>
                            <div class="col-lg-9">
                                <input name="foto_4" type="file" class="form-control" id="foto_4" placeholder="Foto Pendukung" value="{{ old('foto_4') }}">
                                @if ($errors->has('foto_4'))
                                    <div class="form-control-feedback">
                                        {{ $errors->first('foto_4') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row{{ $errors->has('alamat_kost') ? ' has-danger' : '' }}">
                            <label class="col-md-3 form-control-label" for="alamat_kost">Alamat Kost</label>
                            <div class="col-lg-9">
                                <textarea  name="alamat_kost" class="form-control" rows="3" id="alamat_kost" placeholder="Alamat lengkap kost" >{{ old('alamat_kost') }}</textarea>
                                @if ($errors->has('alamat_kost'))
                                    <div class="form-control-feedback">
                                        {{ $errors->first('alamat_kost') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row{{ $errors->has('deskripsi') ? ' has-danger' : '' }}">
                            <label class="col-md-3 form-control-label" for="alamat_kost">Deskripsi Kost</label>
                            <div class="col-lg-9">
                                <textarea  name="deskripsi" class="form-control" rows="5" id="deskripsi" placeholder="Deskripsi kost">{{ old('deskripsi') }}</textarea>
                                @if ($errors->has('deskripsi'))
                                    <div class="form-control-feedback">
                                        {{ $errors->first('deskripsi') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <div class="col-lg-9 col-lg-offset-2">
                                <button type="reset" class="btn btn-secondary"><i class="fa fa-chevron-left"> Batal</i></button>
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"> Simpan</i></button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

    // $(document).ready(function(){
    //     $('#jumlah_kamar').autoNumeric();
    // });

    var tanpa_rupiah = document.getElementById('jumlah_kamar');
    tanpa_rupiah.addEventListener('keyup', function(e)
    {
        tanpa_rupiah.value = formatRupiah(this.value);
    });
    
    tanpa_rupiah.addEventListener('keydown', function(event)
    {
        limitCharacter(event);
    });

    //Fungsinya
    function formatRupiah(bilangan, prefix)
    {
        var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
            split   = number_string.split(','),
            sisa    = split[0].length % 3,
            rupiah  = split[0].substr(0, sisa),
            ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    
    function limitCharacter(event)
    {
        key = event.which || event.keyCode;
        if ( key != 188 // Comma
             && key != 8 // Backspace
             && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
             && (key < 48 || key > 57) // Non digit
             // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
            ) 
        {
            event.preventDefault();
            return false;
        }
    }

</script>
@endsection