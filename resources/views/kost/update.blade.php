@extends('layouts.admin')

@section('content')
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong>Edit Kost</strong> Ubah detail kost
            </div>
            <div class="card-block">
	            {!! Form::model($kost, ['route' => ['kost.update', $kost], 'method' =>'patch', 'files' => true])!!}
	                <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="nama_kost">Nama Kost</label>
                            <div class="col-lg-9">
                                <input name="nama_kost" type="text" class="form-control" id="nama_kost" placeholder="Nama Kost" value="{{$kost->nama_kost}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                        <label class="col-md-3 form-control-label" for="jenis_kost">Jenis Kost</label>
                            <div class="col-lg-9">
                                <select name="jenis_kost" class="form-control" id="jenis_kost" value="{{$kost->jenis_kost}}">
                                    @if($kost->jenis_kost=='Pria')
                                    <option value="Pria" selected>Pria</option>
                                    <option value="Wanita">Wanita</option>
                                    @else
                                    <option value="Pria">Pria</option>
                                    <option value="Wanita"selected>Wanita</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                        <label class="col-md-3 form-control-label" for="kamar_mandi">Jenis Kamar Mandi</label>
                            <div class="col-lg-9">
                                <select name="kamar_mandi" class="form-control" id="kamar_mandi" value="{{$kost->kamar_mandi}}">
                                    @if($kost->kamar_mandi=='Luar')
                                    <option value="Luar" selected>Luar</option>
                                    <option value="Dalam">Dalam</option>
                                    @else
                                    <option value="Luar">Luar</option>
                                    <option value="Dalam"selected>Dalam</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                        <label class="col-md-3 form-control-label" for="kamar_mandi">Status</label>
                            <div class="col-lg-9">
                                <select name="status" class="form-control" id="status" value="{{$kost->status}}">
                                    @if($kost->status=='Proses')
                                    <option value="Proses" selected>Proses</option>
                                    <option value="Disetujui">Disetujui</option>
                                    @else
                                    <option value="Proses">Proses</option>
                                    <option value="Disetujui"selected>Disetujui</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="jumlah_kamar">Jumlah Kamar</label>
                            <div class="col-lg-9">
                                <input name="jumlah_kamar" type="number" class="form-control" id="jumlah_kamar" placeholder="Jumlah Kamar" value="{{$kost->jumlah_kamar}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="foto_1">Foto Depan</label>
                            <div class="col-lg-9">
                                <input name="foto_1" type="file" class="form-control" id="foto_1" placeholder="Foto Depan" value="{{$kost->foto_1}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="foto_2">Foto Kamar Kost</label>
                            <div class="col-lg-9">
                                <input name="foto_2" type="file" class="form-control" id="foto_2" placeholder="Foto Kamar Kost" value="{{$kost->foto_2}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="foto_3">Foto Kamar Mandi</label>
                            <div class="col-lg-9">
                                <input name="foto_3" type="file" class="form-control" id="foto_3" placeholder="Foto Kamar Mandi" value="{{$kost->foto_3}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="foto_4">Foto Pendukung</label>
                            <div class="col-lg-9">
                                <input name="foto_4" type="file" class="form-control" id="foto_4" placeholder="Foto Pendukung" value="{{$kost->foto_4}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="alamat_kost">Alamat Kost</label>
                            <div class="col-lg-9">
                                <textarea  name="alamat_kost" class="form-control" rows="3" id="alamat_kost" placeholder="Alamat lengkap kost">{{$kost->alamat_kost}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="alamat_kost">Deskripsi Kost</label>
                            <div class="col-lg-9">
                                <textarea  name="deskripsi" class="form-control" rows="5" id="deskripsi" placeholder="Deskripsi kost">{{$kost->deskripsi}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group row">
                            <div class="col-lg-9 col-lg-offset-2">
                                <button type="reset" class="btn btn-secondary"><i class="fa fa-chevron-left"> Batal</i></button>
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"> Simpan</i></button>
                            </div>
                        </div>
                    </div>
	            {!! Form::close() !!}
		        </div>
		    </div>
		</div>
    </div>
@endsection