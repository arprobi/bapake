@extends('layouts.admin')

@section('content')
<br>

@if (session('sukses'))
<div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Selamat!</strong> {{ session('sukses') }}
</div>
@elseif (session('gagal'))
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Oops!</strong> {{ session('gagal') }}
</div>
@endif

<div class="row">
    <div class="col-lg-12">
    <h3>Tabel Data Kost</h3>
    <div class="btn-group">
        <a href="{{route ('kost.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Kost</a>
    </div>
    <hr>
        <div class="card card-accent-primary">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Data Kost
            </div>
            <div class="card-block">
                <form action="{{ url('carikost') }}" method="POST">{!! csrf_field() !!}
                    <div class="input-group">
                        <input type="text" name="cari" class="form-control" placeholder="Cari kost berdasarkan nama">
                        <span class="input-group-btn">
                            <button class="btn btn-primary">
                                <i class="fa fa-search"> Cari</i>
                            </button>
                        </span>
                    </div>
                </form>
                <br>
                <table class="table">
					<thead class="thead-inverse">
						<tr>
							<th>Nama Kost</th>
							<th>Status</th>
							<th>Nama Pemilik</th>
							<th>Jenis Kost</th>
							<th>Kamar Mandi</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($kost as $kost)
						<tr id="{{ $kost->id }}">
							<td>{{$kost->nama_kost}}</td>
							<td>
								@if($kost->status=='Disetujui')
								<button class="btn btn-sm btn-block btn-success">
									<i class="fa fa-check"></i> Disetujui
								</button>
								@else
								<button class="btn btn-sm btn-block btn-secondary">
									<i class="fa fa-hourglass"></i> Proses
								</button>
								@endif
							</td>
							<td>{{$kost->user->nama_depan}}</td>
							<td>{{$kost->jenis_kost}}</td>
							<td>{{$kost->kamar_mandi}}</td>
							<td>
								<a href="{{route('kost.show', $kost->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-eye">Lihat</i></a>
								<a href="{{route('kost.edit', $kost->id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit">Ubah</i></a>
								<button name="delete" class="btn btn-sm btn-danger">
									<i class="fa fa-trash-o">Hapus</i>
								</button>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('modal')
    <div id="delete" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/kost') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="id" value="">

                    <div class="modal-header">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Hapus Kost</h4>
                    </div>

                    <div class="modal-body">
                        <p>
                            Apakah Anda yakin ingin menghapus kost ini?
                        </p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" name="batal">
                            <i class="fa fa-btn fa-ban"></i> Batal
                        </button>
                        <button type="submit" class="btn btn-success" name="simpan">
                            <i class="fa fa-btn fa-trash"></i> Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- My App -->
    <script type="text/javascript">
        $(document)
        
        .on('click', 'button[name="delete"]', function() {
            var id = $(this).parents('tr').first().attr('id');
            $('#delete input[name="id"]').val(id);
            $('#delete').removeClass('fade');
            $('#delete').show();
        })

        .on('click', 'button[name="batal"], .close-modal', function() {
            $('.modal').hide();
            $('.modal input[name="id"]').val('');
        })
        ;
    </script>
@endsection