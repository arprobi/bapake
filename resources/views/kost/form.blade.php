<div class="form-group row">
	{!! Form::label('nama_kost','Nama Kost :', ['class' => 'col-lg-2 control-label']) !!}
	<div class="col-lg-10">
	{!! Form::text('nama_kost',null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group row">
	{!! Form::label('jenis_kost','Satuan :', ['class' => 'col-lg-2 control-label']) !!}
	<div class="col-lg-10">
	{!! Form::select('jenis_kost', ['Pria' => 'Pria', 'Wanita' => 'Wanita', 'Campur' => 'Campur'], null, ['placeholder' => 'Jenis kost...', 'class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group row">
	{!! Form::label('kamar_mandi','Kamar Mandi :', ['class' => 'col-lg-2 control-label']) !!}
	<div class="col-lg-10">
	{!! Form::select('kamar_mandi', ['Luar' => 'Luar', 'Dalam' => 'Dalam'], null, ['placeholder' => 'Kamar mandi...', 'class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group row">
	{!! Form::label('foto', 'Foto :', ['class' => 'col-lg-2 control-label']) !!}
	<div class="col-lg-10">
  	{!! Form::file('foto',null, ['class' => 'form-control']) !!}
  	</div>
</div>

<div class="form-group row">
	{!! Form::label('alamat_kost','Alamat :', ['class' => 'col-lg-2 control-label']) !!}
	<div class="col-lg-10">
	{!! Form::textarea('alamat_kost',null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group row">
	{!! Form::label('deskripsi','Deskripsi Kos :', ['class' => 'col-lg-2 control-label']) !!}
	<div class="col-lg-10">
	{!! Form::textarea('deskripsi',null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group row">
	{!! Form::submit(isset($model) ? 'Ubah' : 'Simpan', ['class'=>'btn btn-primary']) !!}
</div>
