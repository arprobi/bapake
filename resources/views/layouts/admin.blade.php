<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="widht=device, initial-scale=1">
    
    <title>Bapak'e</title>

    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/simple-line-icons.css') }}" rel="stylesheet">
    
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet">

    <style>
        .mySlides {display:none}

        /* Slideshow container */
        .slideshow-container {
          max-width: 1000px;
          position: relative;
          margin: auto;
        }

        /* Next & previous buttons */
        .prev, .next {
          cursor: pointer;
          position: absolute;
          top: 50%;
          width: auto;
          padding: 16px;
          margin-top: -22px;
          color: white;
          font-weight: bold;
          font-size: 18px;
          transition: 0.6s ease;
          border-radius: 0 3px 3px 0;
        }

        /* Position the "next button" to the right */
        .next {
          right: 0;
          border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover, .next:hover {
          background-color: rgba(0,0,0,0.8);
        }

        /* Caption text */
        .text {
          color: #f2f2f2;
          font-size: 15px;
          padding: 8px 12px;
          position: absolute;
          bottom: 8px;
          width: 100%;
          text-align: center;
        }

        /* Number text (1/3 etc) */
        .numbertext {
          color: #f2f2f2;
          font-size: 12px;
          padding: 8px 12px;
          position: absolute;
          top: 0;
        }

        /* The dots/bullets/indicators */
        .dot {
          cursor:pointer;
          height: 13px;
          width: 13px;
          margin: 0 2px;
          background-color: #bbb;
          border-radius: 50%;
          display: inline-block;
          transition: background-color 0.6s ease;
        }

        .dot:hover {
          background-color: #717171;
        }

        /* Fading animation */
        .fade {
          -webkit-animation-name: fade;
          -webkit-animation-duration: 1s;
          animation-name: fade;
          animation-duration: 1s;
        }

        @-webkit-keyframes fade {
          from {opacity: .4} 
          to {opacity: 1}
        }

        @keyframes fade {
          from {opacity: .4} 
          to {opacity: 1}
        }

        /* On smaller screens, decrease text size */
        @media only screen and (max-width: 300px) {
          .prev, .next,.text {font-size: 11px}
        }
    </style>

</head>
<body class="navbar-fixed sidebar-nav fixed-nav">
    <!-- Header -->
    <header class="navbar">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ url('/home') }}"></a>
            <ul class="nav navbar-nav hidden-md-down">
                <li class="nav-item px-1 {{ (Request::url() == url('/home') ? 'active' : '') }}">
                    <a class="nav-link" href="{{ url('/home') }}">Beranda</a>
                </li>
                <li class="nav-item px-1 {{ (Request::url() == url('/listkost/all') ? 'active' : '') }}">
                    <a class="nav-link" href="{{ url('/listkost/all') }}">Semua Kost</a>
                </li>
                <li class="nav-item px-1 {{ (Request::url() == url('/pencarian') ? 'active' : '') }}">
                    <a class="nav-link" href="{{ url('/pencarian') }}">Pencarian</a>
                </li>
            </ul>
            <ul class="nav navbar-nav float-xs-right hidden-md-down">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <img src="{{URL::to('/user_image/'.Auth::user()->foto)}}" class="img-avatar">
                        <span class="hidden-md-down">{{Auth::user()->nama_belakang}}</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">

                        <div class="dropdown-header text-xs-center">
                            <strong>Akun</strong>
                        </div>
                        <a class="dropdown-item" href="{{ url('/profil') }}"><i class="fa fa-user">
                            </i> Profil
                        </a>
                        <a class="dropdown-item" href="{{ url('/unduh') }}"><i class="fa fa-download">
                            </i> Unduh Panduan
                        </a>
                        <div class="divider"></div>
                        <a class="dropdown-item" href="{{ url('/logout') }}"><i class="fa fa-lock"></i> Logout</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link aside-toggle"> </a>
                </li>
            </ul>
        </div>
    </header>
    <!-- Akhir Header -->

    <!-- Sidebar -->
    <div class="sidebar">
        <nav class="sidebar-nav">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link {{ (Request::url() == url('/admin') ? 'active' : '') }}" href="{{ url('/admin') }}"><i class="icon-home"></i> Dasboard </a>
                </li>

                <li class="nav-title">
                    Master Data
                </li>

                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown"><i class="icon-people"></i> Data User</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{ (Request::url() == url('/user') ? 'active' : '') }}" href="{{ url('/user') }}"><i class="fa fa-user-secret"></i> Administrator</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ (Request::url() == url('/pengguna/mahasiswa') ? 'active' : '') }}" href="{{ url('/pengguna/mahasiswa') }}"><i class="fa fa-mortar-board"></i> Mahasiswa</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ (Request::url() == url('/pengguna/pemilik') ? 'active' : '') }}" href="{{ url('/pengguna/pemilik') }}"><i class="icon-user"></i> Pemilik</a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ (Request::url() == url('/kost') ? 'active' : '') }}" href="{{ url('/kost') }}"><i class="icon-grid"></i> Data Kost</a>
                </li>

                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" data-toggle="dropdown"><i class="icon-calculator"></i> Data Fuzzy</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{ (Request::url() == url('/parameter') ? 'active' : '') }}" href="{{ url('/parameter') }}" target="_top"><i class="fa fa-tags"></i> Variabel</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{ (Request::url() == url('/himpunan') ? 'active' : '') }}" href="{{ url('/himpunan') }}" target="_top"><i class="fa fa-calculator"></i> Himpunan</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{ (Request::url() == url('/derajat') ? 'active' : '') }}" href="{{ url('/derajat') }}" target="_top"><i class="fa fa-gear"></i> Derajat Keanggotaan</a>
                        </li>
                    </ul>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link {{ (Request::url() == url('/pesan') ? 'active' : '') }}" href="{{ url('/pesan') }}"><i class="icon-envelope"></i> Pemesan Kost</a>
                </li>
<!-- 
                <li class="nav-item">
                    <a class="nav-link {{ (Request::url() == url('/bantuanadm') ? 'active' : '') }}" href="{{ url('/bantuanadm') }}"><i class="icon-question"></i> Bantuan</a>
                </li> -->
            </ul>
        </nav>
    </div>
    <!-- Akhir Sidebar -->
    <!-- Content -->
    <main class="main">
        <div class="container-fluid">
            @yield('content')
        </div>
    </main>
    <!-- Akhir Content -->
    <footer class="footer">
        <span class="text-left">
            <strong class="primary">Andrian Robby © 2016 Skripsweet</strong>.
        </span>
        <span class="float-xs-right">
            Powered by <a href="http://coreui.io">CoreUI</a>
        </span>
    </footer>
    @yield('modal')

    <script src="{{ asset('assets/js/jquery.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js') }}"></script>
    <script src="{{ asset('js/chart/main.js') }}"></script>

    @yield('scripts')
    
</body>
</html>