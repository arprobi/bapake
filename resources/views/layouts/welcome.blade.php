<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="widht=device, initial-scale=1">
    
    <title>Bapak'e</title>

    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/simple-line-icons.css" rel="stylesheet">

    <link href="assets/css/style.css" rel="stylesheet">

    <style type="text/css">
        .produk{
            text-align: center;
            margin-bottom: 20px;
        }
        .devider{
            margin: 80px 0;
        }
        hr{
            border: solid 1px #eee;
        }
        .thumbnail img{
            width: 100%;
        }
    </style>

</head>
<body class="navbar-fixed sidebar-nav fixed-nav">
    <header class="navbar">
        <div class="container-fluid">
            <button class="navbar-toggler mobile-toggler hidden-lg-up" type="button">☰</button>
            <a class="navbar-brand" href="#"></a>
            <ul class="nav navbar-nav hidden-md-down">
                <li class="nav-item">
                    <a class="nav-link navbar-toggler layout-toggler" href="#">☰</a>
                </li>

                <li class="nav-item px-1">
                    <a class="nav-link" href="#">Dashboard</a>
                </li>
                <li class="nav-item px-1">
                    <a class="nav-link" href="#">Users</a>
                </li>
                <li class="nav-item px-1">
                    <a class="nav-link" href="#">Settings</a>
                </li>
            </ul>
            <ul class="nav navbar-nav float-xs-right hidden-md-down">
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="icon-bell"></i><span class="tag tag-pill tag-danger">5</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="icon-list"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="icon-location-pin"></i></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <img src="assets/img/avatars/6.jpg" class="img-avatar" alt="admin@bootstrapmaster.com">
                        <span class="hidden-md-down">admin</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">

                        <div class="dropdown-header text-xs-center">
                            <strong>Account</strong>
                        </div>

                        <a class="dropdown-item" href="#"><i class="fa fa-bell-o"></i> Updates<span class="tag tag-info">42</span></a>
                        <a class="dropdown-item" href="#"><i class="fa fa-envelope-o"></i> Messages<span class="tag tag-success">42</span></a>
                        <a class="dropdown-item" href="#"><i class="fa fa-tasks"></i> Tasks<span class="tag tag-danger">42</span></a>
                        <a class="dropdown-item" href="#"><i class="fa fa-comments"></i> Comments<span class="tag tag-warning">42</span></a>

                        <div class="dropdown-header text-xs-center">
                            <strong>Settings</strong>
                        </div>

                        <a class="dropdown-item" href="#"><i class="fa fa-user"></i> Profile</a>
                        <a class="dropdown-item" href="#"><i class="fa fa-wrench"></i> Settings</a>
                        <a class="dropdown-item" href="#"><i class="fa fa-usd"></i> Payments<span class="tag tag-default">42</span></a>
                        <a class="dropdown-item" href="#"><i class="fa fa-file"></i> Projects<span class="tag tag-primary">42</span></a>
                        <div class="divider"></div>
                        <a class="dropdown-item" href="#"><i class="fa fa-shield"></i> Lock Account</a>
                        <a class="dropdown-item" href="#"><i class="fa fa-lock"></i> Logout</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link aside-toggle" href="#">☰</a>
                </li>

            </ul>
        </div>
    </header>
    <!-- Header -->
    <nav class="navbar navbar-default" style="border-radius: 0px">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- <a class="navbar-brand" href="#">
                <img alt="Brand" src="assets/img/logo.jpg" class="img-circle">
                </a> -->
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="{{ (Request::url() == url('/home') ? 'active' : '') }}">
                        <a href="{{ url('/home') }}">Bapak'e <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Akhir Header -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <!-- Foto Slide  -->
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>

                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="assets/img/kost1.jpg" alt="img1" width="100%">
                            <div class="carousel-caption">
                                <h3>Sistem Pendukung Keputusan</h3>
                                <p>Pemilihan kost daerah Faklutas Teknik Universitas Jenderal Soedirman</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="assets/img/kost2.jpg" alt="img1">
                            <div class="carousel-caption">
                                <h3>Fuzzy Database</h3>
                                <p>Model Tahani, pemiliaan kost Menggunakan metode fuzzy database, tahani</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="assets/img/kost3.jpg" alt="img1">
                            <div class="carousel-caption">
                                <h3>Skripsi</h3>
                                <p>Andrian Robby Pratama, H1L013003</p>
                            </div>
                        </div>
                    </div>

                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Sebelumnya</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Selanjutnya</span>
                    </a>
                </div>
            </div>
            <!-- Akhit Foto Slide -->
        </div>
    </div>
    
    <!-- Jumbroton -->
    <div class="container">
        <div class="container text-center">
            <h2 class="page-header">Selamat Datang di Bapak'e</h2>
            <p>Hemat waktu dan uang anda dengan memilih kost tanpa mendatangi kost satu-persatu. Cukup dengan gunakan situs web ini dan temukan kost di seluruh lingkungan <strong>Fakultas Teknik Universitas Jenderal Soedirman</strong>.</p>
            <p>Tak perlu bingung memilih, karena web ini dilengkapi dengan fitur pembantu pendukung keputusan menggunakan metode fuzzy database model tahani, sehingga anda akan disarankan memilih kost sesuai denga kriteria yang anda mau.</p>
            <p>Atau jika anda mempunyai tempat kost? promosikan segera di web Bapak'e ini.</p>
            <p><strong><h4>GRATIS!!!</h4></strong></p>
            <div class="btn-group">
                <a class="btn btn-success btn-lg" href="{{ url('/register') }}" role="button">Daftar</a>
                <a class="btn btn-primary btn-lg" href="{{ url('/login') }}" role="button">Masuk</a>
            </div>
        </div>
    </div>
    <!-- Selesai Jumbroton -->

    <!-- Daftar Produk -->
    <div class="container">
        
        @yield('content')

        <footer class="inverse">
            <p class="pull-right"><a href="#">Kembali ke atas</a></p>
            <p>Desain oleh : Andrian Robby S,Kom</p>
        </footer>
    </div>
    <!-- Akhir Daftar Produk -->

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
</body>
</html>