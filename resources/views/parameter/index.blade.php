@extends('layouts.admin')

@section('content')

<br>
@if (session('sukses'))
<div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Selamat!</strong> {{ session('sukses') }}
</div>
@elseif (session('gagal'))
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Oops!</strong> {{ session('gagal') }}
</div>
@endif

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            
            <div class="card-header">
                <strong>Tambah Variabel</strong> Isi variabel baru
            </div>
            
            <div class="card-block">
                <form class="form-horizontal" role="form" method="POST" action="{{url('parameter')}}">
                {!! csrf_field() !!}
                    <div class="form-group col-sm-4 {{ $errors->has('nama_parameter') ? ' has-danger' : '' }}">
                        <div class="col-md-12">
                            <label class="control-label" for="nama">Nama Variabel</label>
                            <input name="nama_parameter" type="text" class="form-control" id="nama" placeholder="Nama Variabel"  value="{{ old('nama_parameter') }}">
                            @if ($errors->has('nama_parameter'))
                                <div class="form-control-feedback">
                                    {{ $errors->first('nama_parameter') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('nilai_angka') ? ' has-danger' : '' }}">
                        <div class="col-md-12">
                            <label class="control-label" for="nama">Nilai Tertinggi</label>
                            <input name="nilai_angka" type="text" class="form-control" id="nil" placeholder="Nilai Tertinggi" value="{{ old('nilai_angka') }}">
                            @if ($errors->has('nilai_angka'))
                                <div class="form-control-feedback">
                                    {{ $errors->first('nilai_angka') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('satuan') ? ' has-danger' : '' }}">
                        <div class="col-md-12">
                            <label class="control-label" for="nama">Satuan Variabel</label>
                            <input name="satuan" type="text" class="form-control" id="sat" placeholder="Satuan Variabel" value="{{ old('satuan') }}">
                            @if ($errors->has('satuan'))
                                <div class="form-control-feedback">
                                    {{ $errors->first('satuan') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-sm-4">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info">
                                <i class="fa fa-save"> Simpan</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            
            <div class="card-header">
                <strong>Tabel Variabel</strong>
            </div>
            
            <div class="card-block">
                <table class="table">
                	<thead class="thead-inverse">
                		<tr>
                            <th>Nomor</th>
                			<th>Nama Variabel</th>
                			<th>Nilai Tertinggi</th>
                			<th>Satuan</th>
                			<th>Aksi</th>
                		</tr>
                	</thead>
                	<tbody>
                		@foreach ($parameter as $no => $par)
                		<tr id="{{ $par->id }}">
                            <td>{{$no+1}}</td>
                			<td name="nama_parameter" >{{$par->nama_parameter}}</td>
                			<td name="nilai_angka">{{$par->nilai_angka}}</td>
                			<td name="satuan">{{$par->satuan}}</td>
                			<td>
                				<button type="button" name="edit" class="btn btn-sm btn-warning btn-xs">
                                    <i class="fa fa-btn fa-edit"></i>Ubah
                                </button>
                				<button type="button" name="delete" class="btn btn-sm btn-danger btn-xs">
                                    <i class="fa fa-btn fa-trash"></i>Hapus
                                </button>
                			</td>
                		</tr>
                		@endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
    <div id="put" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/parameter') }}"> {!! csrf_field() !!}

                    <input type="hidden" name="_method" value="put">
                    <input type="hidden" name="id" value="">

                    <div class="modal-header">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Ubah Variabel</h4>
                    </div>

                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-3 control-label">Nama Variabel</label>

                            <div class="col-md-9">
                                <input type="text" class="form-control" name="nama_parameter">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 control-label">Nilai Tertinggi</label>

                            <div class="col-md-9">
                                <input type="text" class="form-control" name="nilai_angka">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 control-label">Satuan</label>

                            <div class="col-md-9">
                                <input type="text" class="form-control" name="satuan">
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" name="batal">
                            <i class="fa fa-btn fa-ban"></i> Batal
                        </button>
                        <button type="submit" class="btn btn-success" name="simpan">
                            <i class="fa fa-btn fa-save"></i> Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="delete" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/parameter') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="id" value="">

                    <div class="modal-header">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Hapus Variabel</h4>
                    </div>

                    <div class="modal-body">
                        <p>
                            Apakah Anda yakin ingin menghapus variabel ini?
                        </p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" name="batal">
                            <i class="fa fa-btn fa-ban"></i> Batal
                        </button>
                        <button type="submit" class="btn btn-success" name="simpan">
                            <i class="fa fa-btn fa-trash"></i> Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- My App -->
    <script type="text/javascript">        
        $(document)
        
        .on('click', 'button[name="edit"]', function() {
            var id = $(this).parents('tr').first().attr('id'); //get id parameter
            var nama_parameter = $(this).parents('tr').first().find('td[name="nama_parameter"]').text();
            var nilai_angka = $(this).parents('tr').first().find('td[name="nilai_angka"]').text();
            var satuan= $(this).parents('tr').first().find('td[name="satuan"]').text();
            $('#put input[name="id"]').val(id);
            $('#put input[name="nama_parameter"]').val(nama_parameter);
            $('#put input[name="nilai_angka"]').val(nilai_angka);
            $('#put input[name="satuan"]').val(satuan);
            $('#put').removeClass('fade');
            $('#put').show();
        })

        .on('click', 'button[name="delete"]', function() {
            var id = $(this).parents('tr').first().attr('id');
            $('#delete input[name="id"]').val(id);
            $('#delete').removeClass('fade');
            $('#delete').show();
        })

        .on('click', 'button[name="batal"], .close-modal', function() {
            $('.modal').hide();
            $('.modal input[name="id"]').val('');
            $('.modal input[name="nama_parameter"]').val('');
            $('.modal input[name="nilai_angka"]').val('');
            $('.modal input[name="satuan"]').val('');
        })
        ;
    </script>
@endsection