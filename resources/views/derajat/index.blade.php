@extends('layouts.admin')

@section('content')
<br>
@if (session('sukses'))
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Selamat!</strong> {{ session('sukses') }}
</div>
@elseif (session('gagal'))
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Oops!</strong> {{ session('gagal') }}
</div>
@endif

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong>Tabel Derajat Keanggotaan</strong>
                <a href="{{url('/derajat/generate')}}" class="btn btn-success pull-right"><i class="fa fa-refresh"></i> Perbaharui</a>
            </div>
            <div class="card-block">
                <table class="table ">
                	<thead class="thead-inverse">
                		<tr>
                            <th>Nomor</th>
                			<th>Nama Kost</th>
                			<th>Nama Variabel</th>
                			<th>Nama Himpunan</th>
                			<th>Nilai Derajat Keanggotaan</th>
                		</tr>
                	</thead>
                	<tbody>
                	@foreach ($derajats as $i => $derajat)
                		<tr>
                            <td>{{$i+1}}</td>
                			<td name="nama_himpunan">{{$derajat->kost->nama_kost}}</td>
                			<td name="nilai_1">{{$derajat->himpunan->parameter->nama_parameter}}</td>
                			<td name="nilai_2">{{$derajat->himpunan->nama_himpunan}}</td>
                			<td name="nilai_3">{{$derajat->nilai}}</td>
                		</tr>
                	@endforeach
                	</tbody>
                </table>
                <nav aria-label="...">
                    <ul class="pagination pagination-sm">
                        <li class="page-item">
                            <a class="page-link" href="{{$derajats->previousPageUrl()}}">Sebelumnya</a>
                        </li>
                        
                        <li class="page-item">
                            <a class="page-link" href="{{$derajats->nextPageUrl()}}">Berikutnya</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection