<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="widht=device, initial-scale=1">
    
    <title>Masuk Bapak'e</title>

    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/simple-line-icons.css') }}" rel="stylesheet">
    
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

</head>

<body>
<div class="container d-table">
    <div class="d-100vh-va-middle">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                @if (session('gagal'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Oops!</strong> {{ session('gagal') }}
                </div>
                @endif
                <div class="card-group">
                    <div class="card p-1">
                        <div class="card-block">
                            <h3>Reset Password</h3>
                            <p class="text-muted">Masukkan Email Anda</p>
                           {{ Form::open(['url' => 'password/reset', 'method' => 'POST']) }}
                                {{ csrf_field() }}
                                <div class="form-group {{ $errors->has('email') ? ' has-danger' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon">@</span>
                                        {{ Form::email('email', $email, ['class' => 'form-control']) }}
                                    </div>
                                    @if ($errors->has('email'))
                                    <div class="form-control-feedback">
                                        {{ $errors->first('email') }}
                                    </div>
                                    @endif
                                    <input type="hidden" name="token" value="{{$token}}">
                                </div>
                                <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-lock"></i></span>
                                        {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password Baru']) }}
                                    </div>
                                    @if ($errors->has('password'))
                                    <div class="form-control-feedback">
                                        {{ $errors->first('password') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-lock"></i></span>
                                        {{ Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Konfirmasi Password']) }}
                                    </div>
                                    @if ($errors->has('password_confirmation'))
                                    <div class="form-control-feedback">
                                        {{ $errors->first('password_confirmation') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        {{ Form::submit('Reset Password', ['class' =>'btn btn-primary px-2']) }}
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

