<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="widht=device, initial-scale=1">
    
    <title>Masuk Bapak'e</title>

    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/simple-line-icons.css') }}" rel="stylesheet">
    
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

</head>

<body>
    <div class="container d-table">
        <div class="d-100vh-va-middle">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    @if (session('gagal'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Oops!</strong> {{ session('gagal') }}
                    </div>
                    @endif
                    <div class="card-group">
                        <div class="card p-2">
                            <div class="card-block">
                                <h1>Masuk</h1>
                                <p class="text-muted">Isikan Email dan Password Anda</p>
                                <form method="POST" action="{{ url('/handleLogin') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group {{ $errors->has('email') ? ' has-danger' : '' }}">
                                        <div class="input-group">
                                            <span class="input-group-addon">@</span>
                                            </span>
                                            <input name="email" type="text" class="form-control" placeholder="Alamat Email" value="{{ old('email') }}">
                                        </div>
                                        @if ($errors->has('email'))
                                        <div class="form-control-feedback">
                                            {{ $errors->first('email') }}
                                        </div>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-lock"></i>
                                            </span>
                                            <input name="password" type="password" class="form-control" placeholder="Password">
                                        </div>
                                        @if ($errors->has('password'))
                                        <div class="form-control-feedback">
                                            {{ $errors->first('password') }}
                                        </div>
                                        @endif
                                    </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <button type="submit" class="btn btn-primary px-2">Login</button>
                                            </div>
                                        </div>
                                </form>
                                <p style="padding-top: 10px">Lupa password? <a href="{{ url('/password/reset') }}" class="forgot-password">
                                    klik disini
                                </a></p>
                            </div>
                        </div>
                        <div class="card card-inverse card-primary py-3 hidden-md-down">
                            <div class="card-block text-xs">
                                <div>
                                    <h2>Daftar</h2>
                                    <p>Silahkan mendaftar jika anda belum mempunyai akun.</p>
                                    <p>Pendaftaran dilakukan untuk tujuan mendapatkan informasi pengguna secara jelas.</p>
                                    <a href="{{ url('/register') }}" class="btn btn-primary active mt-5">Daftar Sekarang!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

