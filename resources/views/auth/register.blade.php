<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="widht=device, initial-scale=1">
    
    <title>Daftar | Bapak'e</title>

    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/simple-line-icons.css') }}" rel="stylesheet">
    
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

</head>

<body>
    <div class="container d-table">
        <div class="d-100vh-va-middle">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    @if (session('gagal'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Oops!</strong> {{ session('gagal') }}
                    </div>
                    @endif
                    <div class="card mx-2">
                        <div class="card-block p-2">
                        <h3>Daftar </h3>
                            <p class="text-muted">Buat akun anda</p>
                                <form action="" method="POST" action="{{ url('/register') }}" role="form">
                                {{ csrf_field() }}
                                <div class="form-group {{ $errors->has('no_identitas') ? ' has-danger' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-credit-card"></i>
                                        </span>
                                        <input name="no_identitas" type="text" class="form-control" placeholder="No Identitas (KTM, KTP, SIM, dll)" value="{{ old('no_identitas') }}">
                                    </div>
                                    @if ($errors->has('no_identitas'))
                                        <div class="form-control-feedback">
                                            {{ $errors->first('no_identitas') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('nama_depan') ? ' has-danger' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i>
                                        </span>
                                        <input name="nama_depan" type="text" class="form-control" placeholder="Nama Depan" value="{{ old('nama_depan') }}">
                                    </div>
                                    @if ($errors->has('nama_depan'))
                                        <div class="form-control-feedback">
                                            {{ $errors->first('nama_depan') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('nama_belakang') ? ' has-danger' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i>
                                        </span>
                                        <input name="nama_belakang" type="text" class="form-control" placeholder="Nama Belakang" value="{{ old('nama_belakang') }}">
                                    </div>
                                    @if ($errors->has('nama_belakang'))
                                        <div class="form-control-feedback">
                                            {{ $errors->first('nama_belakang') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('email') ? ' has-danger' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon">@</span>
                                        <input type="text" name="email" class="form-control" placeholder="Alamat Email" value="{{ old('email') }}">
                                    </div>
                                    @if ($errors->has('email'))
                                        <div class="form-control-feedback">
                                            {{ $errors->first('email') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-lock"></i>
                                        </span>
                                        <input name="password" type="password" class="form-control" placeholder="Password">
                                    </div>
                                    @if ($errors->has('password'))
                                        <div class="form-control-feedback">
                                            {{ $errors->first('password') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('username') ? ' has-danger' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-user"></i>
                                        </span>
                                        <input name="username" type="text" class="form-control" placeholder="Username" value="{{ old('username') }}">
                                    </div>
                                    @if ($errors->has('username'))
                                        <div class="form-control-feedback">
                                            {{ $errors->first('username') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('jenis') ? ' has-danger' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-venus-mars"></i>
                                        </span>
                                        <select name="jenis" type="text" class="form-control">
                                            <option>Jenis kelamin</option>
                                            <option value="Pria">Pria</option>
                                            <option value="Wanita">Wanita</option>
                                        </select>
                                    </div>
                                    @if ($errors->has('jenis'))
                                        <div class="form-control-feedback">
                                            {{ $errors->first('jenis') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('level') ? ' has-danger' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-group"></i>
                                        </span>
                                        <select name="level" type="text" class="form-control">
                                            <option>Daftar sebagai</option>
                                            <option value="mahasiswa">Mahasiswa</option>
                                            <option value="pemilik">Pemilik</option>
                                        </select>
                                    </div>
                                    @if ($errors->has('level'))
                                        <div class="form-control-feedback">
                                            {{ $errors->first('level') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('hp') ? ' has-danger' : '' }}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-phone"></i>
                                        </span>
                                        <input name="hp" type="text" class="form-control" placeholder="No Telepon/HP" value="{{ old('hp') }}">
                                    </div>
                                    @if ($errors->has('hp'))
                                        <div class="form-control-feedback">
                                            {{ $errors->first('hp') }}
                                        </div>
                                    @endif
                                </div>

                                    <button type="submit" class="btn btn-block btn-primary">Daftar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

