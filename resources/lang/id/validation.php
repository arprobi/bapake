<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Baris :attribute must be accepted.',
    'active_url'           => 'Baris :attribute is not a valid URL.',
    'after'                => 'Baris :attribute must be a date after :date.',
    'alpha'                => 'Baris :attribute may only contain letters.',
    'alpha_dash'           => 'Baris :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'Baris :attribute may only contain letters and numbers.',
    'array'                => 'Baris :attribute must be an array.',
    'before'               => 'Baris :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'Baris :attribute must be between :min and :max.',
        'file'    => 'Baris :attribute must be between :min and :max kilobytes.',
        'string'  => 'Baris :attribute must be between :min and :max characters.',
        'array'   => 'Baris :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'Baris :attribute field must be true or false.',
    'confirmed'            => 'Baris konfirmasi tidak cocok.',
    'date'                 => 'Baris :attribute is not a valid date.',
    'date_format'          => 'Baris :attribute does not match the format :format.',
    'different'            => 'Baris :attribute and :other must be different.',
    'digits'               => 'Baris :attribute must be :digits digits.',
    'digits_between'       => 'Baris :attribute must be between :min and :max digits.',
    'dimensions'           => 'Baris :attribute has invalid image dimensions.',
    'distinct'             => 'Baris :attribute field has a duplicate value.',
    'email'                => 'Baris email harus alamat email yang sah.',
    'exists'               => 'The selected :attribute is invalid.',
    'filled'               => 'Baris :attribute field is required.',
    'image'                => 'Baris :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'Baris :attribute field does not exist in :other.',
    'integer'              => 'Baris :attribute harus berupa angka.',
    'ip'                   => 'Baris :attribute must be a valid IP address.',
    'json'                 => 'Baris :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'Baris :attribute tidak boleh lebih dari :max.',
        'file'    => 'Baris :attribute tidak boleh lebih dari :max KB.',
        'string'  => 'Baris :attribute tidak boleh lebih dari :max karakter.',
        'array'   => 'Baris :attribute may not have more than :max baris.',
    ],
    'mimes'                => 'Baris :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'Baris :attribute paling sedikit :min.',
        'file'    => 'Baris :attribute paling sedikit :min KB.',
        'string'  => 'Baris :attribute paling sedikit :min karakter.',
        'array'   => 'Baris :attribute paling sedikit :min baris.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'Baris :attribute must be a number.',
    'present'              => 'Baris :attribute field must be present.',
    'regex'                => 'Baris :attribute format is invalid.',
    'required'             => 'Baris :attribute tidak boleh kosong.',
    'required_if'          => 'Baris :attribute field is required when :other is :value.',
    'required_unless'      => 'Baris :attribute field is required unless :other is in :values.',
    'required_with'        => 'Baris :attribute field is required when :values is present.',
    'required_with_all'    => 'Baris :attribute field is required when :values is present.',
    'required_without'     => 'Baris :attribute field is required when :values is not present.',
    'required_without_all' => 'Baris :attribute field is required when none of :values are present.',
    'same'                 => 'Baris :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'Baris :attribute must be :size.',
        'file'    => 'Baris :attribute must be :size kilobytes.',
        'string'  => 'Baris :attribute must be :size characters.',
        'array'   => 'Baris :attribute must contain :size items.',
    ],
    'string'               => 'Baris :attribute must be a string.',
    'timezone'             => 'Baris :attribute must be a valid zone.',
    'unique'               => 'Baris :attribute sudah dipakai.',
    'url'                  => 'Baris :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
