$('#prev').click(function () {
	const CHART = document.getElementById("linechart");
console.log(CHART);

var domain1 = $('input[name="nilai_1"]').val();
var domain2 = $('input[name="nilai_2"]').val();
var domain3 = $('input[name="nilai_3"]').val();
var domain4 = $('input[name="nilai_4"]').val();

if (domain1 && domain2 == 0) {
	var y1 = 1;	var y2 = 1;	var y3 = 1;	var y4 = 0;

	var x = [domain2, domain3, domain4];
	var y = [y2, y3, y4];
} else if ((domain1 != 0) && (domain2 == domain3) && (domain3 < domain4)) {
	var y1 = 0;	var y2 = 1;	var y3 = 1;	var y4 = 0;

	var x = [domain1, domain3, domain4];
	var y = [y1, y3, y4];
} else if ((domain1 != 0) && (domain3 == domain4)) {
	var y1 = 0;	var y2 = 1;	var y3 = 1;	var y4 = 1;

	var x = [domain1, domain2, domain3];
	var y = [y1, y2, y3];
}

let linechart = new Chart(CHART, {
	type: 'line',
	data: {
		labels: x,
		datasets: [
			{
				label: "Preview grafik",
	            fill: false,
	            lineTension: 0.1,
	            backgroundColor: "rgba(75,192,192,0.4)",
	            borderColor: "rgba(75,192,192,1)",
	            borderCapStyle: 'butt',
	            borderDash: [],
	            borderDashOffset: 0.0,
	            borderJoinStyle: 'miter',
	            pointBorderColor: "rgba(75,192,192,1)",
	            pointBackgroundColor: "#fff",
	            pointBorderWidth: 1,
	            pointHoverRadius: 5,
	            pointHoverBackgroundColor: "rgba(75,192,192,1)",
	            pointHoverBorderColor: "rgba(220,220,220,1)",
	            pointHoverBorderWidth: 2,
	            pointRadius: 1,
	            pointHitRadius: 10,
	            data: y,
	            spanGaps: false,
			}
		]
	}
});	
})
